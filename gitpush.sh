if [ -z "$1" ]; then
    echo "Add some commit message dude..."
else
    git add .
    git commit -m "$1"
    git push origin master
fi
