window.app.models.ProjectModel = (function(){

  function Project(id, projectSector, projectCategory, question1, question2, question3, question4, question5, image, name, video, shortDescription, richDescription, searchKeywords, duration, goal, challenges, gaTrackingCode, congratsText, congratsImage, likeName, likeIcon, likePoint, dislikeName, dislikeIcon, dislikePoint, commentHiding){
    this.id = id;
    this.projectSector = projectSector;
    this.projectCategory = projectCategory;
    this.question1 = question1;
      this.question2 = question2;
      this.question3 = question3;
      this.question4 = question4;
      this.question5 = question5;
    this.image = image;
    this.name = name;
    this.video = video;
    this.shortDescription =  shortDescription;
    this.richDescription = richDescription;
    this.searchKeywords = searchKeywords;
    this.duration = duration;
    this.goal = goal;
    this.challenges = challenges;
    this.gaTrackingCode = gaTrackingCode;
    this.congratsText = congratsText;
    this.congratsImage = congratsImage;
    this.likeName = likeName;
    this.likeIcon = likeIcon;
    this.likePoint = likePoint;
    this.dislikeName = dislikeName;
    this.dislikeIcon = dislikeIcon;
    this.dislikePoint = dislikePoint;
    this.commentHiding = commentHiding;
  }

  return Project;
})();
