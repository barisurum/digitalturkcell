window.app.models.UserModel = (function(){

  function User(id, name, surname, email, phone, biography, website, image, cityId){
    this.id = isNaN(id) ? '' : id;
    this.name = name;
    this.surname = surname;
    this.email =  email;
    this.phone = phone;
    this.biography = biography;
    this.website = website;
    this.image = image;
    this.cityId = cityId;
  }

  return User;
})();
