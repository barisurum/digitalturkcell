window.app.models.RewardModel = (function(){

  function Reward(id, name, count, delivery, description, minAmount){
    this.id = isNaN(id) ? null : id;
    this.name = name;
    this.count = count;
    this.delivery = delivery;
    this.description =  description;
    this.minAmount = minAmount;
  }

  return Reward;
})();
