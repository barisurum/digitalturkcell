(function() {
  'use strict';
  window.app = window.app || {};
  window.app.PledgeModule = (function ($) {
    var baseUrl = '',
      config = {
        search: window.location.search,
        cityName: $('#cityName'),
        cityId: $('#cityId'),
        agreement: $('.agreement'),
        userAgreement: $('#userAgreement'),
        agree: $('#agree'),
        pledgeForm: $('#pledgeForm'),
        pledgeErrorType: document.getElementById('pledgeErrorType'),
        savedCardInfo: document.getElementById('savedCardInfo'),
        cardPass: document.getElementById('cardPass'),
        ccInfo: document.getElementById('ccInfo'),
        submitButton: $('#submit').find('button'),
        numericInput: $('.numeric-input'),
        applyCoupon : $('#applyCoupon'),
      };

      var render = function(data, template, target) {
        console.log(target);
        target.html($.parseHTML(app.Templates[template](data)));
      }

      var sendData = function (url, data) {
        return $.ajax({
          url: baseUrl + url,
          type: 'POST',
          data: data
        });
      }

    var getCoupon = function(){
      render({cs: contentStrings}, 'pledge-coupon', $('#applyCoupon'));

      $(document).ready(function(){
        $.ajax({
            url:'/projects/check-coupon',
            method:'get',
            data: {rewardId: parseInt($('input[name="rewardId"]').val())},
            success:function(data){
                if(data.success){
                  $('#coupon-result-msg').html('<p style="color: #3C763D; font-size: 13px;">' + data["message"] + '</p>').removeAttr('style');
                  $('#coupon-result-success').removeAttr('style');

                  $('#subTotalID').html(data["originalPrice"] + ' ' + contentStrings.currency);
                  $('#discountID').html(data["discount"] + ' ' + contentStrings.currency);
                  $('#totalID').html(data["grandTotal"] + ' ' + contentStrings.currency);
                  $('.reward__pledge-amount').html(data['grandTotal'] + ' ' + contentStrings.currency);

                  $('#couponTextField').val('');
                }
            }
          });


      });

      $('#applyCouponButton').on('click', function(){
        // console.log({couponCode: $('#couponTextField').val(), rewardId: parseInt($('input[name="rewardId"]').val()), pledgeAmount: parseInt($('input[name="amount"]').val()) });
        sendData('/projects/apply-coupon', {couponCode: $('#couponTextField').val(), rewardId: parseInt($('input[name="rewardId"]').val()), pledgeAmount: parseInt($('input[name="amount"]').val()) }).done(function(data){
          // console.log(data);
          if(!data.success){
            $('#coupon-result-msg').html('<p style="color: #ffb900; font-size: 13px;">' + data["message"] + '</p>').removeAttr('style');
          }else{
            $('#coupon-result-msg').html('<p style="color: green; font-size: 13px;">' + data["message"] + '</p>').removeAttr('style');
            $('#coupon-result-success').removeAttr('style');

            $('#subTotalID').html(data["originalPrice"] + ' ' + contentStrings.currency);
            $('#discountID').html(data["discount"] + ' ' + contentStrings.currency);
            $('#totalID').html(data["grandTotal"] + ' ' + contentStrings.currency);
            $('.reward__pledge-amount').html(data['grandTotal'] + ' ' + contentStrings.currency);
          }
          $('#couponTextField').val('');

        });
      });
    }

    var init = function() {
      fbq('track', 'InitiateCheckout');
      fbq('track', 'AddToCart');

      var parsleyOptions = {
        errorClass: 'error',
        successClass: 'valid',
        classHandler: function(el) {
          return el.$element.parents('li');
        },
        excluded: '[disabled]'
      };
      config.pledgeForm.parsley(parsleyOptions);

      //Confirm agreement
      function checkAgreements() {
        var submit = $('#submit').find('button'),
          count = 0;
        config.agreement.each(function(e) {
          if($(this).is(':checked')) {
            count++;
          }
          return count;
        });
        count === config.agreement.length  ? submit.removeAttr('disabled') : submit.attr('disabled', 'disabled');
      }

      function checkSavedCardInfo() {
        if($(this).is(':checked')) {
          $(this).val(true);
          config.ccInfo.style.display = 'none';
          config.cardPass.style.display = 'block';

          config.pledgeForm.parsley(parsleyOptions).destroy();

          $(config.cardPass).find('input').removeAttr('disabled');
          $(config.ccInfo).find('input, select').attr('disabled', 'disabled');

          config.pledgeForm.parsley(parsleyOptions);
        }
        else {
          $(this).val(false);
          config.cardPass.style.display = 'none';
          config.ccInfo.style.display = 'block';

          config.pledgeForm.parsley(parsleyOptions).destroy();

          $(config.ccInfo).find('input, select').removeAttr('disabled');
          $(config.cardPass).find('input').attr('disabled', 'disabled');

          config.pledgeForm.parsley(parsleyOptions);
        }
      }

      checkAgreements();
      config.agreement.change(checkAgreements);
      config.agree.click(function() {
        config.userAgreement.prop('checked', true);
      });

      checkSavedCardInfo();
      $(config.savedCardInfo).change(checkSavedCardInfo);

      //Remember Card
      $('#ccRemember, #threeDs').change(function() {
        $(this).is(':checked') ? $(this).val(true) : $(this).val(false);
      });

      var autoComplete = function() {
        config.cityName.autocomplete({
          minLength: 1,
          source: function( request, response ) {
            $.ajax({
              url: baseUrl + '/misc/autocomplete-city',
              data: {
                query: request.term
              },
              success: function( data ) {
                response( data );
              }
            });
          },
          /*focus: function( event, ui ) {
            config.cityName.val( ui.item.name );
            config.cityId.val( ui.item.id );
            return false;
          },*/
          select: function( event, ui ) {
            event.preventDefault();
            config.cityName.val( ui.item.name );
            config.cityId.val( ui.item.id );
            return false;
          },
          open: function(){
            $('.ui-autocomplete').css('width', '750px');
          }
        })
          .bind('input propertychange', function() {
            config.cityId.val('');
          })
          .autocomplete( 'instance' )._renderItem = function( ul, item ) {
          return $( '<li>' )
            .append( '<a>' + item.name )
            .appendTo( ul );
        };
      }();

      var buttonAnimation = null;

      function animateButton(button, text, padding, addedClass){
        var i = 0,
          btn = button.attr('disabled', 'disabled').removeClass('green-dark--link').addClass(addedClass).html(text).css({
            'text-align': 'left',
            'padding-left': padding+'px'
          });
        buttonAnimation = setInterval(function () {
          i = ++i % 4;
          btn.html(text + new Array(i + 1).join('.'));
        }, 500);

        return button;
      }

      window.onbeforeunload = function() {
        return 'Bir şeyler ters mi gitti?'
      };

      config.pledgeForm.on('submit', function(e){
        if(config.pledgeForm.parsley(parsleyOptions).validate()) {
          console.log('form validated');
          animateButton(config.submitButton, 'Ödeme İşlemi Alınıyor', '50', 'white');
          window.onbeforeunload = null;
        }
      });

      //Show pledge form error after form submit
      if(config.search.indexOf('error=true')!== -1) {
        var msgString = 'message=',
          message = config.search.substring(config.search.indexOf(msgString) + msgString.length);
          config.pledgeErrorType.innerHTML = decodeURIComponent(message);
          config.pledgeErrorType.classList.add('flash');
          config.pledgeErrorType.style.display = 'block';
      }

      //Allow only numeric inputs
      var im = new Inputmask({ 'mask': '9', 'repeat': '*', 'greedy': false, 'rightAlign': false });
      im.mask(config.numericInput);

      //Credit card number
      var ccNumber = new Inputmask({
        mask: '9999 9999 9999 9999',
        placeholder: ' ',
        showMaskOnHover: false,
        showMaskOnFocus: false,
        rightAlign: false
      });
      ccNumber.mask($('#number'));
      //Tooltip
      $('[data-toggle="tooltip"]').tooltip({
        trigger: 'hover'
      });


      $.when.apply($, getCoupon()).done(function() {
          // doneCallback();
      })
      .fail(function() {
        console.log('fail')
      });

    };

    return {
      start: init
    }

  }(jQuery));

}());
