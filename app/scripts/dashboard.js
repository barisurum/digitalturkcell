(function() {
  'use strict';
  window.app = window.app || {};
  window.app.DashboardModule = (function ($) {

    var baseUrl = '';
    var mainTabs = $('#sidebar-tabs');
    var config = {
      body: $('body'),
      hash: window.location.hash,
      tabs: {
        messages: $('a[aria-controls="messages"]'),
        activities: $('a[aria-controls="activities"]'),
        backedProjects: $('a[aria-controls="backedProjects"]'),
        favourites: $('a[aria-controls="favourites"]'),
        projects: $('a[aria-controls="projects"]'),
        profile: $('a[aria-controls="profile"]'),
        //comments: $('a[aria-controls="comments"]'),
        invites: $('a[aria-controls="invites"]'),

      },
      tabContents: {
        activities: document.getElementById('activities'),
        activitiesContent: activities.getElementsByClassName('content'),
        messages: $('#messages'),
        backedProjects: document.getElementById('backedProjects'),
        backedProjectsContent: backedProjects.getElementsByClassName('content'),
        favourites: document.getElementById('favourites'),
        favouritesContent: favourites.getElementsByClassName('content'),
        projects: document.getElementById('projects'),
        projectsContent: projects.getElementsByClassName('content'),
        profile: document.getElementById('profile'),
        profileContent: profile.getElementsByClassName('content'),
        //comments: document.getElementById('comments'),
        //commentsContent: comments.getElementsByClassName('content'),
        invites: document.getElementById('invites'),
        invitesContent: invites.getElementsByClassName('content')
      },
      user: $('#user'),
      userId: $('#user').data('id'),
      fullName: $('#user').text(),
      avatarImg: $('#avatarImg').attr('src'),
      dropDownMenu: $('.dashboard-tab'),
      discardOrSave: $('#discardOrSave'),
      deleteActivityMessage: $('#deleteActivityMessage'),
      activityCancelDelete: $('#activityCancelDelete'),
      activityConfirmDelete: $('#activityConfirmDelete')
    },

    manageTabs = function (e) {
      var hash = $(e.target).attr('href');
      if (hash.substr(0, 1) === '#') {
       /* window.history.pushState(null, null, hash);*/
        var position = $(window).scrollTop();
        location.replace('#' + hash.substr(1));
        $(window).scrollTop(position);
      }
    },

    discardOrSave = function(openOrClose) {
      if(openOrClose==='open') {
        config.discardOrSave.addClass('visible');
      }
      if(openOrClose==='close') {
        config.discardOrSave.removeClass('visible');
      }
    },

    getData = function (url) {
      return $.ajax({
        url: baseUrl + url,
        type: 'GET',
        contentType: 'application/json',
        dataType: 'json'
      });
    },

    sendData = function (url, data) {
      return $.ajax({
        url: baseUrl + url,
        type: 'POST',
        contentType: 'application/json',
        dataType: 'json',
        data: JSON.stringify(data)
      });
    },


    render = function(data, template, target, append) {
      append ? target.append($.parseHTML(app.Templates[template](data))) : target.html($.parseHTML(app.Templates[template](data)));
    },

    doneCallback = function() {
      console.log('done');
    },

    getActivities = function() {
      $(config.tabContents.activitiesContent).html($.parseHTML(app.Templates['dashboard-loading']()));//Loading Animation
      getData('/users/backed-project-activities').done(function(data){
          data.cs = contentStrings;
          console.log(data);
          if (data.length > 0) {
            for (var i = 0; i < data.length; ++i) {
              data[i].cs = contentStrings;
            }
          }
        render(data, 'dashboard-activities', $(config.tabContents.activitiesContent) );
      })
      .fail(function() {
        console.log('fail Activities')
      });
    },

        getInvites = function() {
            $(config.tabContents.invitesContent).html($.parseHTML(app.Templates['dashboard-loading']()));//Loading Animation
            getData('/users/project-invited').done(function(data){
                data.cs = contentStrings;
                console.log(data);
                if (data.length > 0) {
                    for (var i = 0; i < data.length; ++i) {
                        data[i].cs = contentStrings;
                    }
                }
                render(data, 'dashboard-invites', $(config.tabContents.invitesContent) );
            })
                .fail(function() {
                    console.log('fail invites')
                });
        },

        getComments = function() {
            $(config.tabContents.commentsContent).html($.parseHTML(app.Templates['dashboard-loading']()));//Loading Animation
            getData('/users/quick-ratings').done(function(data){
                data.quickRatings.cs = contentStrings;
                console.log(data);
                if (data.quickRatings.length > 0) {
                    for (var i = 0; i < data.length; ++i) {
                        data.quickRatings[i].cs = contentStrings;
                    }
                }
                render(data.quickRatings, 'dashboard-comments', $(config.tabContents.commentsContent) );
            })
                .fail(function() {
                    console.log('fail comments')
                });
        },


    getDashboardProjectActivities = function(projectId) {
      $(config.tabContents.projectsContent).html($.parseHTML(app.Templates['dashboard-loading']()));//Loading Animation
      getData('/projects/'+projectId+'/activities').done(function(data){
        console.log(data);
        data.cs = contentStrings;
        if (data.length > 0) {
          for (var i = 0; i < data.length; ++i) {
            data[i].cs = contentStrings;
          }
        }
        render(data, 'dashboard-activities-new-update', $(config.tabContents.projectsContent));
        projectActivitiesEvents(projectId);
      })
        .fail(function() {
          console.log('fail getDashboardProjectActivities')
        });
    },

    addUpdateProjectActivity = function(projectId, activityTitle, activityDescription, activityId) {
      $(config.tabContents.projectsContent).html($.parseHTML(app.Templates['dashboard-loading']()));//Loading Animation
      var url = function() {
        return activityId ? '/projects/'+projectId+'/activities/'+activityId+'/update' : '/projects/'+projectId+'/activities/create';
      };
      $.ajax({
        type: 'POST',
        contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
        url: baseUrl + url(),
        data: {
          title: activityTitle,
          description: activityDescription
        }
      }).done(function(data) {
        data.cs = contentStrings;
        render(data, 'dashboard-activities-new-update', $(config.tabContents.projectsContent));
        projectActivitiesEvents(projectId);
      })
        .fail(function() {
          console.log('fail addUpdateProjectActivity')
        });
    },

    deleteProjectActivity = function(projectId, activityId) {
      $(config.tabContents.projectsContent).html($.parseHTML(app.Templates['dashboard-loading']()));//Loading Animation
      $.ajax({
        type: 'POST',
        contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
        url: baseUrl + '/projects/'+projectId+'/activities/'+activityId+'/delete'
      }).done(function(data) {
        discardOrSave('close');
        data.cs = contentStrings;
        render(data, 'dashboard-activities-new-update', $(config.tabContents.projectsContent));
        projectActivitiesEvents(projectId);
      })
        .fail(function() {
          console.log('fail addUpdateProjectActivity')
        });
    },

    projectActivitiesEvents = function(projectId) {
      discardOrSave('close');

      //Create and Update Activity
      var btnAddUpdateActivity = $('#btnAddUpdateActivity'),
        clearActivity = $('#clearActivity'),
        activityTitle = $('#activityTitle'),
        activityDescription = $('#activityDescription');

      $('.edit').click(function() {
        var $this = $(this);
        btnAddUpdateActivity.data('activity-id', $this.data('activity-id'));
        activityTitle.val($this.data('title'));
        activityDescription.val($this.data('description'));
        $('html, body').animate({
          scrollTop: $('#titleAddUpdate').offset().top
        }, 500);
      });

      clearActivity.click(function() {
        btnAddUpdateActivity.removeAttr('data-activity-id');
        activityTitle.val('');
        activityDescription.val('');
      });

      btnAddUpdateActivity.click(function(){
        var $activityId = $(this).data('activity-id'),
          $activityTitle  = activityTitle.val(),
          $activityDescription = activityDescription.val();

        $activityId ? addUpdateProjectActivity(projectId, $activityTitle, $activityDescription, $activityId) : addUpdateProjectActivity(projectId, $activityTitle, $activityDescription);
      });

      //Delete Activity
      $('.delete').click(function() {
        var $this = $(this);
        config.activityConfirmDelete.click(function() {
          deleteProjectActivity(projectId, $this.data('activity-id'))
        });
        config.activityCancelDelete.click(function() {
          discardOrSave('close');
        });

        config.deleteActivityMessage.html('"'+$this.data('title')+'" ' + contentStrings.myprojectstimelinedeletebottomtext);
          discardOrSave('open');
      });
    },




    getProjects = function() {
      $(config.tabContents.projectsContent).html($.parseHTML(app.Templates['dashboard-loading']()));//Loading Animation
      getData('/projects/my-projects').done(function(data){
        var obj = {
          data: data,
          cs: contentStrings
        }
        for (var i = 0; i < data.length; ++i) {
          obj.data[i].cs = contentStrings;
        }
        console.log(obj);
        render(obj, 'dashboard-projects', $(config.tabContents.projectsContent) );
        $('.timeline').click(function() {
          getDashboardProjectActivities($(this).data('id'));
        });
      })
        .fail(function() {
          console.log('fail Projects')
        });
    },


    getMessages = function() {
      var csString = '<h3 class="green-dark fwnormal mb30">' + contentStrings.profilepagetabmessagestitle + ' <span>(' + window.unreadMessageCount  + ')</span></h3>';
      $(config.tabContents.messages).html(csString);
      $(config.tabContents.messages).append($.parseHTML(app.Templates['dashboard-loading']()));//Loading Animation
      getData('/messages/conversations').done(function(data){
        data.unreadCount = window.unreadMessageCount;
        data.cs = contentStrings;
        render(data, 'dashboard-conversation', $(config.tabContents.messages) );

        $('.full-link').click(function() { //Add Message Click Listener
          var conversation = data[$(this).data('index')],
          messages = conversation['messages']; //Get message object inside conversations array
          messages.forEach(function(e) {
            if(e.senderId == config.userId) {
              e.name = config.fullName;
              e.image = config.avatarImg;
              e.self = true;
            }
            else {
              e.name = conversation.counterpartName;
              e.image = conversation.counterpartImage;
              e.self = false;
            }
          });
          conversation.cs = contentStrings;
          render(conversation, 'dashboard-message', $(config.tabContents.messages));
          $('html, body').animate({
            scrollTop: $('#message').offset().top
          }, 500);
          $('#sendMessage').click(sendMessage);
        });

      })
        .fail(function() {
          console.log('fail Messages')
        });
    },

    getBackedProjects = function() {
      $(config.tabContents.backedProjectsContent).html($.parseHTML(app.Templates['dashboard-loading']()));//Loading Animation
      getData('/users/backed-projects').done(function(data){
        data.cs = contentStrings;
        render(data, 'dashboard-backed-projects', $(config.tabContents.backedProjectsContent) );
        $('[data-toggle="tooltip"]').tooltip();
      })
      .fail(function() {
        console.log('fail Backed Projects')
      });
    },

    getFavouriteProjects = function() {
      $(config.tabContents.favouritesContent).html($.parseHTML(app.Templates['projects-loading']()));//Loading Animation
      getData('/projects/favorites').done(function(data){
        data.cs = contentStrings;
        for (var i = 0; i < data.length; ++i) {
          data[i].cs = contentStrings;
        }
        data.forEach(function (e) {
          e.favourite = true;
        });
        render(data, 'projects', $(config.tabContents.favouritesContent) );
      })
        .fail(function() {
          console.log('fail Favourite Projects')
        });
    },

    profileEvents = function() {
      /*Profile Listeners*/
      $('#phone').on('keyup', function(e) {
        $(this).val($(this).val().replace(/[^0-9]/g, ''));
      });

      $('#fileupload').dropzone({
        url: baseUrl + '/misc/upload-image',
        paramName: 'image',
        clickable: true,
        previewsContainer: '#uploader',
        headers: {
          'Cache-Control': null,
          'X-Requested-With': null
        },
        acceptedFiles: 'image/*',
        init: function() {
          this.on('addedfile', function(file) {
            $(this.previewsContainer).removeAttr('style');
            $(this.previewsContainer).removeClass('has-avatar');
            $(this.previewsContainer).addClass('loading');
          })
            .on('success', function(file, response) {
              this.element.value = response.url;
              $(this.previewsContainer).css('background-image', 'url("'+response.url+'")');
              $(this.previewsContainer).addClass('has-avatar');
              $(this.previewsContainer).removeClass('loading');
            });
        }
      });

      $('#profileUpdateBtn').click(sendProfile);
      $('#changeSubmit').click(changePassword);
      $('#resetPasswordForm').click(changePasswordReset);

      $('#changePasswordForm').parsley().on('form:error', function (e) {
        $('#changePasswordErrorType2').hide();
        $('#changePasswordErrorType1').removeClass('flash').addClass('flash').show();
      });
    },

    getProfile = function() {
      $(config.tabContents.profileContent).html($.parseHTML(app.Templates['dashboard-loading']()));//Loading Animation
      getData('/users/me').done(function(data){
        data.cs = contentStrings;
        render(data, 'dashboard-profile', $(config.tabContents.profileContent) );
        profileEvents();
      })
        .fail(function() {
          console.log('fail Profile')
        });
    },

    sendProfile = function() {
      var $this = $(this),
        form = $this.parents('form'),
       formData = {
         id: parseInt(form.find('input[name="id"]').val()),
         image: form.find('input[name="image"]').val(),
         name: form.find('input[name="name"]').val(),
         surname: form.find('input[name="surname"]').val(),
         email: form.find('input[name="email"]').val()
       };

      form.parsley().validate();

      form.parsley().on('form:validate', function (formInstance) {
        var ok = formInstance.isValid();
        if (ok) {
          $this.attr('disabled', 'disabled');
          $this.off('click');

          /*var cc = form.find('input[name="image"]').val();
           console.log(cc);*/
          $(config.tabContents.profileContent).html($.parseHTML(app.Templates['dashboard-loading']()));//Loading Animation
          sendData(baseUrl +'/users/update', formData)
            .done(function(data) {
              data.cs = contentStrings;
              render(data, 'dashboard-profile', $(config.tabContents.profileContent));
              location.reload();
              /*profileEvents();*/
            })
            .fail(function() {
              console.log('fail sendProfile')
            });
        }
      });
    },

    sendMessage = function() {
      var $this = $(this),
        message = $('#message').val();

      $this.attr('disabled', 'disabled');
      $this.off('click');
      $.post(baseUrl +'/messages/create', {
        counterpartId: parseInt($('#counterPart').data('id')),
        message: message
      }).done(function() {
        var data = {
          image: config.avatarImg,
          name: config.fullName,
          message: message
        };
        $this.click(sendMessage);
        $this.removeAttr('disabled');
        data.cs = contentStrings;
        render(data, 'dashboard-message', $(config.tabContents.messages).find('.content'), true );
        $('#message').val('');
      });
    },

    changePassword = function(e) {
      e.preventDefault();

      var form = $('#changePasswordForm');
      var parsleyOptions = {
        errorClass: 'error',
        successClass: 'valid'
      };

      var validForm = form.parsley(parsleyOptions);

      if(validForm.validate()) {

        var $this = $(this),
          message = $('#forgotPasswordModalMessage');

        if ( $this.data('requestRunning') ) {
          return;
        }
        $this.data('requestRunning', true);


        $.ajax({
          url: baseUrl + '/users/update-password',
          type: 'POST',
          contentType: 'application/x-www-form-urlencoded; charset=UTF-8',
          data: {
            oldPassword: $('#oldPassword').val(),
            newPassword: $('#newPassword').val()
          }
        }).done(function (data) {
          $this.data('requestRunning', false);

          if (data.success) {
            $('#changePasswordErrorType2').hide();
            form.hide();
            form.find('input').val('');
            message.find('p').html(data.message);
            message.show();
          }
          else if (!data.success) {
            $('#changePasswordErrorType1').hide();
            $('#changePasswordErrorType2').html(data.message).removeClass('flash').addClass('flash').show();
          }
          else {
            console.log('writeComment fail');
            return false;
          }

        });
      }
    },

    changePasswordReset = function() {
      $('#changePasswordModal').on('hidden.bs.modal', function(event){
        $('#forgotPasswordModalMessage').hide();
        $('#changePasswordForm').show();
      });
    },


    init = function() {
      $.ajaxSetup({
        beforeSend: function(jqXHR) {
          jqXHR.setRequestHeader('cf-request-type', 'ajax');
        },
        statusCode: {
          403: function() {
            location.href = '/login'
          }
        }
      });


      mainTabs.on('shown.bs.tab', manageTabs); //Activate Tabs

      config.tabs.activities.click(getActivities);
      config.tabs.backedProjects.click(getBackedProjects);
      config.tabs.favourites.click(getFavouriteProjects);
      config.tabs.messages.click(getMessages);
      config.tabs.projects.click(getProjects);
      config.tabs.profile.click(getProfile);

      //config.tabs.comments.click(getComments);
      config.tabs.invites.click(getInvites);

        config.dropDownMenu.click(function(e) {
        e.preventDefault();
        var href = $(this).attr('aria-controls');
        mainTabs.find('a[href="#'+href+'"]').tab('show');
      });

      if (config.hash !== '') {
        var activeTab = $('a[href="' + config.hash + '"]');
        activeTab.trigger('click');
      }
      else {
        getActivities();
      }

    };

    return {
      start: init
    }
  }(jQuery));

}());
