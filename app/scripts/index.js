(function() {
  'use strict';
  window.app = window.app || {};
  window.app.ProjectIndexModule = (function ($, app) {
    var baseUrl = '';
    var config = {
        projectsHome: $('#projectsHome'),
        savingLayer: $('#savingLayer'),
        mainCarousel: $('#mainCarousel')
      },

      getData = function (url) {
        return $.ajax({
          url: baseUrl + url,
          type: 'GET',
          contentType: 'application/json',
          dataType: 'json'
        });
      },

      getProjects = function() {
        return [
          getData('/projects/home').done(function(data){
              console.log('HOME DATA');
              console.log(data);
            for (var i = 0; i < data.length; ++i) {
              data[i].cs = contentStrings;
              data[i].au = activeUser;
            }
            console.log(data);
            if (window.innerWidth <= 768) {
                render(data, 'projects-mob', config.projectsHome);
            } else {
                render(data, 'projects', config.projectsHome);
            }
            console.log(activeUser);

            if (activeUser == null) {
              $('.project-card').css('height', '370px');
            }
          })
        ];
      },

      doneCallback = function() {
        $('.hasVideo').click(function() {
          var $this = $(this);
          $this.addClass('has_played');
          var video = $this.find('video')[0];

          video.addEventListener('ended', function(){
            $this.removeClass('has_played playing stopped');
          });

          config.mainCarousel.carousel('pause'); //Pause the slideshow

          if(video.paused === false) {
            video.pause();
            $this.addClass('stopped').removeClass('playing');
          }
          else {
            video.play();
            $this.addClass('playing').removeClass('stopped');
          }
        });
        console.log('all done');
      },

      failCallback = function() {
        console.log('fail!');
      },

      render = function(data, template, target) {
        target.html($.parseHTML(app.Templates[template](data)))
      },

      init = function() {
        $.ajaxSetup({
          beforeSend: function(jqXHR) {
            jqXHR.setRequestHeader('cf-request-type', 'ajax');
          },
          statusCode: {
            403: function() {
              location.href = '/login'
            }
          }
        });

        $.when.apply($, getProjects()).done(function() {
          doneCallback();
        })
          .fail(function() {
            failCallback();
          });
      };

    return {
      start: init
    }

  }(jQuery, window.app));
}());
