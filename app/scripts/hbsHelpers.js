(function(Handlebars){
  'use strict';
  function checkCondition(v1, operator, v2) {
    switch(operator) {
      case '==':
        return (v1 == v2);
      case '===':
        return (v1 === v2);
      case '!==':
        return (v1 !== v2);
      case '<':
        return (v1 < v2);
      case '<=':
        return (v1 <= v2);
      case '>':
        return (v1 > v2);
      case '>=':
        return (v1 >= v2);
      case '&&':
        return (v1 && v2);
      case '||':
        return (v1 || v2);
      default:
        return false;
    }
  }

  Handlebars.registerHelper('compare', function(lvalue, rvalue, options) {

    if (arguments.length < 3)
      throw new Error("Handlerbars Helper 'compare' needs 2 parameters");

    var operator = options.hash.operator || "==";

    var operators = {
      '==':       function(l,r) { return l == r; },
      '===':      function(l,r) { return l === r; },
      '!=':       function(l,r) { return l != r; },
      '<':        function(l,r) { return l < r; },
      '>':        function(l,r) { return l > r; },
      '<=':       function(l,r) { return l <= r; },
      '>=':       function(l,r) { return l >= r; },
      'typeof':   function(l,r) { return typeof l == r; }
    }

    if (!operators[operator])
      throw new Error("Handlerbars Helper 'compare' doesn't know the operator "+operator);

    var result = operators[operator](lvalue,rvalue);

    if( result ) {
      return options.fn(this);
    } else {
      return options.inverse(this);
    }

  });

  Handlebars.registerHelper('ifCond', function (v1, operator, v2, options) {
    return checkCondition(v1, operator, v2)
      ? options.fn(this)
      : options.inverse(this);
  });

   Handlebars.registerHelper('math', function(lvalue, operator, rvalue, options) {
     lvalue = parseFloat(lvalue);
     rvalue = parseFloat(rvalue);

     return {
       '+': lvalue + rvalue,
       '-': lvalue - rvalue,
       '*': lvalue * rvalue,
       '/': lvalue / rvalue,
       '%': lvalue % rvalue
     }[operator];
   });

    Handlebars.registerHelper('getMonth', function(passedString, value) {
      var theString = Number(passedString.substring(3,5)).toString();
      if (theString == value) {
        return 'selected="selected"';
      }
      else {
        return '';
      }
    });

    Handlebars.registerHelper('getYear', function(passedString, value) {
      var theString = passedString.substring(6,10);
      if (theString == value) {
        return 'selected="selected"';
      }
      else {
        return '';
      }
    });

    Handlebars.registerHelper('limitCount', function(currentValue) {
      return typeof (currentValue) !== 'undefined' ? 'checked="checked"': '';
    });

    Handlebars.registerHelper('setLimit', function(currentValue) {
      return typeof (currentValue) !== 'undefined' ? 'limit checkbox limit-set': 'limit checkbox';
    });

    Handlebars.registerHelper('isEmpty', function(passedString, defaultValue) {
      return passedString !== '' && typeof passedString !== 'undefined' ? passedString : defaultValue;
    });

    Handlebars.registerHelper('addString', function(currentValue, string) {
      return currentValue+string;
    });

  Handlebars.registerHelper('progressWidth', function(procent) {
    return procent <= 100 ? procent + '%' : '100%';
  });

    Handlebars.registerHelper('isAvailable', function(currentValue) {
      return currentValue > 0 ? 'hover-group js-reward-available reward reward--available': 'hover-group reward reward--all-gone';
    });

    Handlebars.registerHelper('setDate', function(fullDate) {
      var months = ['Ocak', 'Şubat', 'Mart', 'Nisan', 'Mayıs', 'Haziran', 'Temmuz', 'Ağustos', 'Eylül', 'Ekim', 'Kasım', 'Aralık'],
        dateArr = fullDate.split('.');
        return months[dateArr[1]-1]  + ' ' + dateArr[2];
    });

  Handlebars.registerHelper('setDateWithHours', function(fullDate) {
    var date = fullDate.substr(0,10),
      months = ['Ocak', 'Şubat', 'Mart', 'Nisan', 'Mayıs', 'Haziran', 'Temmuz', 'Ağustos', 'Eylül', 'Ekim', 'Kasım', 'Aralık'],
      dateArr = date.split('.');
    return dateArr[0]+ ' ' + months[dateArr[1]-1]  + ' ' + dateArr[2];
  });

  Handlebars.registerHelper('isPopular', function(currentValue) {
    return parseInt(currentValue) > 9999 ? 'col-xs-12 stat-item popular': 'col-xs-12 stat-item';
  });

  Handlebars.registerHelper('isCreatorComment', function(value) {
    return value ? 'comments-content__item creatorComment': 'comments-content__item';
  });

  Handlebars.registerHelper('formatMoney', function(amount) {
    return typeof amount !== 'undefined' ? (amount).formatMoney(0, 3, '.', ',') : '';
  });

  Handlebars.registerHelper('progressbar', function(width) {
    if(width >= 50) {
      return 'project-percent-pledged progress-bar gte50';
    }
    else if (width >= 75) {
      return 'project-percent-pledged progress-bar gte75';
    }
    else {
      return 'project-percent-pledged progress-bar';
    }
  });

  Handlebars.registerHelper('animationDelay', function(index) {
    return (2.5*parseInt(index+1))/100 +'s';
  });

  Handlebars.registerHelper('ternary', function(statement, trueValue, falseValue) {
    return statement ? trueValue : falseValue;
  });

  Handlebars.registerHelper('memType', function(type, value) {
    if(type == value) {
      return 'checked=checked';
    }
  });

  Handlebars.registerHelper('isTranscoding', function(video) {
    return video === 'transcoding' ? 'display: block' : 'display: none';
  });

  Handlebars.registerHelper('isVideoReady', function(video) {
    return video !== 'transcoding' && video !== '' && typeof video !== 'undefined' ? 'display: block' : 'display: none';
  });

  Handlebars.registerHelper('projectStatus', function(status) {
    switch(parseInt(status)) {
      case 0:
        return contentStrings.fillprojectdetailspage1tabsend;
        break;
      case 1:
        return 'Onay Bekliyor';
        break;
      case 2:
        return 'Çılgın Oylama Süreci';
        break;
      case 3:
        return contentStrings.projectbasicInfostatusopen;
        break;
      case 4:
      case 5:
        return 'Proje Kapandı';
        break;
      case 6:
        return contentStrings.projectstatusdiscard;
        break;
      case 7:
        return 'Pasif Proje';
        break;
      case 10:
        return contentStrings.fillprojectdetailspage1tabappeal;
        break;
      case 11:
        return contentStrings.fillprojectdetailspage1tabevaluation;
        break;
      case 12:
        return 'Toplantı Bekleniyor';
        break;
      case 13:
        return contentStrings.projectstatussendmeeting;
        break;
      case 14:
        return 'Başvuru Red';
        break;
      case 15:
        return contentStrings.fillprojectdetailspage1tabrejection;
        break;
      default:
        return 'Bilinmiyor';
    }
  });

  Handlebars.registerHelper('badgeHelper', function(status) {
    if(parseInt(status)===2) {
      return 'thinking-badge';
    }
    else if(parseInt(status)===3) {
      return 'funding-badge';
    }
    else if(parseInt(status)===4) {
      return 'success-badge';
    }
    else if(parseInt(status)===5) {
      return 'fail-badge';
    }
    else {
      return false;
    }
  });

  Handlebars.registerHelper('statusHelper', function(status) {
    if(parseInt(status)==2) {
      return 'status2-icon';
    }
    else if(parseInt(status)==3) {
      return 'status3-icon';
    }
    else if(parseInt(status)==4) {
      return 'status4-icon';
    }
    else if(parseInt(status)==5) {
      return 'status5-icon';
    }
  });

  Handlebars.registerHelper('statusTooltipHelper', function(status) {
      if(parseInt(status)==2) {
        return 'Proje Düşünme Aşamasında';
      }
      else if(parseInt(status)==3) {
        return 'Proje Canlı';
      }
      else if(parseInt(status)==4) {
        return contentStrings.profilebackedprojectssuccessdesc;
      }
      else if(parseInt(status)==5) {
        return 'Projemiz yeterli desteğe ulaşamamıştır. Gelişmeleri aktivitelerden takip edebilirsiniz.';
      }
  });

    // more handlebar helpers...
  })(Handlebars);
