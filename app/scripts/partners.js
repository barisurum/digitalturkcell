/**
 * Created by barisurum on 26/05/16.
 */

$(document).ready(function() {
  var modules = $('.module-partner').toArray();
  for (var cnt = 0; cnt < modules.length; ++cnt) {
    console.log(cnt);
    if (cnt === 0 || (cnt % 5) === 0) {
      $(modules[cnt]).addClass('module-partner-two');
      $(modules[cnt + 1]).addClass('module-partner-two').css('float', 'right');
      $(modules[cnt + 2]).addClass('module-partner-three three-first');
      $(modules[cnt + 3]).addClass('module-partner-three');
      $(modules[cnt + 4]).addClass('module-partner-three');
    }
  }
  $('#partners-container').fadeIn('slow');
});
