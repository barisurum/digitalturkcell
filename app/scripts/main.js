(function() {
  'use strict';
  window.app = window.app || {};
  window.app.mainModule = (function () {
    return {
      formatMoney: function (number, places, symbol, thousand, decimal) {
        number = number || 0;
        places = !isNaN(places = Math.abs(places)) ? places : 2;
        symbol = symbol !== undefined ? symbol : 'TL';
        thousand = thousand || '.';
        decimal = decimal || ',';
        var negative = number < 0 ? '-' : '',
          i = parseInt(number = Math.abs(+number || 0).toFixed(places), 10) + '',
          j = i.length > 3 ? j % 3 : 0;
        return negative + (j ? i.substr(0, j) + thousand : '') + i.substr(j).replace(/(\d{3})(?=\d)/g, '1TL' + thousand) + (places ? decimal + Math.abs(number - i).toFixed(places).slice(2) : '') + symbol;
      }
    }
  });
});
