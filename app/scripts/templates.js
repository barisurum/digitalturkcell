(function() {
  window.app = window.app || {};
  window.app.models = window.app.models || {};
  window.app.Templates = (function (Handlebars) {  var template = Handlebars.template, templates = Handlebars.templates = Handlebars.templates || {};
templates['about-main'] = template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=container.lambda, alias2=container.escapeExpression, alias3=depth0 != null ? depth0 : {}, alias4=helpers.helperMissing, alias5="function";

  return "<div class=\"center block\">\n    <h2 class=\"h2 green-dark fwbold\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.fillprojectdetailspage4textboxtitle : stack1), depth0))
    + "</h2>\n    <p class=\"h4 mt0 green-dark\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.fillprojectdetailspage4textboxsubtitle : stack1), depth0))
    + "</p>\n</div>\n\n<div class=\"table-display full-width mt60 mb30 ptb20 plr10 m-center bg-white border\">\n    <div class=\"row\">\n        <div class=\"col-xs-9\">\n            <div id=\"about-main\" class=\"project-main-form mr10\" data-parsley-validate>\n\n              <input type=\"hidden\" id=\"userPhoto\" value=\""
    + alias2(((helper = (helper = helpers.image || (depth0 != null ? depth0.image : depth0)) != null ? helper : alias4),(typeof helper === alias5 ? helper.call(alias3,{"name":"image","hash":{},"data":data}) : helper)))
    + "\" />\n              <ol class=\"fields no-margin\">\n                <li class=\"profile-photo\">\n                    <div class=\"grey-field mb20 clearfix\">\n                        <label for=\"photoUpload\">\n                            "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.fillprojectdetailspage4textboxprofilepictitle : stack1), depth0))
    + "\n                        </label>\n                        <div class=\"field-wrapper profile-photo\">\n                            <div class=\"pull-left success table-cell\">\n                                <div class=\"image-clip\">\n                                    <img alt=\"Fb profile picture.original\" class=\"preview circle image-clip-img\" src=\""
    + ((stack1 = (helpers.isEmpty || (depth0 && depth0.isEmpty) || alias4).call(alias3,(depth0 != null ? depth0.image : depth0),"/images/placeholders/profile.png",{"name":"isEmpty","hash":{},"data":data})) != null ? stack1 : "")
    + "\" />\n                                </div>\n                            </div>\n                            <div id=\"profileUploader\" class=\"upload border\">\n                                <input class=\"photo file\" id=\"photoUpload\" data-parsley-required value=\""
    + alias2(((helper = (helper = helpers.image || (depth0 != null ? depth0.image : depth0)) != null ? helper : alias4),(typeof helper === alias5 ? helper.call(alias3,{"name":"image","hash":{},"data":data}) : helper)))
    + "\" />\n                                <strong class=\"center\">\n                                    "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.fillprojectdetailspage1textboxpicsubtext : stack1), depth0))
    + "\n                                </strong>\n                            </div>\n                            <div id=\"profileUploadSuccess\" class=\"field-help text-center\" style=\"display: none\">\n                                <p>Görseliniz başarılı ile yüklenmiştir.</p>\n                                <p>Yeni bir görsel yüklemek için var olanı kaldırmanız gerekmektedir.</p>\n                            </div>\n                            <!--<div id=\"progressUserPhoto\" class=\"progress green-dark center mt10\">\n                                <div class=\"progress-bar progress-bar-success\" style=\"width: 0\"></div>\n                            </div>-->\n                        </div>\n                    </div>\n                </li>\n                <li class=\"profile-name\">\n                    <div class=\"grey-field mb20 clearfix\">\n                        <label for=\"name\">\n                          "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.fillprojectdetailspage4textboxnametitle : stack1), depth0))
    + "\n                        </label>\n                        <div class=\"field-wrapper\">\n                            <input type=\"text\" id=\"name\" class=\"form-control border\" maxlength=\"60\" value=\""
    + alias2(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias4),(typeof helper === alias5 ? helper.call(alias3,{"name":"name","hash":{},"data":data}) : helper)))
    + "\" data-parsley-required />\n                            <div class=\"field-help\">\n                                <p>"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.fillprojectdetailspage4textboxnamesubtitle : stack1), depth0))
    + "</p>\n                            </div>\n                        </div>\n                    </div>\n                </li>\n                <li class=\"profile-last-name\">\n                    <div class=\"grey-field mb20 clearfix\">\n                        <label for=\"surname\">\n                            "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.fillprojectdetailspage4textboxsurnametitle : stack1), depth0))
    + "\n                        </label>\n                        <div class=\"field-wrapper\">\n                            <input type=\"text\" id=\"surname\" class=\"form-control border\" maxlength=\"60\" value=\""
    + alias2(((helper = (helper = helpers.surname || (depth0 != null ? depth0.surname : depth0)) != null ? helper : alias4),(typeof helper === alias5 ? helper.call(alias3,{"name":"surname","hash":{},"data":data}) : helper)))
    + "\" data-parsley-required />\n                            <div class=\"field-help\">\n                                <p>"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.fillprojectdetailspage4textboxsurnamesubtitle : stack1), depth0))
    + "</p>\n                            </div>\n                        </div>\n                    </div>\n                </li>\n                <li class=\"profile-email\">\n                    <div class=\"grey-field mb20 clearfix\">\n                        <label for=\"email\">\n                          "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.fillprojectdetailspage4textboxemailtitle : stack1), depth0))
    + "\n                        </label>\n                        <div class=\"field-wrapper\">\n                            <input type=\"text\" id=\"email\" class=\"form-control border\" maxlength=\"60\" value=\""
    + alias2(((helper = (helper = helpers.email || (depth0 != null ? depth0.email : depth0)) != null ? helper : alias4),(typeof helper === alias5 ? helper.call(alias3,{"name":"email","hash":{},"data":data}) : helper)))
    + "\" data-parsley-required />\n                            <div class=\"field-help\">\n                                <p>"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.fillprojectdetailspage4textboxemailsubtitle : stack1), depth0))
    + "</p>\n                            </div>\n                        </div>\n                    </div>\n                </li>\n                <li class=\"profile-phone\">\n                    <div class=\"grey-field mb20 clearfix\">\n                        <label for=\"phone\">\n                          "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.fillprojectdetailspage4textboxmobilephonetitle : stack1), depth0))
    + "\n                        </label>\n                        <div class=\"field-wrapper\">\n                            <input type=\"text\" id=\"phone\" class=\"form-control border\" maxlength=\"60\" value=\""
    + alias2(((helper = (helper = helpers.phone || (depth0 != null ? depth0.phone : depth0)) != null ? helper : alias4),(typeof helper === alias5 ? helper.call(alias3,{"name":"phone","hash":{},"data":data}) : helper)))
    + "\" data-parsley-required />\n                        </div>\n                    </div>\n                </li>\n                <li class=\"biography\">\n                    <div class=\"grey-field mb20 clearfix\">\n                        <label for=\"biography\">\n                          "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.fillprojectdetailspage4textboxbiotitle : stack1), depth0))
    + "\n                        </label>\n                        <div class=\"field-wrapper\">\n                            <textarea id=\"biography\" rows=\"4\" class=\"form-control border\" maxlength=\"135\" data-parsley-required>"
    + alias2(((helper = (helper = helpers.biography || (depth0 != null ? depth0.biography : depth0)) != null ? helper : alias4),(typeof helper === alias5 ? helper.call(alias3,{"name":"biography","hash":{},"data":data}) : helper)))
    + "</textarea>\n                        </div>\n                    </div>\n                </li>\n                <li class=\"city\">\n                    <div class=\"grey-field mb20 clearfix\">\n                        <label for=\"location\">\n                          "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.fillprojectdetailspage4textboxlocationtitle : stack1), depth0))
    + "\n                        </label>\n                        <div class=\"field-wrapper\">\n                            <input type=\"text\" class=\"form-control border cityName\" maxlength=\"61\" autocomplete=\"off\" required  data-parsley-required-message=\"Lokasyonunu giriniz\" value=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.city : depth0)) != null ? stack1.name : stack1), depth0))
    + "\" />\n                            <input type=\"text\" id=\"location\" class=\"cityId\" value=\""
    + alias2(((helper = (helper = helpers.cityId || (depth0 != null ? depth0.cityId : depth0)) != null ? helper : alias4),(typeof helper === alias5 ? helper.call(alias3,{"name":"cityId","hash":{},"data":data}) : helper)))
    + "\" style=\"display: none\" required data-parsley-required-message=\"Lokasyonunu giriniz\" />\n                        </div>\n                    </div>\n                </li>\n                <li class=\"website\">\n                    <div class=\"grey-field mb20 clearfix\">\n                        <label for=\"website\">\n                          "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.fillprojectdetailspage4textboxwebsitetitle : stack1), depth0))
    + "\n                        </label>\n                        <div class=\"field-wrapper\">\n                            <input type=\"text\" id=\"website\" class=\"form-control border\" maxlength=\"60\" value=\""
    + alias2(((helper = (helper = helpers.website || (depth0 != null ? depth0.website : depth0)) != null ? helper : alias4),(typeof helper === alias5 ? helper.call(alias3,{"name":"website","hash":{},"data":data}) : helper)))
    + "\" data-parsley-required />\n                            <div class=\"field-help\">\n                                <p>"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.fillprojectdetailspage4textboxwebsitesubtitle : stack1), depth0))
    + "</p>\n                            </div>\n                        </div>\n                    </div>\n                </li>\n              </ol>\n\n            </div>\n        </div>\n        <div class=\"col-xs-3\">\n            <div id=\"about-sidebar\" class=\"project-sidebar-form ml10\">\n                <div class=\"project-thumbnail\">\n\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n";
},"useData":true});
templates['basics'] = template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=container.lambda, alias2=container.escapeExpression, alias3=depth0 != null ? depth0 : {}, alias4=helpers.helperMissing, alias5="function";

  return "<div class=\"center block\">\n    <h2 class=\"h2 green-dark fwbold\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.fillprojectdetailspage1textboxtitle : stack1), depth0))
    + "</h2>\n    <p class=\"h4 mt0 green-dark\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.fillprojectdetailspage1textboxsubtitle : stack1), depth0))
    + "</p>\n</div>\n<div class=\"table-display full-width mt60 mb30 ptb20 plr10 m-center bg-white border\">\n    <div class=\"row\">\n        <div class=\"col-xs-9\">\n            <div id=\"basics-main\" class=\"project-main-form mr10\">\n                <form id=\"basics-form\" data-parsley-validate>\n                  <input type=\"hidden\" id=\"projectImage\" value=\""
    + alias2((helpers.isEmpty || (depth0 && depth0.isEmpty) || alias4).call(alias3,(depth0 != null ? depth0.image : depth0),"/images/placeholders/project.jpg",{"name":"isEmpty","hash":{},"data":data}))
    + "\" />\n                  <input type=\"hidden\" id=\"successProjectImage\" value=\""
    + alias2((helpers.isEmpty || (depth0 && depth0.isEmpty) || alias4).call(alias3,(depth0 != null ? depth0.image : depth0),"/images/placeholders/project.jpg",{"name":"isEmpty","hash":{},"data":data}))
    + "\" />\n                  <ol class=\"fields no-margin\">\n                      <li class=\"project-image\">\n                          <div class=\"grey-field mb20 clearfix\">\n                              <label for=\"fileupload\">\n                                  "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.fillprojectdetailspage1textboxpictext : stack1), depth0))
    + "\n                              </label>\n                              <div class=\"field-wrapper\">\n                                  <div id=\"uploader\" class=\"upload\">\n                                      <input class=\"photo file\" id=\"fileupload\" name=\"image\" value=\""
    + alias2(((helper = (helper = helpers.image || (depth0 != null ? depth0.image : depth0)) != null ? helper : alias4),(typeof helper === alias5 ? helper.call(alias3,{"name":"image","hash":{},"data":data}) : helper)))
    + "\" />\n                                      <strong class=\"center\">\n                                        "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.fillprojectdetailspage1textboxpicsubtext : stack1), depth0))
    + "\n                                      </strong>\n                                  </div>\n                                  <div id=\"uploadSuccess\" class=\"field-help text-center\" style=\"display: none\">\n                                      <p class=\"mb10\">Görseliniz başarılı ile yüklenmiştir.</p>\n                                      <p>Yeni bir görsel yüklemek için var olanı kaldırmanız gerekmektedir.</p>\n                                  </div>\n                              </div>\n                          </div>\n                      </li>\n\n                      <li class=\"project-title\">\n                          <div class=\"grey-field mb20 clearfix\">\n                              <label for=\"projectTitle\">\n                                  "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.fillprojectdetailspage1textboxtitletext : stack1), depth0))
    + " *\n                              </label>\n                              <div class=\"field-wrapper\">\n                                  <input type=\"text\" id=\"projectTitle\" name=\"projectTitle\" value=\""
    + alias2(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias4),(typeof helper === alias5 ? helper.call(alias3,{"name":"name","hash":{},"data":data}) : helper)))
    + "\" class=\"form-control border remaining\" maxlength=\"50\" required />\n                                  <div class=\"field-help\">\n                                    "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.fillprojectdetailspage1textboxtitlesubtext : stack1), depth0))
    + "\n                                  </div>\n                              </div>\n                              <div class=\"field-remaining pull-right mr20 t10 fwbold\">Kalan</div>\n                          </div>\n                      </li>\n                      <li class=\"short-description\">\n                          <div class=\"grey-field mb20 clearfix\">\n                              <label for=\"shortDescription\">\n                                "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.fillprojectdetailspage2textboxdesctext : stack1), depth0))
    + " *\n                                  <span class=\"grey-field__info-box t10\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.fillprojectdetailspage1textboxshortdesccoloredsubtext : stack1), depth0))
    + "</span>\n                              </label>\n                              <div class=\"field-wrapper\">\n                                  <textarea id=\"shortDescription\" rows=\"4\" class=\"form-control border remaining\" maxlength=\"500\" required>"
    + alias2(((helper = (helper = helpers.shortDescription || (depth0 != null ? depth0.shortDescription : depth0)) != null ? helper : alias4),(typeof helper === alias5 ? helper.call(alias3,{"name":"shortDescription","hash":{},"data":data}) : helper)))
    + "</textarea>\n                                  <div class=\"field-help\">\n                                      <p>"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.projectdetailexplaintweet : stack1), depth0))
    + "</p>\n                                  </div>\n                              </div>\n                              <div class=\"field-remaining pull-right mr20 t10 fwbold\">Kalan</div>\n                          </div>\n                      </li>\n\n                      <!-- sss --!-->\n\n\n                      <li class=\"project-sector\">\n                          <div class=\"grey-field mb20 clearfix\">\n                              <label for=\"projectSector\">\n                                  Proje Sektörü\n                              </label>\n                              <div class=\"field-wrapper\">\n                                  <input type=\"text\" id=\"projectSector\" name=\"projectSector\" value=\""
    + alias2(((helper = (helper = helpers.projectSector || (depth0 != null ? depth0.projectSector : depth0)) != null ? helper : alias4),(typeof helper === alias5 ? helper.call(alias3,{"name":"projectSector","hash":{},"data":data}) : helper)))
    + "\" class=\"form-control border remaining\" maxlength=\"50\" />\n                                  <div class=\"field-help\">\n                                      Telekomünikasyon , Bankacılık, Otomotiv, Savunma sanayi, Sağlık, Enerji, Eğitim, Havacılık ve Uzay, Elektronik, Eğlence/Medya, Sosyal Sorumluluk, Diğer\n                                  </div>\n                              </div>\n                              <div class=\"field-remaining pull-right mr20 t10 fwbold\">Kalan</div>\n                          </div>\n                      </li>\n                      <li class=\"project-category\">\n                          <div class=\"grey-field mb20 clearfix\">\n                              <label for=\"projectCategory\">\n                                  Proje Kategorisi *\n                              </label>\n                              <div class=\"field-wrapper\">\n                                  <input type=\"text\" id=\"projectCategory\" name=\"projectCategory\" value=\""
    + alias2(((helper = (helper = helpers.projectCategory || (depth0 != null ? depth0.projectCategory : depth0)) != null ? helper : alias4),(typeof helper === alias5 ? helper.call(alias3,{"name":"projectCategory","hash":{},"data":data}) : helper)))
    + "\" class=\"form-control border remaining\" maxlength=\"50\" required />\n                                  <div class=\"field-help\">\n                                      Yazılım, Donanım, Giyilebilir Tek., Oyun, Robotik, IOT, Medya Tek., Kurumsal Çözüm, Mobil Uyg., Web Uyg., Diğer\n                                  </div>\n                              </div>\n                              <div class=\"field-remaining pull-right mr20 t10 fwbold\">Kalan</div>\n                          </div>\n                      </li>\n                      <li class=\"project-deger\">\n                          <div class=\"grey-field mb20 clearfix\">\n                              <label for=\"projectDeger\">\n                                  Projenle müşteriye sağlayacağın değer nedir?\n                              </label>\n                              <div class=\"field-wrapper\">\n                                  <input type=\"text\" id=\"question1\" name=\"question1\" value=\""
    + alias2(((helper = (helper = helpers.question1 || (depth0 != null ? depth0.question1 : depth0)) != null ? helper : alias4),(typeof helper === alias5 ? helper.call(alias3,{"name":"question1","hash":{},"data":data}) : helper)))
    + "\" class=\"form-control border remaining\" maxlength=\"200\" />\n                                  <div class=\"field-help\">\n                                  </div>\n                              </div>\n                              <div class=\"field-remaining pull-right mr20 t10 fwbold\">Kalan</div>\n                          </div>\n                      </li>\n                      <li class=\"project-deger\">\n                          <div class=\"grey-field mb20 clearfix\">\n                              <label for=\"projectDeger\">\n                                  Projen müşterinin hangi problemini çözüyor veya ihtiyacını karşılıyor?\n                              </label>\n                              <div class=\"field-wrapper\">\n                                  <input type=\"text\" id=\"question2\" name=\"question2\" value=\""
    + alias2(((helper = (helper = helpers.question2 || (depth0 != null ? depth0.question2 : depth0)) != null ? helper : alias4),(typeof helper === alias5 ? helper.call(alias3,{"name":"question2","hash":{},"data":data}) : helper)))
    + "\" class=\"form-control border remaining\" maxlength=\"200\" />\n                                  <div class=\"field-help\">\n                                  </div>\n                              </div>\n                              <div class=\"field-remaining pull-right mr20 t10 fwbold\">Kalan</div>\n                          </div>\n                      </li>\n                      <li class=\"project-deger\">\n                          <div class=\"grey-field mb20 clearfix\">\n                              <label for=\"projectDeger\">\n                                  Hedef müşteri kitlesi kim?\n                              </label>\n                              <div class=\"field-wrapper\">\n                                  <input type=\"text\" id=\"question3\" name=\"question3\" value=\""
    + alias2(((helper = (helper = helpers.question3 || (depth0 != null ? depth0.question3 : depth0)) != null ? helper : alias4),(typeof helper === alias5 ? helper.call(alias3,{"name":"question3","hash":{},"data":data}) : helper)))
    + "\" class=\"form-control border remaining\" maxlength=\"200\" />\n                                  <div class=\"field-help\">\n                                  </div>\n                              </div>\n                              <div class=\"field-remaining pull-right mr20 t10 fwbold\">Kalan</div>\n                          </div>\n                      </li>\n                      <li class=\"project-deger\">\n                          <div class=\"grey-field mb20 clearfix\">\n                              <label for=\"projectDeger\">\n                                  Rakipleriniz var mı? Varsa projeni diğerlerinden ayıran özellikler neler?\n                              </label>\n                              <div class=\"field-wrapper\">\n                                  <input type=\"text\" id=\"question4\" name=\"question4\" value=\""
    + alias2(((helper = (helper = helpers.question4 || (depth0 != null ? depth0.question4 : depth0)) != null ? helper : alias4),(typeof helper === alias5 ? helper.call(alias3,{"name":"question4","hash":{},"data":data}) : helper)))
    + "\" class=\"form-control border remaining\" maxlength=\"200\" />\n                                  <div class=\"field-help\">\n                                  </div>\n                              </div>\n                              <div class=\"field-remaining pull-right mr20 t10 fwbold\">Kalan</div>\n                          </div>\n                      </li>\n                      <li class=\"project-deger\">\n                          <div class=\"grey-field mb20 clearfix\">\n                              <label for=\"projectDeger\">\n                                  Projenin maddi veya manevi yönleriyle gelir modelini anlat\n                              </label>\n                              <div class=\"field-wrapper\">\n                                  <input type=\"text\" id=\"question5\" name=\"question5\" value=\""
    + alias2(((helper = (helper = helpers.question5 || (depth0 != null ? depth0.question5 : depth0)) != null ? helper : alias4),(typeof helper === alias5 ? helper.call(alias3,{"name":"question5","hash":{},"data":data}) : helper)))
    + "\" class=\"form-control border remaining\" maxlength=\"200\" />\n                                  <div class=\"field-help\">\n                                  </div>\n                              </div>\n                              <div class=\"field-remaining pull-right mr20 t10 fwbold\">Kalan</div>\n                          </div>\n                      </li>\n                  </ol>\n                </form>\n            </div>\n        </div>\n        <div class=\"col-xs-3\">\n            <div id=\"basics-sidebar\" class=\"project-sidebar-form ml10\">\n                <div class=\"project-thumbnail\">\n                      <img alt=\"Project image\" class=\"project-thumbnail-img\" src=\""
    + alias2((helpers.isEmpty || (depth0 && depth0.isEmpty) || alias4).call(alias3,(depth0 != null ? depth0.image : depth0),"/images/placeholders/project.jpg",{"name":"isEmpty","hash":{},"data":data}))
    + "\" width=\"100%\" />\n                </div>\n                <p class=\"green-dark mt40\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.fillprojectdetailspage1textboxshortdesccoloredsubtext : stack1), depth0))
    + "</p>\n            </div>\n        </div>\n        <input type=\"hidden\" id=\"status\" value=\""
    + alias2(((helper = (helper = helpers.status || (depth0 != null ? depth0.status : depth0)) != null ? helper : alias4),(typeof helper === alias5 ? helper.call(alias3,{"name":"status","hash":{},"data":data}) : helper)))
    + "\" />\n    </div>\n</div>\n";
},"useData":true});
templates['dashboard-activities-new-update'] = template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=container.lambda, alias2=container.escapeExpression;

  return "    <div class=\"inline-block full-width ptb15 white mb10 mt20 bg-yellow-dark box-shadow\">\n        <div class=\"col-xs-2\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.profilepageaddtimelineactivitiestablecolumns1 : stack1), depth0))
    + "</div>\n        <div class=\"col-xs-3\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.profilepageaddtimelineactivitiestablecolumns2 : stack1), depth0))
    + "</div>\n        <div class=\"col-xs-4\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.profilepageaddtimelineactivitiestablecolumns3 : stack1), depth0))
    + "</div>\n        <div class=\"col-xs-2 text-center\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.profilepageaddtimelineactivitiestablecolumns4 : stack1), depth0))
    + "</div>\n        <div class=\"col-xs-1 text-center\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.profilepageaddtimelineactivitiestablecolumns5 : stack1), depth0))
    + "</div>\n    </div>\n";
},"3":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : {}, alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression, alias5=container.lambda;

  return "    <div class=\"dashboard-item__item backed-projects fadeInDown\" style=\"animation-delay: "
    + ((stack1 = (helpers.animationDelay || (depth0 && depth0.animationDelay) || alias2).call(alias1,(data && data.index),{"name":"animationDelay","hash":{},"data":data})) != null ? stack1 : "")
    + "\">\n        <div class=\"row\">\n            <div class=\"col-xs-2\">\n                <span class=\"h5 green-dark block m0 absolute valign fwnormal\">"
    + ((stack1 = (helpers.setDateWithHours || (depth0 && depth0.setDateWithHours) || alias2).call(alias1,(depth0 != null ? depth0.created : depth0),{"name":"setDateWithHours","hash":{},"data":data})) != null ? stack1 : "")
    + "</span>\n            </div>\n            <div class=\"col-xs-3\">\n                <span class=\"h5 green-dark block m0 absolute valign fwnormal\">"
    + alias4(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"title","hash":{},"data":data}) : helper)))
    + "</span>\n            </div>\n            <div class=\"col-xs-4\">\n                <span class=\"h5 green-dark block m0 absolute valign fwnormal\">"
    + alias4(((helper = (helper = helpers.description || (depth0 != null ? depth0.description : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"description","hash":{},"data":data}) : helper)))
    + "</span>\n            </div>\n            <div class=\"col-xs-2 text-center\">\n                <button data-activity-id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" title=\""
    + alias4(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"title","hash":{},"data":data}) : helper)))
    + " aktivitesini düzenle\" data-title=\""
    + alias4(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"title","hash":{},"data":data}) : helper)))
    + "\" data-description=\""
    + alias4(((helper = (helper = helpers.description || (depth0 != null ? depth0.description : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"description","hash":{},"data":data}) : helper)))
    + "\"\n                        class=\"edit btn btn--white green-dark--link absolute align-center fwnormal\">"
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.profilepageaddtimelineactivitiestablecolumns4 : stack1), depth0))
    + "\n                </button>\n            </div>\n            <div class=\"col-xs-1 text-center\">\n                <button data-activity-id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" title=\""
    + alias4(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"title","hash":{},"data":data}) : helper)))
    + " aktivitesini sil\" data-title=\""
    + alias4(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"title","hash":{},"data":data}) : helper)))
    + "\"\n                        class=\"delete btn btn--yellow green-dark--link absolute align-center fwnormal\">"
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.profilepageaddtimelineactivitiestablecolumns5 : stack1), depth0))
    + "\n                </button>\n            </div>\n        </div>\n    </div>\n";
},"5":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "    <p class=\"mt20\">"
    + container.escapeExpression(container.lambda(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.projectdetailspagetimelinesubtextnothing : stack1), depth0))
    + "</p>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : {}, alias2=container.lambda, alias3=container.escapeExpression;

  return ((stack1 = helpers["if"].call(alias1,depth0,{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\n"
    + ((stack1 = helpers.each.call(alias1,depth0,{"name":"each","hash":{},"fn":container.program(3, data, 0),"inverse":container.program(5, data, 0),"data":data})) != null ? stack1 : "")
    + "\n<h3 id=\"titleAddUpdate\" class=\"mt50 mb20 fwnormal green-dark\">"
    + alias3(alias2(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.profilepageaddtimelineactivitiestitle : stack1), depth0))
    + "</h3>\n\n<div class=\"full-width inline-block relative mb50\">\n    <div class=\"dashboard-item--message-detail pull-left\">\n        <input id=\"activityTitle\" class=\"primary-form mb10 box-shadow\" placeholder=\""
    + alias3(alias2(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.profilepageaddtimelineactivitiestextbox1innertext : stack1), depth0))
    + "\" />\n        <textarea id=\"activityDescription\" class=\"primary-form box-shadow\" rows=\"4\" placeholder=\""
    + alias3(alias2(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.profilepageaddtimelineactivitiestextbox2innertext : stack1), depth0))
    + "\"></textarea>\n    </div>\n    <button id=\"clearActivity\" class=\"btn btn--small btn--yellow pull-right plr15 absolute fwnormal green-dark--link\"\n            style=\"right: 0; bottom: 59px;\">"
    + alias3(alias2(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.profilepageaddtimelineactivitiesbuttontext1 : stack1), depth0))
    + "\n    </button>\n    <button id=\"btnAddUpdateActivity\" class=\"btn btn--small btn--yellow pull-right plr15 absolute fwnormal green-dark--link\"\n            style=\"right: 0; bottom: 7px;\">"
    + alias3(alias2(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.profilepageaddtimelineactivitiesbuttontext2 : stack1), depth0))
    + "\n    </button>\n</div>\n";
},"useData":true});
templates['dashboard-activities'] = template({"1":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1, helper, alias1=depth0 != null ? depth0 : {}, alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div class=\"dashboard-item__item fadeInDown\" style=\"animation-delay: "
    + ((stack1 = (helpers.animationDelay || (depth0 && depth0.animationDelay) || alias2).call(alias1,(data && data.index),{"name":"animationDelay","hash":{},"data":data})) != null ? stack1 : "")
    + "\">\n    <div class=\"avatar-container mr20\">\n"
    + ((stack1 = helpers["with"].call(alias1,(depth0 != null ? depth0.project : depth0),{"name":"with","hash":{},"fn":container.program(2, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "    </div>\n    <div class=\"info-container info-container--dashboard\">\n        <span class=\"h5 fwnormal\">Güncelleme #"
    + ((stack1 = (helpers.math || (depth0 && depth0.math) || alias2).call(alias1,(depths[1] != null ? depths[1].length : depths[1]),"-",(data && data.index),{"name":"math","hash":{},"data":data})) != null ? stack1 : "")
    + "</span> <!--Array length - index-->\n        <span class=\"h6 fwnormal pull-right\">"
    + ((stack1 = (helpers.setDateWithHours || (depth0 && depth0.setDateWithHours) || alias2).call(alias1,(depth0 != null ? depth0.created : depth0),{"name":"setDateWithHours","hash":{},"data":data})) != null ? stack1 : "")
    + "</span>\n        <h4 class=\"green-dark mr10 fwnormal\">"
    + alias4(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"title","hash":{},"data":data}) : helper)))
    + "</h4>\n        <p class=\"h6\">"
    + alias4(((helper = (helper = helpers.description || (depth0 != null ? depth0.description : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"description","hash":{},"data":data}) : helper)))
    + "</p>\n    </div>\n</div>\n";
},"2":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : {}, alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "        <div class=\"bg-img item-photo\" style=\"background-image: url('"
    + alias4(((helper = (helper = helpers.image || (depth0 != null ? depth0.image : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"image","hash":{},"data":data}) : helper)))
    + "')\">\n            <img class=\"opacity0\" src=\""
    + alias4(((helper = (helper = helpers.image || (depth0 != null ? depth0.image : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"image","hash":{},"data":data}) : helper)))
    + "\" title=\""
    + alias4(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data}) : helper)))
    + "\" alt=\""
    + alias4(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data}) : helper)))
    + "\" />\n        </div>\n";
},"4":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "    <p>"
    + container.escapeExpression(container.lambda(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.acitivitiesnoactivity : stack1), depth0))
    + "</p>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0 != null ? depth0 : {},depth0,{"name":"each","hash":{},"fn":container.program(1, data, 0, blockParams, depths),"inverse":container.program(4, data, 0, blockParams, depths),"data":data})) != null ? stack1 : "");
},"useData":true,"useDepths":true});
templates['dashboard-backed-projects'] = template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=container.lambda, alias2=container.escapeExpression;

  return "<div class=\"inline-block full-width ptb15 white mb10 mt20 bg-yellow-dark box-shadow\">\n    <div class=\"col-xs-6\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.profilepagetabbackedprojectstablecolumns1 : stack1), depth0))
    + "</div>\n    <div class=\"col-xs-2\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.profilepagetabbackedprojectstablecolumns2 : stack1), depth0))
    + "</div>\n    <div class=\"col-xs-3 text-center\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.profilepagetabbackedprojectstablecolumns3 : stack1), depth0))
    + "</div>\n    <div class=\"col-xs-1 text-center\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.profilepagetabbackedprojectstablecolumns4 : stack1), depth0))
    + "</div>\n</div>\n";
},"3":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : {}, alias2=helpers.helperMissing, alias3=container.lambda, alias4=container.escapeExpression;

  return "  <div class=\"dashboard-item__item backed-projects fadeInDown\" style=\"animation-delay: "
    + ((stack1 = (helpers.animationDelay || (depth0 && depth0.animationDelay) || alias2).call(alias1,(data && data.index),{"name":"animationDelay","hash":{},"data":data})) != null ? stack1 : "")
    + "\">\n      <div class=\"row\">\n          <div class=\"col-xs-6\">\n              <div class=\"avatar-container mr20\">\n                  <div class=\"bg-img item-photo\" style=\"background-image: url('"
    + alias4(alias3(((stack1 = (depth0 != null ? depth0.project : depth0)) != null ? stack1.image : stack1), depth0))
    + "')\">\n                      <img class=\"opacity0\" src=\""
    + alias4(alias3(((stack1 = (depth0 != null ? depth0.project : depth0)) != null ? stack1.image : stack1), depth0))
    + "\" title=\""
    + alias4(alias3(((stack1 = (depth0 != null ? depth0.project : depth0)) != null ? stack1.name : stack1), depth0))
    + "\" alt=\""
    + alias4(alias3(((stack1 = (depth0 != null ? depth0.project : depth0)) != null ? stack1.name : stack1), depth0))
    + "\" />\n                  </div>\n              </div>\n              <h4 class=\"green-dark ml90 mr10 valign absolute project-title\"><a href=\"/projeler/"
    + alias4(alias3(((stack1 = (depth0 != null ? depth0.project : depth0)) != null ? stack1.urlPostfix : stack1), depth0))
    + "/detay\">"
    + alias4(alias3(((stack1 = (depth0 != null ? depth0.project : depth0)) != null ? stack1.name : stack1), depth0))
    + "</a></h4>\n          </div>\n\n          <div class=\"col-xs-2\">\n              <span class=\"h4 green-dark block valign absolute m0 ls-2 text-center fwnormal\">"
    + ((stack1 = (helpers.formatMoney || (depth0 && depth0.formatMoney) || alias2).call(alias1,(depth0 != null ? depth0.amount : depth0),{"name":"formatMoney","hash":{},"data":data})) != null ? stack1 : "")
    + " "
    + alias4(alias3(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.currency : stack1), depth0))
    + "</span>\n          </div>\n          <div class=\"col-xs-3 text-center\">\n              <p class=\"h5 fwnormal mt0\">"
    + alias4(alias3(((stack1 = (depth0 != null ? depth0.reward : depth0)) != null ? stack1.description : stack1), depth0))
    + "</p>\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.delivery : depth0),{"name":"if","hash":{},"fn":container.program(4, data, 0),"inverse":container.program(6, data, 0),"data":data})) != null ? stack1 : "")
    + "          </div>\n          <div class=\"col-xs-1 text-center\" data-toggle=\"tooltip\" data-placement=\"top\" title=\""
    + alias4((helpers.statusTooltipHelper || (depth0 && depth0.statusTooltipHelper) || alias2).call(alias1,((stack1 = (depth0 != null ? depth0.project : depth0)) != null ? stack1.status : stack1),{"name":"statusTooltipHelper","hash":{},"data":data}))
    + "\">\n              <div class=\""
    + alias4((helpers.statusHelper || (depth0 && depth0.statusHelper) || alias2).call(alias1,((stack1 = (depth0 != null ? depth0.project : depth0)) != null ? stack1.status : stack1),{"name":"statusHelper","hash":{},"data":data}))
    + "\" style=\"position: absolute; top: 0; bottom: 0; left: 0; right: 0; margin: auto;\"></div>\n          </div>\n\n      </div>\n  </div>\n";
},"4":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "	              <span class=\"h6 fwnormal\">"
    + container.escapeExpression(container.lambda(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.projectdetailspagerewardscarddelivery : stack1), depth0))
    + ": "
    + ((stack1 = (helpers.setDate || (depth0 && depth0.setDate) || helpers.helperMissing).call(depth0 != null ? depth0 : {},(depth0 != null ? depth0.delivery : depth0),{"name":"setDate","hash":{},"data":data})) != null ? stack1 : "")
    + "</span>\n";
},"6":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "	              <span class=\"h6 fwnormal\">"
    + container.escapeExpression(container.lambda(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.projectdetailspagerewardscarddelivery : stack1), depth0))
    + ": Belirsiz</span>\n";
},"8":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "    <p class=\"mt20\">"
    + container.escapeExpression(container.lambda(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.homepagealreadyloginmouseoverbackedprojecttext : stack1), depth0))
    + "</p>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : {};

  return ((stack1 = helpers["if"].call(alias1,depth0,{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\n"
    + ((stack1 = helpers.each.call(alias1,depth0,{"name":"each","hash":{},"fn":container.program(3, data, 0),"inverse":container.program(8, data, 0),"data":data})) != null ? stack1 : "");
},"useData":true});
templates['dashboard-comments'] = template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : {}, alias2=helpers.helperMissing, alias3=container.escapeExpression;

  return "    <div class=\""
    + ((stack1 = (helpers.isCreatorComment || (depth0 && depth0.isCreatorComment) || alias2).call(alias1,(depth0 != null ? depth0.creatorComment : depth0),{"name":"isCreatorComment","hash":{},"data":data})) != null ? stack1 : "")
    + "\">\n        <div class=\"info-container\">\n            <div class=\"col-xs-3\">\n                <span class=\"h6\">"
    + alias3(container.lambda(((stack1 = (depth0 != null ? depth0.project : depth0)) != null ? stack1.name : stack1), depth0))
    + "</span>\n            </div>\n            <div class=\"col-xs-3\">\n                <span class=\"h6\">"
    + ((stack1 = (helpers.setDateWithHours || (depth0 && depth0.setDateWithHours) || alias2).call(alias1,(depth0 != null ? depth0.created : depth0),{"name":"setDateWithHours","hash":{},"data":data})) != null ? stack1 : "")
    + "</span>\n            </div>\n            <div class=\"col-xs-3\">\n"
    + ((stack1 = (helpers.ifCond || (depth0 && depth0.ifCond) || alias2).call(alias1,(depth0 != null ? depth0.firstApproval : depth0),"==",false,{"name":"ifCond","hash":{},"fn":container.program(2, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\n            </div>\n            <div class=\"col-xs-3\">\n"
    + ((stack1 = (helpers.ifCond || (depth0 && depth0.ifCond) || alias2).call(alias1,(depth0 != null ? depth0.rate : depth0),"===",1,{"name":"ifCond","hash":{},"fn":container.program(4, data, 0),"inverse":container.program(6, data, 0),"data":data})) != null ? stack1 : "")
    + "            </div>\n            <div class=\"col-xs-12\" style=\"float: left !important;\">\n                <p>"
    + alias3(((helper = (helper = helpers.comment || (depth0 != null ? depth0.comment : depth0)) != null ? helper : alias2),(typeof helper === "function" ? helper.call(alias1,{"name":"comment","hash":{},"data":data}) : helper)))
    + "</p>\n                <p class=\"corp-micro-like-dislike\">\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.fileUrl : depth0),{"name":"if","hash":{},"fn":container.program(8, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "                </p>\n            </div>\n        </div>\n    </div>\n";
},"2":function(container,depth0,helpers,partials,data) {
    return "                    <span class=\"h6\">Onay Bekliyor</span>";
},"4":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=container.lambda, alias2=container.escapeExpression;

  return "                    <span class=\"h6\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.project : depth0)) != null ? stack1.likeName : stack1), depth0))
    + "</span>\n                    <span class=\"h6\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.project : depth0)) != null ? stack1.likePoint : stack1), depth0))
    + " puan</span>\n";
},"6":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=container.lambda, alias2=container.escapeExpression;

  return "                    <span class=\"h6\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.project : depth0)) != null ? stack1.dislikeName : stack1), depth0))
    + "</span>\n                    <span class=\"h6\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.project : depth0)) != null ? stack1.dislikePoint : stack1), depth0))
    + " puan</span>\n";
},"8":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : {}, alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "                        <button class=\"btn btn-default btn-xs corp-button-micro button-micro-file\" data-href=\""
    + alias4(((helper = (helper = helpers.fileUrl || (depth0 != null ? depth0.fileUrl : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"fileUrl","hash":{},"data":data}) : helper)))
    + "\">\n                            <span class=\"glyphicon glyphicon-file corp-button-micro button-micro-file\" data-href=\""
    + alias4(((helper = (helper = helpers.fileUrl || (depth0 != null ? depth0.fileUrl : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"fileUrl","hash":{},"data":data}) : helper)))
    + "\"></span>\n                        </button>\n";
},"10":function(container,depth0,helpers,partials,data) {
    return "    <p>Henüz bir giriş yapılmamıştır.</p>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0 != null ? depth0 : {},depth0,{"name":"each","hash":{},"fn":container.program(1, data, 0),"inverse":container.program(10, data, 0),"data":data})) != null ? stack1 : "");
},"useData":true});
templates['dashboard-conversation'] = template({"1":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1, helper, alias1=depth0 != null ? depth0 : {}, alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "    <div class=\"dashboard-item__item dashboard-message fadeInDown\" style=\"animation-delay: "
    + ((stack1 = (helpers.animationDelay || (depth0 && depth0.animationDelay) || alias2).call(alias1,(data && data.index),{"name":"animationDelay","hash":{},"data":data})) != null ? stack1 : "")
    + "\">\n        <div class=\"avatar-container mr20\">\n            <div class=\"bg-img item-photo\" style=\"background-image: url('"
    + alias4(((helper = (helper = helpers.counterpartImage || (depth0 != null ? depth0.counterpartImage : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"counterpartImage","hash":{},"data":data}) : helper)))
    + "')\">\n                <img class=\"opacity0\" src=\""
    + alias4(((helper = (helper = helpers.counterpartImage || (depth0 != null ? depth0.counterpartImage : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"counterpartImage","hash":{},"data":data}) : helper)))
    + "\" title=\""
    + alias4(((helper = (helper = helpers.counterpartName || (depth0 != null ? depth0.counterpartName : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"counterpartName","hash":{},"data":data}) : helper)))
    + "\" alt=\""
    + alias4(((helper = (helper = helpers.counterpartName || (depth0 != null ? depth0.counterpartName : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"counterpartName","hash":{},"data":data}) : helper)))
    + "\">\n            </div>\n        </div>\n        <div class=\"info-container info-container--dashboard\">\n            <h4 class=\"green-dark inline mr10 fwnormal\">"
    + alias4(((helper = (helper = helpers.counterpartName || (depth0 != null ? depth0.counterpartName : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"counterpartName","hash":{},"data":data}) : helper)))
    + " "
    + alias4(container.lambda(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.profilemessegingwithuser : stack1), depth0))
    + "</h4> <!--Array length - index-->\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.messages : depth0),{"name":"each","hash":{},"fn":container.program(2, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "        </div>\n        <a class=\"full-link\" data-index=\""
    + alias4(((helper = (helper = helpers.index || (data && data.index)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"index","hash":{},"data":data}) : helper)))
    + "\"></a>\n    </div>\n";
},"2":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1;

  return ((stack1 = helpers["if"].call(depth0 != null ? depth0 : {},(data && data.last),{"name":"if","hash":{},"fn":container.program(3, data, 0, blockParams, depths),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"3":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1, helper, alias1=depth0 != null ? depth0 : {}, alias2=helpers.helperMissing, alias3=container.escapeExpression;

  return "                <h6 class=\"mt0 fwnormal pull-right\">"
    + ((stack1 = (helpers.setDateWithHours || (depth0 && depth0.setDateWithHours) || alias2).call(alias1,(depth0 != null ? depth0.created : depth0),{"name":"setDateWithHours","hash":{},"data":data})) != null ? stack1 : "")
    + "</h6>\n                <span class=\"message-count bg-yellow-dark box-shadow plr10 white fwnormal\">"
    + alias3(container.lambda((depths[1] != null ? depths[1].messageCount : depths[1]), depth0))
    + "</span>\n                <p class=\"h6\">"
    + alias3(((helper = (helper = helpers.message || (depth0 != null ? depth0.message : depth0)) != null ? helper : alias2),(typeof helper === "function" ? helper.call(alias1,{"name":"message","hash":{},"data":data}) : helper)))
    + "</p>\n";
},"5":function(container,depth0,helpers,partials,data) {
    return "    <p>Mesaj bulunmamaktadır</p>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data,blockParams,depths) {
    var stack1, helper, alias1=depth0 != null ? depth0 : {};

  return "<h3 class=\"green-dark fwnormal mb30\">Mesajlar ("
    + container.escapeExpression(((helper = (helper = helpers.unreadCount || (depth0 != null ? depth0.unreadCount : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(alias1,{"name":"unreadCount","hash":{},"data":data}) : helper)))
    + ")</h3>\n<div class=\"content\">\n"
    + ((stack1 = helpers.each.call(alias1,depth0,{"name":"each","hash":{},"fn":container.program(1, data, 0, blockParams, depths),"inverse":container.program(5, data, 0, blockParams, depths),"data":data})) != null ? stack1 : "")
    + "</div>\n";
},"useData":true,"useDepths":true});
templates['dashboard-invites'] = template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : {}, alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "    <div class=\"dashboard-item__item fadeInDown\" style=\"animation-delay: "
    + ((stack1 = (helpers.animationDelay || (depth0 && depth0.animationDelay) || alias2).call(alias1,(data && data.index),{"name":"animationDelay","hash":{},"data":data})) != null ? stack1 : "")
    + "\">\n        <div class=\"info-container info-container--dashboard\">\n            <span class=\"h5 fwnormal\">\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.status : depth0),{"name":"if","hash":{},"fn":container.program(2, data, 0),"inverse":container.program(4, data, 0),"data":data})) != null ? stack1 : "")
    + "            </span>\n            <span class=\"h6 fwnormal pull-right\">"
    + ((stack1 = (helpers.setDateWithHours || (depth0 && depth0.setDateWithHours) || alias2).call(alias1,(depth0 != null ? depth0.created : depth0),{"name":"setDateWithHours","hash":{},"data":data})) != null ? stack1 : "")
    + "</span>\n            <h4 class=\"green-dark mr10 fwnormal\">"
    + alias4(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"title","hash":{},"data":data}) : helper)))
    + "</h4>\n            <p class=\"h6\">"
    + alias4(((helper = (helper = helpers.description || (depth0 != null ? depth0.description : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"description","hash":{},"data":data}) : helper)))
    + "</p>\n        </div>\n    </div>\n";
},"2":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=container.lambda, alias2=container.escapeExpression;

  return "                    "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.project : depth0)) != null ? stack1.name : stack1), depth0))
    + " isimli projeye ulaşmak için <a href=\"/projeler/"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.project : depth0)) != null ? stack1.urlPostfix : stack1), depth0))
    + "/detay\">tıklayınız</a>.\n";
},"4":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=container.lambda, alias2=container.escapeExpression;

  return "                    "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.project : depth0)) != null ? stack1.name : stack1), depth0))
    + " isimli projeye davetiniz bulunmaktadır. Ulaşmak için <a href=\"/projeler/"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.project : depth0)) != null ? stack1.urlPostfix : stack1), depth0))
    + "/detay\">tıklayınız</a>.\n";
},"6":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "    <p>"
    + container.escapeExpression(container.lambda(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.acitivitiesnoactivity : stack1), depth0))
    + "</p>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0 != null ? depth0 : {},depth0,{"name":"each","hash":{},"fn":container.program(1, data, 0),"inverse":container.program(6, data, 0),"data":data})) != null ? stack1 : "");
},"useData":true});
templates['dashboard-loading'] = template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<div class=\"dashboard-item__item full-width bg-white\">\n    <div class=\"animated-background facebook\">\n        <div class=\"background-masker header-top\"></div>\n        <div class=\"background-masker header-left\"></div>\n        <div class=\"background-masker header-right\"></div>\n        <div class=\"background-masker header-bottom\"></div>\n        <div class=\"background-masker subheader-left\"></div>\n        <div class=\"background-masker subheader-right\"></div>\n        <div class=\"background-masker subheader-bottom\"></div>\n        <div class=\"background-masker content-top\"></div>\n        <div class=\"background-masker content-first-end\"></div>\n        <div class=\"background-masker content-second-line\"></div>\n        <div class=\"background-masker content-second-end\"></div>\n        <div class=\"background-masker content-third-line\"></div>\n        <div class=\"background-masker content-third-end\"></div>\n    </div>\n</div>\n\n<div class=\"dashboard-item__item full-width bg-white\">\n    <div class=\"animated-background facebook\">\n        <div class=\"background-masker header-top\"></div>\n        <div class=\"background-masker header-left\"></div>\n        <div class=\"background-masker header-right\"></div>\n        <div class=\"background-masker header-bottom\"></div>\n        <div class=\"background-masker subheader-left\"></div>\n        <div class=\"background-masker subheader-right\"></div>\n        <div class=\"background-masker subheader-bottom\"></div>\n        <div class=\"background-masker content-top\"></div>\n        <div class=\"background-masker content-first-end\"></div>\n        <div class=\"background-masker content-second-line\"></div>\n        <div class=\"background-masker content-second-end\"></div>\n        <div class=\"background-masker content-third-line\"></div>\n        <div class=\"background-masker content-third-end\"></div>\n    </div>\n</div>\n\n<div class=\"dashboard-item__item full-width bg-white\">\n    <div class=\"animated-background facebook\">\n        <div class=\"background-masker header-top\"></div>\n        <div class=\"background-masker header-left\"></div>\n        <div class=\"background-masker header-right\"></div>\n        <div class=\"background-masker header-bottom\"></div>\n        <div class=\"background-masker subheader-left\"></div>\n        <div class=\"background-masker subheader-right\"></div>\n        <div class=\"background-masker subheader-bottom\"></div>\n        <div class=\"background-masker content-top\"></div>\n        <div class=\"background-masker content-first-end\"></div>\n        <div class=\"background-masker content-second-line\"></div>\n        <div class=\"background-masker content-second-end\"></div>\n        <div class=\"background-masker content-third-line\"></div>\n        <div class=\"background-masker content-third-end\"></div>\n    </div>\n</div>\n";
},"useData":true});
templates['dashboard-message'] = template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : {}, alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression, alias5=container.lambda;

  return "  <h3 id=\"counterPart\" class=\"green-dark fwnormal mb30\" data-id=\""
    + alias4(((helper = (helper = helpers.counterpartId || (depth0 != null ? depth0.counterpartId : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"counterpartId","hash":{},"data":data}) : helper)))
    + "\">"
    + alias4(((helper = (helper = helpers.counterpartName || (depth0 != null ? depth0.counterpartName : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"counterpartName","hash":{},"data":data}) : helper)))
    + " "
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.profilemessaginguser : stack1), depth0))
    + "</h3>\n  <!--<h5 class=\"fwnormal mb40\">Lorem Ipsum ile olan mesajlarınız</h5>-->\n  <div class=\"content\">\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.messages : depth0),{"name":"each","hash":{},"fn":container.program(2, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "  </div>\n\n\n  <div class=\"full-width inline-block mtb50\">\n      <div class=\"dashboard-item--message-detail pull-left\">\n          <textarea id=\"message\" class=\"primary-form box-shadow\" rows=\"4\" placeholder=\""
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.profilemessegingyourmsg : stack1), depth0))
    + "\"></textarea>\n      </div>\n      <button id=\"sendMessage\" class=\"btn btn--small btn--yellow pull-right plr15 fwnormal green-dark--link mt60\">Gönder</button>\n  </div>\n";
},"2":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : {}, alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "      <div class=\"row fadeInDown mb10\" style=\"animation-delay: "
    + ((stack1 = (helpers.animationDelay || (depth0 && depth0.animationDelay) || alias2).call(alias1,(data && data.index),{"name":"animationDelay","hash":{},"data":data})) != null ? stack1 : "")
    + "\">\n          <div class=\"col-xs-12\">\n\n              <div class=\""
    + ((stack1 = (helpers.ternary || (depth0 && depth0.ternary) || alias2).call(alias1,(depth0 != null ? depth0.self : depth0),"message-detail-avatar-container pull-left","message-detail-avatar-container pull-right",{"name":"ternary","hash":{},"data":data})) != null ? stack1 : "")
    + "\">\n                  <div class=\"bg-img item-photo box-shadow\" style=\"background-image: url('"
    + alias4(((helper = (helper = helpers.image || (depth0 != null ? depth0.image : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"image","hash":{},"data":data}) : helper)))
    + "')\">\n                      <img class=\"opacity0\" src=\""
    + alias4(((helper = (helper = helpers.image || (depth0 != null ? depth0.image : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"image","hash":{},"data":data}) : helper)))
    + "\" title=\""
    + alias4(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data}) : helper)))
    + "\" alt=\""
    + alias4(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data}) : helper)))
    + "\">\n                  </div>\n              </div>\n\n              <div class=\""
    + ((stack1 = (helpers.ternary || (depth0 && depth0.ternary) || alias2).call(alias1,(depth0 != null ? depth0.self : depth0),"dashboard-item__item dashboard-item--message-detail pull-right","dashboard-item__item dashboard-item--message-detail pull-left",{"name":"ternary","hash":{},"data":data})) != null ? stack1 : "")
    + "\">\n                  <div class=\"info-container info-container--dashboard\">\n                      <h4 class=\"green-dark inline mr10 fwnormal\">"
    + alias4(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data}) : helper)))
    + "</h4>\n                      <h6 class=\"mt0 fwnormal pull-right\">"
    + ((stack1 = (helpers.setDateWithHours || (depth0 && depth0.setDateWithHours) || alias2).call(alias1,(depth0 != null ? depth0.created : depth0),{"name":"setDateWithHours","hash":{},"data":data})) != null ? stack1 : "")
    + "</h6>\n                      <p class=\"h6 mb0\">"
    + alias4(((helper = (helper = helpers.message || (depth0 != null ? depth0.message : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"message","hash":{},"data":data}) : helper)))
    + "</p>\n                  </div>\n              </div>\n\n          </div>\n      </div>\n";
},"4":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : {}, alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "    <div class=\"row slideInLeft mb10\">\n        <div class=\"col-xs-12\">\n\n            <div class=\"message-detail-avatar-container pull-left\">\n                <div class=\"bg-img item-photo box-shadow\" style=\"background-image: url('"
    + alias4(((helper = (helper = helpers.image || (depth0 != null ? depth0.image : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"image","hash":{},"data":data}) : helper)))
    + "')\">\n                    <img class=\"opacity0\" src=\""
    + alias4(((helper = (helper = helpers.image || (depth0 != null ? depth0.image : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"image","hash":{},"data":data}) : helper)))
    + "\" title=\""
    + alias4(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data}) : helper)))
    + "\" alt=\""
    + alias4(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data}) : helper)))
    + "\">\n                </div>\n            </div>\n\n            <div class=\"dashboard-item__item dashboard-item--message-detail pull-right\">\n                <div class=\"info-container info-container--dashboard\">\n                    <h4 class=\"green-dark inline mr10 fwnormal\">"
    + alias4(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data}) : helper)))
    + "</h4>\n                    <h6 class=\"mt0 fwnormal pull-right\">Şimdi</h6>\n                    <p class=\"h6 mb0\">"
    + alias4(((helper = (helper = helpers.message || (depth0 != null ? depth0.message : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"message","hash":{},"data":data}) : helper)))
    + "</p>\n                </div>\n            </div>\n\n        </div>\n    </div>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers["if"].call(depth0 != null ? depth0 : {},(depth0 != null ? depth0.messages : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.program(4, data, 0),"data":data})) != null ? stack1 : "");
},"useData":true});
templates['dashboard-profile'] = template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : {}, alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression, alias5=container.lambda;

  return "<form id=\"signUpForm\" data-parsley-validate style=\"width: 400px; margin: 0 auto\">\n    <fieldset>\n        <ol class=\"grouped mt0\">\n            <li class=\"mb30 required\">\n                <div id=\"uploader\" class=\"upload form-avatar has-avatar circular\" style=\"background-image: url('"
    + alias4(((helper = (helper = helpers.image || (depth0 != null ? depth0.image : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"image","hash":{},"data":data}) : helper)))
    + "')\">\n                    <span class=\"form-avatar__placeholder\"></span>\n                    <div class=\"overlay\"><span class=\"text\">"
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.profileditpicturemouseovertext : stack1), depth0))
    + "</span></div>\n                    <input class=\"photo file fileupload\" id=\"fileupload\" name=\"image\" value=\""
    + alias4(((helper = (helper = helpers.image || (depth0 != null ? depth0.image : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"image","hash":{},"data":data}) : helper)))
    + "\" />\n                </div>\n            </li>\n            <li class=\"required\">\n                <input class=\"primary-form green-dark ptb15\" value=\""
    + alias4(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data}) : helper)))
    + "\" required name=\"name\" data-parsley-required-message=\"Lütfen isminizi yazınız\" placeholder=\""
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.profileeditnamelabel : stack1), depth0))
    + "\" type=\"text\" />\n            </li>\n            <li class=\"required\">\n                <input class=\"primary-form green-dark ptb15 mt10\" value=\""
    + alias4(((helper = (helper = helpers.surname || (depth0 != null ? depth0.surname : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"surname","hash":{},"data":data}) : helper)))
    + "\" required name=\"surname\" data-parsley-required-message=\"Lütfen soyisminizi yazınız\" placeholder=\""
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.profileeditsurnamelabel : stack1), depth0))
    + "\" type=\"text\" />\n            </li>\n            <li class=\"required\">\n                <input id=\"email\" class=\"primary-form green-dark ptb15 mt10\" value=\""
    + alias4(((helper = (helper = helpers.email || (depth0 != null ? depth0.email : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"email","hash":{},"data":data}) : helper)))
    + "\" required name=\"email\" data-parsley-required-message=\"Lütfen emailinizi yazınız\" placeholder=\"Email adresi\" type=\"text\" />\n            </li>\n            <!--<li class=\"required\">\n                <div class=\"primary-form disabled green-dark ptb15 mt10\">"
    + alias4(((helper = (helper = helpers.phone || (depth0 != null ? depth0.phone : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"phone","hash":{},"data":data}) : helper)))
    + "</div>\n            </li>-->\n        </ol>\n    </fieldset>\n    <div class=\"not-refund*checkout mt50 mb30\" id=\"submit\">\n        <button id=\"profileUpdateBtn\" class=\"ptb15 btn btn--yellow full-width green-dark--link block submit\" style=\"font-size: 18px\" tabindex=\"-1\">"
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.profilepagetabmyprofilebuttontext1 : stack1), depth0))
    + "</button>\n    </div>\n    <!--Hidden Fields-->\n    <input type=\"hidden\" name=\"id\" value=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" />\n</form>";
},"useData":true});
templates['dashboard-projects'] = template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=container.lambda, alias2=container.escapeExpression;

  return "<div class=\"inline-block full-width ptb15 white mb10 mt20 bg-yellow-dark box-shadow\">\n    <div class=\"col-xs-4\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.profilepagetabmyprojectstablecolumns1 : stack1), depth0))
    + "</div>\n    <!--<div class=\"col-xs-2 text-center\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.profilepagetabmyprojectstablecolumns2 : stack1), depth0))
    + "</div>-->\n    <!--<div class=\"col-xs-2 text-center\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.profilepagetabmyprojectstablecolumns3 : stack1), depth0))
    + "</div>-->\n    <div class=\"col-xs-2 text-center\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.profilepagetabmyprojectstablecolumns4 : stack1), depth0))
    + "</div>\n    <div class=\"col-xs-2 text-center\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.profilepagetabmyprojectstablecolumns5 : stack1), depth0))
    + "</div>\n</div>\n";
},"3":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : {}, alias2=helpers.helperMissing;

  return "<div class=\"dashboard-item__item backed-projects fadeInDown\" style=\"animation-delay: "
    + ((stack1 = (helpers.animationDelay || (depth0 && depth0.animationDelay) || alias2).call(alias1,(data && data.index),{"name":"animationDelay","hash":{},"data":data})) != null ? stack1 : "")
    + "\">\n    <div class=\"row\">\n        <div class=\"col-xs-4\">\n"
    + ((stack1 = (helpers.compare || (depth0 && depth0.compare) || alias2).call(alias1,(depth0 != null ? depth0.status : depth0),10,{"name":"compare","hash":{"operator":">="},"fn":container.program(4, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.compare || (depth0 && depth0.compare) || alias2).call(alias1,(depth0 != null ? depth0.status : depth0),10,{"name":"compare","hash":{"operator":"<"},"fn":container.program(6, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "        </div>\n        <!--<div class=\"col-xs-2\">\n            <span class=\"h5 yellow-dark block m0 absolute align-center text-center fwnormal\">"
    + ((stack1 = (helpers.projectStatus || (depth0 && depth0.projectStatus) || alias2).call(alias1,(depth0 != null ? depth0.status : depth0),{"name":"projectStatus","hash":{},"data":data})) != null ? stack1 : "")
    + "</span>\n        </div>\n        <div class=\"col-xs-2\">\n            <span class=\"h4 green-dark block m0 absolute align-center ls-2 text-center fwnormal\">%"
    + container.escapeExpression(((helper = (helper = helpers.completionPercentage || (depth0 != null ? depth0.completionPercentage : depth0)) != null ? helper : alias2),(typeof helper === "function" ? helper.call(alias1,{"name":"completionPercentage","hash":{},"data":data}) : helper)))
    + "</span>\n        </div>-->\n        <div class=\"col-xs-2 text-center\">\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.inFormState : depth0),{"name":"if","hash":{},"fn":container.program(8, data, 0),"inverse":container.program(10, data, 0),"data":data})) != null ? stack1 : "")
    + "        </div>\n"
    + ((stack1 = helpers.unless.call(alias1,(depth0 != null ? depth0.inFormState : depth0),{"name":"unless","hash":{},"fn":container.program(15, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "    </div>\n</div>\n";
},"4":function(container,depth0,helpers,partials,data) {
    var helper;

  return "            <h4 class=\"green-dark valign absolute mr10 project-title\">\n                <span>"
    + container.escapeExpression(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : {},{"name":"name","hash":{},"data":data}) : helper)))
    + "</span>\n            </h4>\n";
},"6":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : {}, alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "	            <h4 class=\"green-dark valign absolute mr10 project-title\">\n		            <a href=\"/projeler/"
    + alias4(((helper = (helper = helpers.urlPostfix || (depth0 != null ? depth0.urlPostfix : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"urlPostfix","hash":{},"data":data}) : helper)))
    + "/detay\">"
    + alias4(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data}) : helper)))
    + "</a>\n	            </h4>\n";
},"8":function(container,depth0,helpers,partials,data) {
    return "";
},"10":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : {}, alias2=helpers.helperMissing;

  return ((stack1 = (helpers.compare || (depth0 && depth0.compare) || alias2).call(alias1,(depth0 != null ? depth0.status : depth0),7,{"name":"compare","hash":{"operator":">="},"fn":container.program(11, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + ((stack1 = (helpers.compare || (depth0 && depth0.compare) || alias2).call(alias1,(depth0 != null ? depth0.status : depth0),7,{"name":"compare","hash":{"operator":"<"},"fn":container.program(13, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"11":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=container.escapeExpression;

  return "                <a href=\"/projeler/"
    + alias1(((helper = (helper = helpers.urlPostfix || (depth0 != null ? depth0.urlPostfix : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : {},{"name":"urlPostfix","hash":{},"data":data}) : helper)))
    + "/detay\" class=\"btn btn--yellow green-dark--link absolute align-center fwnormal\" target=\"_blank\">\n                  "
    + alias1(container.lambda(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.projectseeproject : stack1), depth0))
    + "\n                </a>\n";
},"13":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=container.escapeExpression;

  return "	                <a href=\"/edit/"
    + alias1(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : {},{"name":"id","hash":{},"data":data}) : helper)))
    + "\" class=\"btn btn--yellow green-dark--link absolute align-center fwnormal\" target=\"_blank\">\n                    "
    + alias1(container.lambda(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.profilepageaddtimelineactivitiestablecolumns4 : stack1), depth0))
    + "\n	                </a>\n";
},"15":function(container,depth0,helpers,partials,data) {
    var helper;

  return "          <div class=\"col-xs-2 text-center\">\n              <button data-id=\""
    + container.escapeExpression(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : helpers.helperMissing),(typeof helper === "function" ? helper.call(depth0 != null ? depth0 : {},{"name":"id","hash":{},"data":data}) : helper)))
    + "\" class=\"timeline btn btn--white green-dark--link absolute align-center fwnormal\">Timeline</button>\n          </div>\n";
},"17":function(container,depth0,helpers,partials,data) {
    return "    <p class=\"mt20\">Proje bulunmamaktadır</p>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : {};

  return ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.data : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "\n"
    + ((stack1 = helpers.each.call(alias1,(depth0 != null ? depth0.data : depth0),{"name":"each","hash":{},"fn":container.program(3, data, 0),"inverse":container.program(17, data, 0),"data":data})) != null ? stack1 : "");
},"useData":true});
templates['merchant-info'] = template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=container.lambda, alias2=container.escapeExpression, alias3=depth0 != null ? depth0 : {}, alias4=helpers.helperMissing, alias5="function";

  return "<div class=\"center block\">\n    <h2 class=\"h2 green-dark fwbold\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.fillprojectdetailspage5textboxtitle : stack1), depth0))
    + "</h2>\n\n    <p class=\"h4 mt0 green-dark\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.fillprojectdetailspage5textboxsubtitle : stack1), depth0))
    + "</p>\n</div>\n\n<div class=\"table-display full-width mt60 mb30 ptb20 plr10 m-center bg-white border\">\n    <div class=\"row\">\n        <div class=\"col-xs-9\">\n            <div id=\"merchantInfo-form\" class=\"project-main-form content mr10\" data-parsley-validate>\n                <ol class=\"fields no-margin\">\n                    <li class=\"memType\">\n                        <div class=\"grey-field mb20 clearfix\">\n                            <label>\n                              "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.fillprojectdetailspage5textboxtypetitle : stack1), depth0))
    + "\n                            </label>\n\n                            <div class=\"field-wrapper radio\">\n                                <label class=\"width-auto pt0 mr15\">\n                                    <input type=\"radio\" class=\"form-control\" name=\"memType\"\n                                           value=\"1\" "
    + ((stack1 = (helpers.memType || (depth0 && depth0.memType) || alias4).call(alias3,(depth0 != null ? depth0.type : depth0),1,{"name":"memType","hash":{},"data":data})) != null ? stack1 : "")
    + " />\n                                    <span class=\"cr white-rb\"><i class=\"cr-icon glyphicon glyphicon-record\"></i></span>\n                                    <span>"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.fillprojectdetailspage5textboxtypesubtitle1 : stack1), depth0))
    + "</span>\n                                </label>\n                                <label class=\"width-auto pt0 mr15\">\n                                    <input type=\"radio\" class=\"form-control\" name=\"memType\"\n                                           value=\"2\" "
    + ((stack1 = (helpers.memType || (depth0 && depth0.memType) || alias4).call(alias3,(depth0 != null ? depth0.type : depth0),2,{"name":"memType","hash":{},"data":data})) != null ? stack1 : "")
    + " />\n                                    <span class=\"cr white-rb\"><i class=\"cr-icon glyphicon glyphicon-record\"></i></span>\n	                                <span>"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.fillprojectdetailspage5textboxtypesubtitle2 : stack1), depth0))
    + "</span>\n                                </label>\n                                <label class=\"width-auto pt0\">\n                                    <input type=\"radio\" class=\"form-control\" name=\"memType\"\n                                           value=\"3\" "
    + ((stack1 = (helpers.memType || (depth0 && depth0.memType) || alias4).call(alias3,(depth0 != null ? depth0.type : depth0),3,{"name":"memType","hash":{},"data":data})) != null ? stack1 : "")
    + " />\n                                    <span class=\"cr white-rb\"><i class=\"cr-icon glyphicon glyphicon-record\"></i></span>\n                                    <span>"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.fillprojectdetailspage5textboxtypesubtitle3 : stack1), depth0))
    + "</span>\n                                </label>\n                            </div>\n                        </div>\n                    </li>\n                    <li class=\"iban\">\n                        <div class=\"grey-field mb20 clearfix\">\n                            <label for=\"iban\">\n                                "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.fillprojectdetailspage5textboxibantitle : stack1), depth0))
    + "\n                            </label>\n\n                            <div class=\"field-wrapper\">\n                                <input type=\"text\" id=\"iban\" class=\"form-control border\" maxlength=\"28\"\n                                       data-parsley-required value=\""
    + alias2(((helper = (helper = helpers.iban || (depth0 != null ? depth0.iban : depth0)) != null ? helper : alias4),(typeof helper === alias5 ? helper.call(alias3,{"name":"iban","hash":{},"data":data}) : helper)))
    + "\"/>\n                            </div>\n                        </div>\n                    </li>\n                    <li class=\"pstn\">\n                        <div class=\"grey-field mb20 clearfix\">\n                            <label for=\"pstn\">\n                                "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.fillprojectdetailspage5textboxphonetitle : stack1), depth0))
    + "\n                            </label>\n\n                            <div class=\"field-wrapper\">\n                                <input type=\"text\" id=\"pstn\" class=\"form-control border\" maxlength=\"11\"\n                                       data-parsley-required value=\""
    + alias2(((helper = (helper = helpers.pstn || (depth0 != null ? depth0.pstn : depth0)) != null ? helper : alias4),(typeof helper === alias5 ? helper.call(alias3,{"name":"pstn","hash":{},"data":data}) : helper)))
    + "\"/>\n                            </div>\n                        </div>\n                    </li>\n                    <li class=\"address\">\n                        <div class=\"grey-field mb20 clearfix\">\n                            <label for=\"address\">\n                                "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.fillprojectdetailspage5textboxaddresstitle : stack1), depth0))
    + "\n                            </label>\n\n                            <div class=\"field-wrapper\">\n                                <textarea id=\"address\" rows=\"3\" class=\"form-control border\" maxlength=\"100\"\n                                          data-parsley-required>"
    + alias2(((helper = (helper = helpers.address || (depth0 != null ? depth0.address : depth0)) != null ? helper : alias4),(typeof helper === alias5 ? helper.call(alias3,{"name":"address","hash":{},"data":data}) : helper)))
    + "</textarea>\n                            </div>\n                        </div>\n                    </li>\n                    <li class=\"tckNo conditional\">\n                        <div class=\"grey-field mb20 clearfix\">\n                            <label for=\"tckNo\">\n                                TC Kimlik No\n                            </label>\n\n                            <div class=\"field-wrapper\">\n                                <input type=\"text\" id=\"tckNo\" class=\"form-control border\" maxlength=\"11\"\n                                       value=\""
    + alias2(((helper = (helper = helpers.tckNo || (depth0 != null ? depth0.tckNo : depth0)) != null ? helper : alias4),(typeof helper === alias5 ? helper.call(alias3,{"name":"tckNo","hash":{},"data":data}) : helper)))
    + "\"/>\n                            </div>\n                        </div>\n                    </li>\n                    <li class=\"companyTitle conditional\">\n                        <div class=\"grey-field mb20 clearfix\">\n                            <label for=\"companyTitle\">\n                                "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.fillprojectdetailspage5textboxcompanynametitle : stack1), depth0))
    + "\n                            </label>\n\n                            <div class=\"field-wrapper\">\n                                <input type=\"text\" id=\"companyTitle\" class=\"form-control border\"\n                                       value=\""
    + alias2(((helper = (helper = helpers.companyTitle || (depth0 != null ? depth0.companyTitle : depth0)) != null ? helper : alias4),(typeof helper === alias5 ? helper.call(alias3,{"name":"companyTitle","hash":{},"data":data}) : helper)))
    + "\"/>\n                            </div>\n                        </div>\n                    </li>\n                    <li class=\"taxNo conditional\">\n                        <div class=\"grey-field mb20 clearfix\">\n                            <label for=\"taxNo\">\n                                "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.fillprojectdetailspage5textboxtaxnumbertitle : stack1), depth0))
    + "\n                            </label>\n\n                            <div class=\"field-wrapper\">\n                                <input type=\"text\" id=\"taxNo\" class=\"form-control border\" maxlength=\"11\"\n                                       value=\""
    + alias2(((helper = (helper = helpers.taxNo || (depth0 != null ? depth0.taxNo : depth0)) != null ? helper : alias4),(typeof helper === alias5 ? helper.call(alias3,{"name":"taxNo","hash":{},"data":data}) : helper)))
    + "\"/>\n                            </div>\n                        </div>\n                    </li>\n                    <li class=\"taxOffice conditional\">\n                        <div class=\"grey-field mb20 clearfix\">\n                            <label for=\"taxOffice\">\n                              "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.fillprojectdetailspage5textboxtaxofficetitle : stack1), depth0))
    + "\n                            </label>\n\n                            <div class=\"field-wrapper\">\n                                <input type=\"text\" id=\"taxOffice\" class=\"form-control border\" value=\""
    + alias2(((helper = (helper = helpers.taxOffice || (depth0 != null ? depth0.taxOffice : depth0)) != null ? helper : alias4),(typeof helper === alias5 ? helper.call(alias3,{"name":"taxOffice","hash":{},"data":data}) : helper)))
    + "\"/>\n                            </div>\n                        </div>\n                    </li>\n                    <li class=\"agreed\">\n	                    <div class=\"checkbox grey-field mb20 clearfix\">\n		                    <label style=\"width: 400px\" th:utext=\"${contentStrings.projectapprovepaymentterms}\">\n			                    <input id=\"agreed-checkbox\" type=\"checkbox\" checked=\""
    + alias2(((helper = (helper = helpers.agreementAccepted || (depth0 != null ? depth0.agreementAccepted : depth0)) != null ? helper : alias4),(typeof helper === alias5 ? helper.call(alias3,{"name":"agreementAccepted","hash":{},"data":data}) : helper)))
    + "\" name=\"payment-agreed\" required data-parsley-required-message=\"Lütfen ilerleyebilmek için sözleşmeyi kabul edin.\">\n		                      <span class=\"cr white-cb\"><i class=\"cr-icon glyphicon glyphicon-ok\"></i></span>\n			                    <a href=\"https://www.iyzico.com/pazaryeri-satici-anlasma\" target=\"_blank\">Ödeme Sistemleri sözleşmesini</a> okudum, onaylıyorum.\n		                    </label>\n	                    </div>\n                    </li>\n                </ol>\n            </div>\n        </div>\n        <div class=\"col-xs-3\">\n            <div id=\"merchant-sidebar\" class=\"project-sidebar-form ml10\">\n                <div class=\"project-thumbnail\">\n\n                </div>\n            </div>\n        </div>\n    </div>\n</div>\n";
},"useData":true});
templates['pledge-coupon'] = template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=container.lambda, alias2=container.escapeExpression;

  return "<input type=\"text\" id=\"couponTextField\" placeholder=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.chechkoutpagecoupontextboxinnertext : stack1), depth0))
    + "\" /> <button class=\"pledge-coupon-button\" type=\"button\" id=\"applyCouponButton\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.chechkoutpagecouponbuttontext : stack1), depth0))
    + "</button>\n<div id=\"coupon-result-msg\" style=\"display:none;\"></div>\n<div id=\"coupon-result-success\" class=\"table-responsive\" style=\"display:none;\">\n	<table class=\"table-hover\">\n		<tr><td>Toplam:</td><td class=\"pledge-coupon-success\" id=\"subTotalID\"></td></tr>\n		<tr><td>İndirim:</td><td class=\"pledge-coupon-success\" id=\"discountID\"></td></tr>\n		<tr><td>Genel Toplam:</td><td class=\"pledge-coupon-success\" id=\"totalID\"></td></tr>\n	</table>\n<div>\n";
},"useData":true});
templates['project-detail-comments'] = template({"1":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers["if"].call(depth0 != null ? depth0 : {},(depth0 != null ? depth0.comment : depth0),{"name":"if","hash":{},"fn":container.program(2, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"2":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : {}, alias2=helpers.helperMissing, alias3=container.lambda, alias4=container.escapeExpression;

  return "<div class=\""
    + ((stack1 = (helpers.isCreatorComment || (depth0 && depth0.isCreatorComment) || alias2).call(alias1,(depth0 != null ? depth0.creatorComment : depth0),{"name":"isCreatorComment","hash":{},"data":data})) != null ? stack1 : "")
    + "\">\n    <div class=\"avatar-container mr20\">\n        <div class=\"bg-img avatar\" style=\"background-image: url('"
    + alias4(alias3(((stack1 = (depth0 != null ? depth0.user : depth0)) != null ? stack1.image : stack1), depth0))
    + "')\">\n            <img class=\"opacity0\" src=\""
    + alias4(alias3(((stack1 = (depth0 != null ? depth0.user : depth0)) != null ? stack1.image : stack1), depth0))
    + "\" alt=\""
    + alias4(alias3(((stack1 = (depth0 != null ? depth0.user : depth0)) != null ? stack1.name : stack1), depth0))
    + "&nbsp;"
    + alias4(alias3(((stack1 = (depth0 != null ? depth0.user : depth0)) != null ? stack1.surname : stack1), depth0))
    + "\" title=\""
    + alias4(alias3(((stack1 = (depth0 != null ? depth0.user : depth0)) != null ? stack1.name : stack1), depth0))
    + "&nbsp;"
    + alias4(alias3(((stack1 = (depth0 != null ? depth0.user : depth0)) != null ? stack1.surname : stack1), depth0))
    + "\" />\n        </div>\n    </div>\n    <div class=\"info-container\">\n        <span class=\"h5 green-dark mr10\" style=\"min-width: 120px;display: inline-block;\">"
    + alias4(alias3(((stack1 = (depth0 != null ? depth0.user : depth0)) != null ? stack1.name : stack1), depth0))
    + "&nbsp;"
    + alias4(alias3(((stack1 = (depth0 != null ? depth0.user : depth0)) != null ? stack1.surname : stack1), depth0))
    + "</span>\n        <span class=\"h6\">"
    + ((stack1 = (helpers.setDateWithHours || (depth0 && depth0.setDateWithHours) || alias2).call(alias1,(depth0 != null ? depth0.created : depth0),{"name":"setDateWithHours","hash":{},"data":data})) != null ? stack1 : "")
    + "</span>\n        <p style=\"width:522px;float:left;\">"
    + alias4(((helper = (helper = helpers.comment || (depth0 != null ? depth0.comment : depth0)) != null ? helper : alias2),(typeof helper === "function" ? helper.call(alias1,{"name":"comment","hash":{},"data":data}) : helper)))
    + "</p>\n	    <p style=\"float:right;\" class=\"corp-micro-like-dislike\">\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.fileUrl : depth0),{"name":"if","hash":{},"fn":container.program(3, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "	    </p>\n    </div>\n</div>\n";
},"3":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : {}, alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "			    <button class=\"btn btn-default btn-xs corp-button-micro button-micro-file\" data-href=\""
    + alias4(((helper = (helper = helpers.fileUrl || (depth0 != null ? depth0.fileUrl : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"fileUrl","hash":{},"data":data}) : helper)))
    + "\">\n				    <span class=\"glyphicon glyphicon-file corp-button-micro button-micro-file\" data-href=\""
    + alias4(((helper = (helper = helpers.fileUrl || (depth0 != null ? depth0.fileUrl : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"fileUrl","hash":{},"data":data}) : helper)))
    + "\"></span>\n			    </button>\n";
},"5":function(container,depth0,helpers,partials,data) {
    return "<p>Henüz bir giriş yapılmamıştır.</p>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0 != null ? depth0 : {},depth0,{"name":"each","hash":{},"fn":container.program(1, data, 0),"inverse":container.program(5, data, 0),"data":data})) != null ? stack1 : "");
},"useData":true});
templates['project-detail-tabs'] = template({"1":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : {}, alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "	<li class=\"project-nav-item\" data-value=\"1\">\n	  <span data-comments-count=\"4\" id=\"newFeaturesCount\">\n	     <a class=\"js-load-project-content ptb20 green-dark--link block\" aria-controls=\"yorumlar\" data-toggle=\"tab\" href=\"#newFeatures\"><span>"
    + alias4(((helper = (helper = helpers.likeName || (depth0 != null ? depth0.likeName : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"likeName","hash":{},"data":data}) : helper)))
    + "</span>\n		     (<data class=\"Project813830071\" data-format=\"number\">0</data>)\n	     </a>\n	  </span>\n	</li>\n	<li class=\"project-nav-item\" data-value=\"2\">\n	  <span data-comments-count=\"4\" id=\"defectsCount\">\n	     <a class=\"js-load-project-content ptb20 green-dark--link block\" aria-controls=\"yorumlar\" data-toggle=\"tab\" href=\"#defects\"><span>"
    + alias4(((helper = (helper = helpers.dislikeName || (depth0 != null ? depth0.dislikeName : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"dislikeName","hash":{},"data":data}) : helper)))
    + "</span>\n		     (<data class=\"Project813830071\" data-format=\"number\">0</data>)\n	     </a>\n	  </span>\n	</li>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=container.lambda, alias2=container.escapeExpression;

  return "<!--<div class=\"selected-indicator js-selected-indicator transition-all-bezier\" style=\"left: 18.4688px; width: 69px;\"></div>-->\n<li class=\"project-nav-item active\">\n    <a class=\"js-load-project-content ptb20 green-dark--link block\" aria-controls=\"proje-bilgileri\" data-toggle=\"tab\" href=\"#proje-bilgileri\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.projectdetailspagetabsprojectinfo : stack1), depth0))
    + "</a>\n</li>\n<!--\n<li class=\"project-nav-item\">\n    <a class=\"js-load-project-content ptb20 green-dark--link block\" aria-controls=\"bocek-avcilari\" data-toggle=\"tab\" href=\"#bocek-avcilari\">Puan Sıralaması</a>\n</li>\n-->\n"
    + ((stack1 = helpers["if"].call(depth0 != null ? depth0 : {},(depth0 != null ? depth0.au : depth0),{"name":"if","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "<!--\n<li class=\"project-nav-item\" id=\"project-rewards-tab\" style=\"display:none;\">\n  <a class=\"js-load-project-content ptb20 green-dark--link block\" aria-controls=\"oduller\" data-toggle=\"tab\" href=\"#oduller\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.projectdetailspagerewardsheader : stack1), depth0))
    + "</a>\n</li>-->";
},"useData":true});
templates['project-detail-timeline'] = template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : {}, alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "<div class=\"row\">\n    <div class=\"col-xs-6\">\n        <h6 class=\"date\">"
    + ((stack1 = (helpers.setDateWithHours || (depth0 && depth0.setDateWithHours) || alias2).call(alias1,(depth0 != null ? depth0.created : depth0),{"name":"setDateWithHours","hash":{},"data":data})) != null ? stack1 : "")
    + "</h6>\n        <h5 class=\"green-dark\">"
    + alias4(((helper = (helper = helpers.title || (depth0 != null ? depth0.title : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"title","hash":{},"data":data}) : helper)))
    + "</h5>\n        <p>"
    + alias4(((helper = (helper = helpers.description || (depth0 != null ? depth0.description : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"description","hash":{},"data":data}) : helper)))
    + "</p>\n    </div>\n    <div class=\"col-xs-6\"></div>\n</div>\n";
},"3":function(container,depth0,helpers,partials,data) {
    var stack1;

  return "  <p class=\"text-center block\">"
    + container.escapeExpression(container.lambda(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.projectdetailspagetimelinesubtextnothing : stack1), depth0))
    + "</p>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0 != null ? depth0 : {},depth0,{"name":"each","hash":{},"fn":container.program(1, data, 0),"inverse":container.program(3, data, 0),"data":data})) != null ? stack1 : "");
},"useData":true});
templates['project-stats-mob'] = template({"1":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : {}, alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "            <div class=\"num h1 mb0 mt10\">\n                    <span class=\"icon-to-go mr10\" data-value=\"1\">\n                        <img src=\""
    + alias4(((helper = (helper = helpers.likeIcon || (depth0 != null ? depth0.likeIcon : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"likeIcon","hash":{},"data":data}) : helper)))
    + "\" style=\"float:left; margin-top: 5px; width: 100%; height: 100%;\"/>\n                    </span>\n                <data itemprop=\"Project[quickRateLikeCount]\" data-format=\"number\"\n                      data-value=\""
    + alias4(((helper = (helper = helpers.quickRateLikeCount || (depth0 != null ? depth0.quickRateLikeCount : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"quickRateLikeCount","hash":{},"data":data}) : helper)))
    + "\" style=\"font-weight: 900;\">"
    + alias4(((helper = (helper = helpers.quickRateLikeCount || (depth0 != null ? depth0.quickRateLikeCount : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"quickRateLikeCount","hash":{},"data":data}) : helper)))
    + "</data>\n                <span class=\"h4\" style=\"display: block; font-weight: 300;\">"
    + alias4(((helper = (helper = helpers.likeName || (depth0 != null ? depth0.likeName : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"likeName","hash":{},"data":data}) : helper)))
    + "</span>\n            </div>\n";
},"3":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : {}, alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "            <div class=\"num h1 mb0 mt10\" data-backers-count=\"200\" id=\"backersCount\">\n            <span class=\"icon-to-go mr10\">\n              <svg>\n                <use xmlns:xlink=\"http://www.w3.org/1999/xlink\" xlink:href=\"#backers\"></use>\n              </svg>\n            </span>\n                <data itemprop=\"Project[backersCount]\" data-format=\"number\"\n                      data-value=\""
    + alias4(((helper = (helper = helpers.backersCount || (depth0 != null ? depth0.backersCount : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"backersCount","hash":{},"data":data}) : helper)))
    + "\">"
    + alias4(((helper = (helper = helpers.backersCount || (depth0 != null ? depth0.backersCount : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"backersCount","hash":{},"data":data}) : helper)))
    + "</data>\n                <span style=\"position: relative;\" class=\"h4\">"
    + alias4(container.lambda(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.projectdetailspagefundingcardbacker : stack1), depth0))
    + "</span>\n            </div>\n";
},"5":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : {}, alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "            <div class=\"num h1 mb0\">\n                    <span class=\"icon-to-go mr10\" data-value=\"2\">\n                        <img src=\""
    + alias4(((helper = (helper = helpers.dislikeIcon || (depth0 != null ? depth0.dislikeIcon : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"dislikeIcon","hash":{},"data":data}) : helper)))
    + "\" style=\"float:left; margin-top: 5px; width: 100%; height: 100%;\"/>\n                    </span>\n                <data itemprop=\"Project[quickRateDislikeCount]\" data-format=\"number\"\n                      data-value=\""
    + alias4(((helper = (helper = helpers.quickRateDislikeCount || (depth0 != null ? depth0.quickRateDislikeCount : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"quickRateDislikeCount","hash":{},"data":data}) : helper)))
    + "\" style=\"font-weight: 900;\">"
    + alias4(((helper = (helper = helpers.quickRateDislikeCount || (depth0 != null ? depth0.quickRateDislikeCount : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"quickRateDislikeCount","hash":{},"data":data}) : helper)))
    + "</data>\n                <span class=\"h4\" style=\"display: block; font-weight: 300;\">"
    + alias4(((helper = (helper = helpers.dislikeName || (depth0 != null ? depth0.dislikeName : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"dislikeName","hash":{},"data":data}) : helper)))
    + "</span>\n            </div>\n        </div>\n";
},"7":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : {}, alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "            <div class=\"num h1 mb0\">\n                    <span class=\"icon-to-go mr10\">\n                        <svg>\n                          <use xmlns:xlink=\"http://www.w3.org/1999/xlink\" xlink:href=\"#timer\"></use>\n                        </svg>\n                    </span>\n                <data itemprop=\"Project[remainingDays]\" data-format=\"number\"\n                      data-value=\""
    + alias4(((helper = (helper = helpers.remainingDays || (depth0 != null ? depth0.remainingDays : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"remainingDays","hash":{},"data":data}) : helper)))
    + "\">"
    + alias4(((helper = (helper = helpers.remainingDays || (depth0 != null ? depth0.remainingDays : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"remainingDays","hash":{},"data":data}) : helper)))
    + "</data>\n                <span class=\"h4\">"
    + alias4(container.lambda(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.projectdetailspagefundingcarddaysleft : stack1), depth0))
    + "</span>\n            </div>\n        </div>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : {}, alias2=helpers.helperMissing;

  return "<div class=\"row\" style=\"\n  display: flex;\n  justify-content: center;\n  flex-direction: column;\n  text-align: center;\nheight: 300px;\">\n    <div class=\"col-xs-6 mb20 stat-item\" style=\"border-left: 6px solid #00b6f1; -webkit-box-shadow: 0px 0px 20px 0px rgba(0,0,0,0.21);\n-moz-box-shadow: 0px 0px 20px 0px rgba(0,0,0,0.21);\nbox-shadow: 0px 0px 20px 0px rgba(0,0,0,0.21); -webkit-border-radius: 10px;-moz-border-radius: 10px;border-radius: 10px; height: 130px;justify-content: center;\nflex-direction: column;\ntext-align: center; display: flex;\">\n"
    + ((stack1 = (helpers.ifCond || (depth0 && depth0.ifCond) || alias2).call(alias1,(depth0 != null ? depth0.status : depth0),"==",2,{"name":"ifCond","hash":{},"fn":container.program(1, data, 0),"inverse":container.program(3, data, 0),"data":data})) != null ? stack1 : "")
    + "    </div>\n\n    <div class=\"col-xs-6 mb30 stat-item\" style=\"border-left: 6px solid #ffcb05; -webkit-box-shadow: 0px 0px 20px 0px rgba(0,0,0,0.21);\n-moz-box-shadow: 0px 0px 20px 0px rgba(0,0,0,0.21);\nbox-shadow: 0px 0px 20px 0px rgba(0,0,0,0.21); -webkit-border-radius: 10px;-moz-border-radius: 10px;border-radius: 10px; height: 130px;justify-content: center;\nflex-direction: column;\ntext-align: center; display: flex;\">\n    <div class=\"Project834766624\">\n    <div class=\"ksr_page_timer poll stat\">\n"
    + ((stack1 = (helpers.ifCond || (depth0 && depth0.ifCond) || alias2).call(alias1,(depth0 != null ? depth0.status : depth0),"==",2,{"name":"ifCond","hash":{},"fn":container.program(5, data, 0),"inverse":container.program(7, data, 0),"data":data})) != null ? stack1 : "")
    + "    </div>\n</div>\n";
},"useData":true});
templates['project-stats'] = template({"1":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : {}, alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "            <div class=\"num h1 mb0 mt10\">\n                    <span class=\"icon-to-go mr10\" data-value=\"1\">\n                        <img src=\""
    + alias4(((helper = (helper = helpers.likeIcon || (depth0 != null ? depth0.likeIcon : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"likeIcon","hash":{},"data":data}) : helper)))
    + "\" style=\"float:left; margin-top: 5px; width: 100%; height: 100%;\"/>\n                    </span>\n                <data itemprop=\"Project[quickRateLikeCount]\" data-format=\"number\"\n                      data-value=\""
    + alias4(((helper = (helper = helpers.quickRateLikeCount || (depth0 != null ? depth0.quickRateLikeCount : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"quickRateLikeCount","hash":{},"data":data}) : helper)))
    + "\" style=\"font-weight: 900;\">"
    + alias4(((helper = (helper = helpers.quickRateLikeCount || (depth0 != null ? depth0.quickRateLikeCount : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"quickRateLikeCount","hash":{},"data":data}) : helper)))
    + "</data>\n                <span class=\"h4\" style=\"display: block; font-weight: 300;\">"
    + alias4(((helper = (helper = helpers.likeName || (depth0 != null ? depth0.likeName : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"likeName","hash":{},"data":data}) : helper)))
    + "</span>\n            </div>\n";
},"3":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : {}, alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "            <div class=\"num h1 mb0 mt10\" data-backers-count=\"200\" id=\"backersCount\">\n            <span class=\"icon-to-go mr10\">\n              <svg>\n                <use xmlns:xlink=\"http://www.w3.org/1999/xlink\" xlink:href=\"#backers\"></use>\n              </svg>\n            </span>\n                <data itemprop=\"Project[backersCount]\" data-format=\"number\"\n                      data-value=\""
    + alias4(((helper = (helper = helpers.backersCount || (depth0 != null ? depth0.backersCount : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"backersCount","hash":{},"data":data}) : helper)))
    + "\">"
    + alias4(((helper = (helper = helpers.backersCount || (depth0 != null ? depth0.backersCount : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"backersCount","hash":{},"data":data}) : helper)))
    + "</data>\n                <span style=\"position: relative;\" class=\"h4\">"
    + alias4(container.lambda(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.projectdetailspagefundingcardbacker : stack1), depth0))
    + "</span>\n            </div>\n";
},"5":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : {}, alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "            <div class=\"num h1 mb0\">\n                    <span class=\"icon-to-go mr10\" data-value=\"2\">\n                        <img src=\""
    + alias4(((helper = (helper = helpers.dislikeIcon || (depth0 != null ? depth0.dislikeIcon : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"dislikeIcon","hash":{},"data":data}) : helper)))
    + "\" style=\"float:left; margin-top: 5px; width: 100%; height: 100%;\"/>\n                    </span>\n                <data itemprop=\"Project[quickRateDislikeCount]\" data-format=\"number\"\n                      data-value=\""
    + alias4(((helper = (helper = helpers.quickRateDislikeCount || (depth0 != null ? depth0.quickRateDislikeCount : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"quickRateDislikeCount","hash":{},"data":data}) : helper)))
    + "\" style=\"font-weight: 900;\">"
    + alias4(((helper = (helper = helpers.quickRateDislikeCount || (depth0 != null ? depth0.quickRateDislikeCount : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"quickRateDislikeCount","hash":{},"data":data}) : helper)))
    + "</data>\n                <span class=\"h4\" style=\"display: block; font-weight: 300;\">"
    + alias4(((helper = (helper = helpers.dislikeName || (depth0 != null ? depth0.dislikeName : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"dislikeName","hash":{},"data":data}) : helper)))
    + "</span>\n            </div>\n        </div>\n";
},"7":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : {}, alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "            <div class=\"num h1 mb0\">\n                    <span class=\"icon-to-go mr10\">\n                        <svg>\n                          <use xmlns:xlink=\"http://www.w3.org/1999/xlink\" xlink:href=\"#timer\"></use>\n                        </svg>\n                    </span>\n                <data itemprop=\"Project[remainingDays]\" data-format=\"number\"\n                      data-value=\""
    + alias4(((helper = (helper = helpers.remainingDays || (depth0 != null ? depth0.remainingDays : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"remainingDays","hash":{},"data":data}) : helper)))
    + "\">"
    + alias4(((helper = (helper = helpers.remainingDays || (depth0 != null ? depth0.remainingDays : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"remainingDays","hash":{},"data":data}) : helper)))
    + "</data>\n                <span class=\"h4\">"
    + alias4(container.lambda(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.projectdetailspagefundingcarddaysleft : stack1), depth0))
    + "</span>\n            </div>\n        </div>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=depth0 != null ? depth0 : {}, alias2=helpers.helperMissing;

  return "<div class=\"row\" style=\"\n  display: flex;\n  justify-content: center;\n  flex-direction: column;\n  text-align: center;\nheight: 500px;\">\n    <div class=\"col-xs-12 mb20 stat-item\" style=\"border-left: 6px solid #00b6f1; -webkit-box-shadow: 0px 0px 20px 0px rgba(0,0,0,0.21);\n-moz-box-shadow: 0px 0px 20px 0px rgba(0,0,0,0.21);\nbox-shadow: 0px 0px 20px 0px rgba(0,0,0,0.21); -webkit-border-radius: 10px;-moz-border-radius: 10px;border-radius: 10px; height: 130px;justify-content: center;\nflex-direction: column;\ntext-align: center; display: flex;\">\n"
    + ((stack1 = (helpers.ifCond || (depth0 && depth0.ifCond) || alias2).call(alias1,(depth0 != null ? depth0.status : depth0),"==",2,{"name":"ifCond","hash":{},"fn":container.program(1, data, 0),"inverse":container.program(3, data, 0),"data":data})) != null ? stack1 : "")
    + "    </div>\n\n    <div class=\"col-xs-12 mb30 stat-item\" style=\"border-left: 6px solid #ffcb05; -webkit-box-shadow: 0px 0px 20px 0px rgba(0,0,0,0.21);\n-moz-box-shadow: 0px 0px 20px 0px rgba(0,0,0,0.21);\nbox-shadow: 0px 0px 20px 0px rgba(0,0,0,0.21); -webkit-border-radius: 10px;-moz-border-radius: 10px;border-radius: 10px; height: 130px;justify-content: center;\nflex-direction: column;\ntext-align: center; display: flex;\">\n    <div class=\"Project834766624\">\n    <div class=\"ksr_page_timer poll stat\">\n"
    + ((stack1 = (helpers.ifCond || (depth0 && depth0.ifCond) || alias2).call(alias1,(depth0 != null ? depth0.status : depth0),"==",2,{"name":"ifCond","hash":{},"fn":container.program(5, data, 0),"inverse":container.program(7, data, 0),"data":data})) != null ? stack1 : "")
    + "    </div>\n</div>\n";
},"useData":true});
templates['projects-loading'] = template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    return "<div class=\"col-xs-4\">\n    <div class=\"project-card project-card-tall-small NS_projects__stats border-top\">\n        <div class=\"project-thumbnail contain\"></div>\n        <div class=\"project-card-content\">\n            <div class=\"animated-background facebook\">\n                <div class=\"background-masker header-top\"></div>\n                <div class=\"background-masker header-left\"></div>\n                <div class=\"background-masker header-right\"></div>\n                <div class=\"background-masker header-bottom\"></div>\n                <div class=\"background-masker subheader-left\"></div>\n                <div class=\"background-masker subheader-right\"></div>\n                <div class=\"background-masker subheader-bottom\"></div>\n                <div class=\"background-masker content-top\"></div>\n                <div class=\"background-masker content-first-end\"></div>\n                <div class=\"background-masker content-second-line\"></div>\n                <div class=\"background-masker content-second-end\"></div>\n                <div class=\"background-masker content-third-line\"></div>\n                <div class=\"background-masker content-third-end\"></div>\n            </div>\n            <div class=\"animated-background facebook mt30\">\n                <div class=\"background-masker header-top\"></div>\n                <div class=\"background-masker header-left\"></div>\n                <div class=\"background-masker header-right\"></div>\n                <div class=\"background-masker header-bottom\"></div>\n                <div class=\"background-masker subheader-left\"></div>\n                <div class=\"background-masker subheader-right\"></div>\n                <div class=\"background-masker subheader-bottom\"></div>\n                <div class=\"background-masker content-top\"></div>\n                <div class=\"background-masker content-first-end\"></div>\n                <div class=\"background-masker content-second-line\"></div>\n                <div class=\"background-masker content-second-end\"></div>\n                <div class=\"background-masker content-third-line\"></div>\n                <div class=\"background-masker content-third-end\"></div>\n            </div>\n        </div>\n    </div>\n</div>\n\n<div class=\"col-xs-4\">\n    <div class=\"project-card project-card-tall-small NS_projects__stats border-top\">\n        <div class=\"project-thumbnail contain\"></div>\n        <div class=\"project-card-content\">\n            <div class=\"animated-background facebook\">\n                <div class=\"background-masker header-top\"></div>\n                <div class=\"background-masker header-left\"></div>\n                <div class=\"background-masker header-right\"></div>\n                <div class=\"background-masker header-bottom\"></div>\n                <div class=\"background-masker subheader-left\"></div>\n                <div class=\"background-masker subheader-right\"></div>\n                <div class=\"background-masker subheader-bottom\"></div>\n                <div class=\"background-masker content-top\"></div>\n                <div class=\"background-masker content-first-end\"></div>\n                <div class=\"background-masker content-second-line\"></div>\n                <div class=\"background-masker content-second-end\"></div>\n                <div class=\"background-masker content-third-line\"></div>\n                <div class=\"background-masker content-third-end\"></div>\n            </div>\n            <div class=\"animated-background facebook mt30\">\n                <div class=\"background-masker header-top\"></div>\n                <div class=\"background-masker header-left\"></div>\n                <div class=\"background-masker header-right\"></div>\n                <div class=\"background-masker header-bottom\"></div>\n                <div class=\"background-masker subheader-left\"></div>\n                <div class=\"background-masker subheader-right\"></div>\n                <div class=\"background-masker subheader-bottom\"></div>\n                <div class=\"background-masker content-top\"></div>\n                <div class=\"background-masker content-first-end\"></div>\n                <div class=\"background-masker content-second-line\"></div>\n                <div class=\"background-masker content-second-end\"></div>\n                <div class=\"background-masker content-third-line\"></div>\n                <div class=\"background-masker content-third-end\"></div>\n            </div>\n        </div>\n    </div>\n</div>\n\n<div class=\"col-xs-4\">\n    <div class=\"project-card project-card-tall-small NS_projects__stats border-top\">\n        <div class=\"project-thumbnail contain\"></div>\n        <div class=\"project-card-content\">\n            <div class=\"animated-background facebook\">\n                <div class=\"background-masker header-top\"></div>\n                <div class=\"background-masker header-left\"></div>\n                <div class=\"background-masker header-right\"></div>\n                <div class=\"background-masker header-bottom\"></div>\n                <div class=\"background-masker subheader-left\"></div>\n                <div class=\"background-masker subheader-right\"></div>\n                <div class=\"background-masker subheader-bottom\"></div>\n                <div class=\"background-masker content-top\"></div>\n                <div class=\"background-masker content-first-end\"></div>\n                <div class=\"background-masker content-second-line\"></div>\n                <div class=\"background-masker content-second-end\"></div>\n                <div class=\"background-masker content-third-line\"></div>\n                <div class=\"background-masker content-third-end\"></div>\n            </div>\n            <div class=\"animated-background facebook mt30\">\n                <div class=\"background-masker header-top\"></div>\n                <div class=\"background-masker header-left\"></div>\n                <div class=\"background-masker header-right\"></div>\n                <div class=\"background-masker header-bottom\"></div>\n                <div class=\"background-masker subheader-left\"></div>\n                <div class=\"background-masker subheader-right\"></div>\n                <div class=\"background-masker subheader-bottom\"></div>\n                <div class=\"background-masker content-top\"></div>\n                <div class=\"background-masker content-first-end\"></div>\n                <div class=\"background-masker content-second-line\"></div>\n                <div class=\"background-masker content-second-end\"></div>\n                <div class=\"background-masker content-third-line\"></div>\n                <div class=\"background-masker content-third-end\"></div>\n            </div>\n        </div>\n    </div>\n</div>\n";
},"useData":true});
templates['projects-mob'] = template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : {}, alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "	  <div class=\""
    + ((stack1 = (helpers.ternary || (depth0 && depth0.ternary) || alias2).call(alias1,(depth0 != null ? depth0.favourite : depth0),"project-card project-card-tall-small NS_projects__stats fadeInDown","project-card project-card-tall-big NS_projects__stats",{"name":"ternary","hash":{},"data":data})) != null ? stack1 : "")
    + "\" style=\"animation-delay: "
    + ((stack1 = (helpers.animationDelay || (depth0 && depth0.animationDelay) || alias2).call(alias1,(data && data.index),{"name":"animationDelay","hash":{},"data":data})) != null ? stack1 : "")
    + "; height: 415px;\">\n		  <!--<div class=\"project-badge\">\n			  <svg>\n				  <use xmlns:xlink=\"http://www.w3.org/1999/xlink\" xlink:href=\"#"
    + ((stack1 = (helpers.badgeHelper || (depth0 && depth0.badgeHelper) || alias2).call(alias1,(depth0 != null ? depth0.status : depth0),{"name":"badgeHelper","hash":{},"data":data})) != null ? stack1 : "")
    + "\"></use>\n			  </svg>\n		  </div>-->\n		  <div class=\"project-thumbnail\" style=\"background-image: url('"
    + alias4(((helper = (helper = helpers.image || (depth0 != null ? depth0.image : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"image","hash":{},"data":data}) : helper)))
    + "'); position: relative\">\n			  <a href=\"/projeler/"
    + alias4(((helper = (helper = helpers.urlPostfix || (depth0 != null ? depth0.urlPostfix : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"urlPostfix","hash":{},"data":data}) : helper)))
    + "/detay\">\n				  <img alt=\""
    + alias4(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data}) : helper)))
    + "\" title=\""
    + alias4(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data}) : helper)))
    + "\" class=\"project-thumbnail-img\" src=\""
    + alias4(((helper = (helper = helpers.image || (depth0 != null ? depth0.image : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"image","hash":{},"data":data}) : helper)))
    + "\" width=\"100%\">\n"
    + ((stack1 = helpers["if"].call(alias1,((stack1 = (depth0 != null ? depth0.partner : depth0)) != null ? stack1.badgeImage : stack1),{"name":"if","hash":{},"fn":container.program(2, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "			  </a>\n		  </div>\n		  <div class=\"project-card-content\">\n			  <h4 class=\"project-title\">\n				  <a data-score=\"null\" data-version=\"null\" href=\"/projeler/"
    + alias4(((helper = (helper = helpers.urlPostfix || (depth0 != null ? depth0.urlPostfix : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"urlPostfix","hash":{},"data":data}) : helper)))
    + "/detay\">"
    + alias4(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data}) : helper)))
    + "</a>\n			  </h4>\n              <p>"
    + alias4(((helper = (helper = helpers.teamName || (depth0 != null ? depth0.teamName : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"teamName","hash":{},"data":data}) : helper)))
    + "</p>\n			  <!--<p class=\"project-blurb\">"
    + alias4(((helper = (helper = helpers.shortDescription || (depth0 != null ? depth0.shortDescription : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"shortDescription","hash":{},"data":data}) : helper)))
    + "</p>-->\n		  </div>\n		  <div class=\"project-card-footer\">\n			    <div class=\"project-stats-container project-card--thinking\">\n				    <ul class=\"project-stats\">\n					    <li class=\"h4 yellow-dark text-right ml40 pull-left\">\n                        <span class=\"goal like\">\n                            <svg>\n	                            <use xmlns:xlink=\"http://www.w3.org/1999/xlink\" xlink:href=\"#like\"></use>\n                            </svg>\n                        </span>\n						    <span class=\"goal num h3 ml10\">"
    + alias4(((helper = (helper = helpers.quickRateLikeCount || (depth0 != null ? depth0.quickRateLikeCount : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"quickRateLikeCount","hash":{},"data":data}) : helper)))
    + "</span>\n					    </li>\n					    <li class=\"h4 mr40 pull-right\">\n                        <span class=\"goal like project-stats-label\">\n                            <svg>\n	                            <use xmlns:xlink=\"http://www.w3.org/1999/xlink\" xlink:href=\"#dislike\"></use>\n                            </svg>\n                        </span>\n						    <span class=\"goal num h3 ml10\">"
    + alias4(((helper = (helper = helpers.quickRateDislikeCount || (depth0 != null ? depth0.quickRateDislikeCount : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"quickRateDislikeCount","hash":{},"data":data}) : helper)))
    + "</span>\n					    </li>\n				    </ul>\n				    <!--<div class=\"project-stats-value mt15 text-center\">\n					    <div class=\"h4 pl15 mb0\">\n                      <span class=\"comment mr10\">\n                          <svg>\n	                          <use xmlns:xlink=\"http://www.w3.org/1999/xlink\" xlink:href=\"#comment\"></use>\n                          </svg>\n                      </span><span class=\"goal text inline-block\">"
    + alias4(((helper = (helper = helpers.commentCount || (depth0 != null ? depth0.commentCount : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"commentCount","hash":{},"data":data}) : helper)))
    + " yorum</span>\n					    </div>\n				    </div>-->\n			    </div>\n		  </div>\n	  </div>\n</div>\n";
},"2":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : {}, alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "            <img alt=\""
    + alias4(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data}) : helper)))
    + "\" title=\""
    + alias4(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data}) : helper)))
    + "\" class=\"project-badge-img\" src=\""
    + alias4(container.lambda(((stack1 = (depth0 != null ? depth0.partner : depth0)) != null ? stack1.badgeImage : stack1), depth0))
    + "\" width=\"100%\">\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0 != null ? depth0 : {},depth0,{"name":"each","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"useData":true});
templates['projects'] = template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : {}, alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "	  <div class=\""
    + ((stack1 = (helpers.ternary || (depth0 && depth0.ternary) || alias2).call(alias1,(depth0 != null ? depth0.favourite : depth0),"project-card project-card-tall-small NS_projects__stats fadeInDown","project-card project-card-tall-big NS_projects__stats",{"name":"ternary","hash":{},"data":data})) != null ? stack1 : "")
    + "\" style=\"animation-delay: "
    + ((stack1 = (helpers.animationDelay || (depth0 && depth0.animationDelay) || alias2).call(alias1,(data && data.index),{"name":"animationDelay","hash":{},"data":data})) != null ? stack1 : "")
    + "; width: auto; float: left; margin-right: 30px; height: 415px;\">\n		  <!--<div class=\"project-badge\">\n			  <svg>\n				  <use xmlns:xlink=\"http://www.w3.org/1999/xlink\" xlink:href=\"#"
    + ((stack1 = (helpers.badgeHelper || (depth0 && depth0.badgeHelper) || alias2).call(alias1,(depth0 != null ? depth0.status : depth0),{"name":"badgeHelper","hash":{},"data":data})) != null ? stack1 : "")
    + "\"></use>\n			  </svg>\n		  </div>-->\n		  <div class=\"project-thumbnail\" style=\"background-image: url('"
    + alias4(((helper = (helper = helpers.image || (depth0 != null ? depth0.image : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"image","hash":{},"data":data}) : helper)))
    + "'); position: relative; background-size: cover; background-repeat: no-repeat; background-position: center center;\">\n			  <a href=\"/projeler/"
    + alias4(((helper = (helper = helpers.urlPostfix || (depth0 != null ? depth0.urlPostfix : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"urlPostfix","hash":{},"data":data}) : helper)))
    + "/detay\">\n				  <img alt=\""
    + alias4(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data}) : helper)))
    + "\" title=\""
    + alias4(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data}) : helper)))
    + "\" class=\"project-thumbnail-img\" src=\""
    + alias4(((helper = (helper = helpers.image || (depth0 != null ? depth0.image : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"image","hash":{},"data":data}) : helper)))
    + "\" width=\"100%\">\n"
    + ((stack1 = helpers["if"].call(alias1,((stack1 = (depth0 != null ? depth0.partner : depth0)) != null ? stack1.badgeImage : stack1),{"name":"if","hash":{},"fn":container.program(2, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "			  </a>\n		  </div>\n		  <div class=\"project-card-content\">\n			  <h4 class=\"project-title\">\n				  <a data-score=\"null\" data-version=\"null\" href=\"/projeler/"
    + alias4(((helper = (helper = helpers.urlPostfix || (depth0 != null ? depth0.urlPostfix : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"urlPostfix","hash":{},"data":data}) : helper)))
    + "/detay\">"
    + alias4(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data}) : helper)))
    + "</a>\n			  </h4>\n              <p>"
    + alias4(((helper = (helper = helpers.teamName || (depth0 != null ? depth0.teamName : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"teamName","hash":{},"data":data}) : helper)))
    + "</p>\n			  <!--<p class=\"project-blurb\">"
    + alias4(((helper = (helper = helpers.shortDescription || (depth0 != null ? depth0.shortDescription : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"shortDescription","hash":{},"data":data}) : helper)))
    + "</p>-->\n		  </div>\n		  <div class=\"project-card-footer\">\n			    <div class=\"project-stats-container project-card--thinking\">\n				    <ul class=\"project-stats\">\n					    <li class=\"h4 yellow-dark text-right ml40 pull-left\">\n                        <span class=\"goal like\">\n                            <svg>\n	                            <use xmlns:xlink=\"http://www.w3.org/1999/xlink\" xlink:href=\"#like\"></use>\n                            </svg>\n                        </span>\n						    <span class=\"goal num h3 ml10\">"
    + alias4(((helper = (helper = helpers.quickRateLikeCount || (depth0 != null ? depth0.quickRateLikeCount : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"quickRateLikeCount","hash":{},"data":data}) : helper)))
    + "</span>\n					    </li>\n					    <li class=\"h4 mr40 pull-right\">\n                        <span class=\"goal like project-stats-label\">\n                            <svg>\n	                            <use xmlns:xlink=\"http://www.w3.org/1999/xlink\" xlink:href=\"#dislike\"></use>\n                            </svg>\n                        </span>\n						    <span class=\"goal num h3 ml10\">"
    + alias4(((helper = (helper = helpers.quickRateDislikeCount || (depth0 != null ? depth0.quickRateDislikeCount : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"quickRateDislikeCount","hash":{},"data":data}) : helper)))
    + "</span>\n					    </li>\n				    </ul>\n				    <!--<div class=\"project-stats-value mt15 text-center\">\n					    <div class=\"h4 pl15 mb0\">\n                      <span class=\"comment mr10\">\n                          <svg>\n	                          <use xmlns:xlink=\"http://www.w3.org/1999/xlink\" xlink:href=\"#comment\"></use>\n                          </svg>\n                      </span><span class=\"goal text inline-block\">"
    + alias4(((helper = (helper = helpers.commentCount || (depth0 != null ? depth0.commentCount : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"commentCount","hash":{},"data":data}) : helper)))
    + " yorum</span>\n					    </div>\n				    </div>-->\n			    </div>\n		  </div>\n	  </div>\n</div>\n";
},"2":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : {}, alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "            <img alt=\""
    + alias4(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data}) : helper)))
    + "\" title=\""
    + alias4(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data}) : helper)))
    + "\" class=\"project-badge-img\" src=\""
    + alias4(container.lambda(((stack1 = (depth0 != null ? depth0.partner : depth0)) != null ? stack1.badgeImage : stack1), depth0))
    + "\" width=\"100%\">\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0 != null ? depth0 : {},depth0,{"name":"each","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"useData":true});
templates['reward-detail'] = template({"1":function(container,depth0,helpers,partials,data) {
    var helper, alias1=depth0 != null ? depth0 : {}, alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression;

  return "    <li class=\"reward col-xs-3\" data-reward-id=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\">\n        <div class=\"reward__info rev-reward-info\" style=\"text-align: center\">\n	        <div class=\"corp-comment-user-image\" style=\"background-image: url("
    + alias4(((helper = (helper = helpers.image || (depth0 != null ? depth0.image : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"image","hash":{},"data":data}) : helper)))
    + ")\"></div>\n	        <h4>"
    + alias4(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data}) : helper)))
    + " "
    + alias4(((helper = (helper = helpers.surname || (depth0 != null ? depth0.surname : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"surname","hash":{},"data":data}) : helper)))
    + "</h4>\n	        <div class=\"corp-bugcount\"><span>"
    + alias4(((helper = (helper = helpers.score || (depth0 != null ? depth0.score : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"score","hash":{},"data":data}) : helper)))
    + "</span></div>\n        </div>\n    </li>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0 != null ? depth0 : {},depth0,{"name":"each","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"useData":true});
templates['reward-single'] = template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, alias1=container.lambda, alias2=container.escapeExpression;

  return "<li class=\"reward\">\n    <div class=\"grey-field clearfix mb20\">\n        <div class=\"fields backer_rewards_fields\">\n            <label class=\"primary help\">\n              "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.fillprojectdetailspage3textboxrewardtext : stack1), depth0))
    + " #<span class=\"reward-num\">1</span><span class=\"icon tsr-icon__help ml5\"></span>\n            </label>\n            <div class=\"field-wrapper\">\n                <div class=\"field-help\" style=\"display: none;\">\n                    <p>\n                        <strong>Estimated delivery:</strong>\n                        This is the date you expect to deliver the reward to backers. If the reward includes multiple items, choose the date you expect the final item to be delivered.\n                        <br>\n                        <strong>Shipping details:</strong>\n                        Select whether your reward requires shipping, and whether there are any shipping restrictions. If you need, you can add specific locations and assign them specific shipping costs. (The details for a precise country, like \"France,\" will always take precedence over larger regions, like the \"European Union.\")\n                        <br>\n                        <strong>Limit quantity</strong>\n                        You can set a number and limit the availability of a particular reward. Once the limit is reached, the reward will be marked as \"sold out.\"\n                    </p>\n                </div>\n                <div class=\"field-container\">\n                    <input type=\"hidden\" class=\"rewardId\" />\n                    <div class=\"keep-or-delete js-unless_has_backers\">\n                        <a class=\"delete-reward deleteReward\" style=\"\" title=\""
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.projectrewarddeleteapprove : stack1), depth0))
    + "\">\n                            <span class=\"tsr-icon__delete\"></span>\n                            "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.projectrewarddeletetext : stack1), depth0))
    + "\n                        </a>\n                    </div>\n                    <div class=\"minimum\">\n                        <label>"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.fillprojectdetailspage3textboxrewardsubboxesamount : stack1), depth0))
    + "</label>\n                        <input class=\"minAmount currency\" type=\"text\" data-parsley-required data-parsley-min=\"1\">\n                    </div>\n                    <div class=\"description border-top\">\n                        <label>"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.fillprojectdetailspage3textboxrewardsubboxesname : stack1), depth0))
    + "</label>\n                        <input class=\"name\" type=\"text\" data-parsley-required />\n                    </div>\n                    <div class=\"description border-top-bottom\">\n                        <label>"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.fillprojectdetailspage3textboxrewardsubboxesdesc : stack1), depth0))
    + "</label>\n                        <textarea class=\"descriptionText\" maxlength=\"200\" data-parsley-required></textarea>\n                    </div>\n                    <div class=\"delivery-date\">\n                        <label>"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.fillprojectdetailspage3textboxrewardsubboxesdeliverytime : stack1), depth0))
    + "</label>\n                        <div class=\"date-container\">\n                            <select class=\"month select\" data-parsley-min=\"1\" data-parsley-min-message=\"Lütfen ay seçiniz\">\n                               <!-- <option value=\"0\">Ay Seçiniz</option>-->\n                                <option value=\"1\">Ocak</option>\n                                <option value=\"2\">Şubat</option>\n                                <option value=\"3\">Mart</option>\n                                <option value=\"4\">Nisan</option>\n                                <option value=\"5\">Mayıs</option>\n                                <option value=\"6\">Haziran</option>\n                                <option value=\"7\">Temmuz</option>\n                                <option value=\"8\">Ağustos</option>\n                                <option value=\"9\">Eylül</option>\n                                <option value=\"10\">Ekim</option>\n                                <option value=\"11\">Kasım</option>\n                                <option value=\"12\">Aralık</option></select>\n                            <select class=\"year select\" data-parsley-min=\"1\" data-parsley-min-message=\"Lütfen yıl seçiniz\">\n                               <!-- <option value=\"0\">Yıl Seçiniz</option>-->\n                                <option value=\"2016\">2016</option>\n                                <option value=\"2017\">2017</option>\n                                <option value=\"2018\">2018</option>\n                                <option value=\"2019\">2019</option>\n                                <option value=\"2020\">2020</option>\n                                <option value=\"2021\">2021</option>\n                                <option value=\"2022\">2022</option>\n                                <option value=\"2023\">2023</option>\n                                <option value=\"2024\">2024</option>\n                                <option value=\"2025\">2025</option>\n                            </select>\n                        </div>\n                    </div>\n                    <div class=\"limit checkbox\" style=\"margin: auto\">\n                        <label class=\"limit-label\">\n                            <input class=\"limit-checkbox\" type=\"checkbox\" name=\"limit-checkbox\" data-parsley-excluded />\n                            <span class=\"cr white-cb\"><i class=\"cr-icon glyphicon glyphicon-ok\"></i></span>\n                          "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.projectrewardrestrictedamount : stack1), depth0))
    + "\n                        </label>\n                        <input class=\"count\" type=\"text\" data-parsley-excluded />\n                    </div>\n                </div>\n            </div>\n        </div>\n\n    </div>\n</li>\n";
},"useData":true});
templates['reward'] = template({"1":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=depth0 != null ? depth0 : {}, alias2=helpers.helperMissing, alias3="function", alias4=container.escapeExpression, alias5=container.lambda;

  return "<li class=\"reward\">\n    <div class=\"grey-field clearfix mb20\">\n        <div class=\"fields backer_rewards_fields\">\n            <label class=\"primary help\">\n                Ödül #<span class=\"reward-num\">"
    + ((stack1 = (helpers.math || (depth0 && depth0.math) || alias2).call(alias1,(data && data.index),"+",1,{"name":"math","hash":{},"data":data})) != null ? stack1 : "")
    + "</span><span class=\"icon tsr-icon__help ml5\"></span>\n            </label>\n            <div class=\"field-wrapper\">\n                <div class=\"field-help\" style=\"display: none;\">\n                    <p>\n                        <strong>Estimated delivery:</strong>\n                        This is the date you expect to deliver the reward to backers. If the reward includes multiple items, choose the date you expect the final item to be delivered.\n                        <br>\n                        <strong>Shipping details:</strong>\n                        Select whether your reward requires shipping, and whether there are any shipping restrictions. If you need, you can add specific locations and assign them specific shipping costs. (The details for a precise country, like \"France,\" will always take precedence over larger regions, like the \"European Union.\")\n                        <br>\n                        <strong>Limit quantity</strong>\n                        You can set a number and limit the availability of a particular reward. Once the limit is reached, the reward will be marked as \"sold out.\"\n                    </p>\n                </div>\n                <div class=\"field-container\">\n                    <input type=\"hidden\" class=\"rewardId\" value=\""
    + alias4(((helper = (helper = helpers.id || (depth0 != null ? depth0.id : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"id","hash":{},"data":data}) : helper)))
    + "\" />\n                    <div class=\"keep-or-delete js-unless_has_backers\">\n                        <a class=\"delete-reward deleteReward\" style=\"\" title=\""
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.projectrewarddeleteapprove : stack1), depth0))
    + "\">\n                            <span class=\"tsr-icon__delete\"></span>\n                            <span>"
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.profilepageaddtimelineactivitiestablecolumns5 : stack1), depth0))
    + "</span>\n                        </a>\n                    </div>\n                    <div class=\"minimum\">\n                        <label>"
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.fillprojectdetailspage3textboxrewardsubboxesamount : stack1), depth0))
    + "</label>\n                        <input class=\"minAmount currency\" type=\"text\" value=\""
    + alias4(((helper = (helper = helpers.minAmount || (depth0 != null ? depth0.minAmount : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"minAmount","hash":{},"data":data}) : helper)))
    + "\" data-parsley-required data-parsley-min=\"1\">\n                    </div>\n                    <div class=\"description border-top\">\n                        <label>"
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.fillprojectdetailspage3textboxrewardsubboxesname : stack1), depth0))
    + "</label>\n                        <input class=\"name\" type=\"text\" value=\""
    + alias4(((helper = (helper = helpers.name || (depth0 != null ? depth0.name : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"name","hash":{},"data":data}) : helper)))
    + "\" data-parsley-required />\n                    </div>\n                    <div class=\"description border-top-bottom\">\n                        <label>"
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.fillprojectdetailspage3textboxrewardsubboxesdesc : stack1), depth0))
    + "</label>\n                        <textarea class=\"descriptionText\" maxlength=\"200\" data-parsley-required>"
    + alias4(((helper = (helper = helpers.description || (depth0 != null ? depth0.description : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"description","hash":{},"data":data}) : helper)))
    + "</textarea>\n                    </div>\n                    <div class=\"delivery-date\">\n                        <label>"
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.projectdetailspagerewardscarddelivery : stack1), depth0))
    + "</label>\n                        <div class=\"date-container\">\n                            <select class=\"month select\" data-parsley-min=\"1\" data-parsley-min-message=\"Lütfen ay seçiniz\">\n                               <!-- <option value=\"0\">Ay Seçiniz</option>-->\n                                <option value=\"1\" "
    + ((stack1 = (helpers.getMonth || (depth0 && depth0.getMonth) || alias2).call(alias1,(depth0 != null ? depth0.delivery : depth0),"1",{"name":"getMonth","hash":{},"data":data})) != null ? stack1 : "")
    + ">Ocak</option>\n                                <option value=\"2\" "
    + ((stack1 = (helpers.getMonth || (depth0 && depth0.getMonth) || alias2).call(alias1,(depth0 != null ? depth0.delivery : depth0),"2",{"name":"getMonth","hash":{},"data":data})) != null ? stack1 : "")
    + ">Şubat</option>\n                                <option value=\"3\" "
    + ((stack1 = (helpers.getMonth || (depth0 && depth0.getMonth) || alias2).call(alias1,(depth0 != null ? depth0.delivery : depth0),"3",{"name":"getMonth","hash":{},"data":data})) != null ? stack1 : "")
    + ">Mart</option>\n                                <option value=\"4\" "
    + ((stack1 = (helpers.getMonth || (depth0 && depth0.getMonth) || alias2).call(alias1,(depth0 != null ? depth0.delivery : depth0),"4",{"name":"getMonth","hash":{},"data":data})) != null ? stack1 : "")
    + ">Nisan</option>\n                                <option value=\"5\" "
    + ((stack1 = (helpers.getMonth || (depth0 && depth0.getMonth) || alias2).call(alias1,(depth0 != null ? depth0.delivery : depth0),"5",{"name":"getMonth","hash":{},"data":data})) != null ? stack1 : "")
    + ">Mayıs</option>\n                                <option value=\"6\" "
    + ((stack1 = (helpers.getMonth || (depth0 && depth0.getMonth) || alias2).call(alias1,(depth0 != null ? depth0.delivery : depth0),"6",{"name":"getMonth","hash":{},"data":data})) != null ? stack1 : "")
    + ">Haziran</option>\n                                <option value=\"7\" "
    + ((stack1 = (helpers.getMonth || (depth0 && depth0.getMonth) || alias2).call(alias1,(depth0 != null ? depth0.delivery : depth0),"7",{"name":"getMonth","hash":{},"data":data})) != null ? stack1 : "")
    + ">Temmuz</option>\n                                <option value=\"8\" "
    + ((stack1 = (helpers.getMonth || (depth0 && depth0.getMonth) || alias2).call(alias1,(depth0 != null ? depth0.delivery : depth0),"8",{"name":"getMonth","hash":{},"data":data})) != null ? stack1 : "")
    + ">Ağustos</option>\n                                <option value=\"9\" "
    + ((stack1 = (helpers.getMonth || (depth0 && depth0.getMonth) || alias2).call(alias1,(depth0 != null ? depth0.delivery : depth0),"9",{"name":"getMonth","hash":{},"data":data})) != null ? stack1 : "")
    + ">Eylül</option>\n                                <option value=\"10\" "
    + ((stack1 = (helpers.getMonth || (depth0 && depth0.getMonth) || alias2).call(alias1,(depth0 != null ? depth0.delivery : depth0),"10",{"name":"getMonth","hash":{},"data":data})) != null ? stack1 : "")
    + ">Ekim</option>\n                                <option value=\"11\" "
    + ((stack1 = (helpers.getMonth || (depth0 && depth0.getMonth) || alias2).call(alias1,(depth0 != null ? depth0.delivery : depth0),"11",{"name":"getMonth","hash":{},"data":data})) != null ? stack1 : "")
    + ">Kasım</option>\n                                <option value=\"12\" "
    + ((stack1 = (helpers.getMonth || (depth0 && depth0.getMonth) || alias2).call(alias1,(depth0 != null ? depth0.delivery : depth0),"12",{"name":"getMonth","hash":{},"data":data})) != null ? stack1 : "")
    + ">Aralık</option></select>\n                            <select class=\"year select\" data-parsley-min=\"1\" data-parsley-min-message=\"Lütfen yıl seçiniz\">\n                                <!--<option value=\"0\">Yıl Seçiniz</option>-->\n                                <option value=\"2016\" "
    + ((stack1 = (helpers.getYear || (depth0 && depth0.getYear) || alias2).call(alias1,(depth0 != null ? depth0.delivery : depth0),"2016",{"name":"getYear","hash":{},"data":data})) != null ? stack1 : "")
    + ">2016</option>\n                                <option value=\"2017\" "
    + ((stack1 = (helpers.getYear || (depth0 && depth0.getYear) || alias2).call(alias1,(depth0 != null ? depth0.delivery : depth0),"2017",{"name":"getYear","hash":{},"data":data})) != null ? stack1 : "")
    + ">2017</option>\n                                <option value=\"2018\" "
    + ((stack1 = (helpers.getYear || (depth0 && depth0.getYear) || alias2).call(alias1,(depth0 != null ? depth0.delivery : depth0),"2018",{"name":"getYear","hash":{},"data":data})) != null ? stack1 : "")
    + ">2018</option>\n                                <option value=\"2019\" "
    + ((stack1 = (helpers.getYear || (depth0 && depth0.getYear) || alias2).call(alias1,(depth0 != null ? depth0.delivery : depth0),"2019",{"name":"getYear","hash":{},"data":data})) != null ? stack1 : "")
    + ">2019</option>\n                                <option value=\"2020\" "
    + ((stack1 = (helpers.getYear || (depth0 && depth0.getYear) || alias2).call(alias1,(depth0 != null ? depth0.delivery : depth0),"2020",{"name":"getYear","hash":{},"data":data})) != null ? stack1 : "")
    + ">2020</option>\n                                <option value=\"2020\" "
    + ((stack1 = (helpers.getYear || (depth0 && depth0.getYear) || alias2).call(alias1,(depth0 != null ? depth0.delivery : depth0),"2021",{"name":"getYear","hash":{},"data":data})) != null ? stack1 : "")
    + ">2021</option>\n                                <option value=\"2020\" "
    + ((stack1 = (helpers.getYear || (depth0 && depth0.getYear) || alias2).call(alias1,(depth0 != null ? depth0.delivery : depth0),"2022",{"name":"getYear","hash":{},"data":data})) != null ? stack1 : "")
    + ">2022</option>\n                                <option value=\"2020\" "
    + ((stack1 = (helpers.getYear || (depth0 && depth0.getYear) || alias2).call(alias1,(depth0 != null ? depth0.delivery : depth0),"2023",{"name":"getYear","hash":{},"data":data})) != null ? stack1 : "")
    + ">2023</option>\n                                <option value=\"2020\" "
    + ((stack1 = (helpers.getYear || (depth0 && depth0.getYear) || alias2).call(alias1,(depth0 != null ? depth0.delivery : depth0),"2024",{"name":"getYear","hash":{},"data":data})) != null ? stack1 : "")
    + ">2024</option>\n                                <option value=\"2020\" "
    + ((stack1 = (helpers.getYear || (depth0 && depth0.getYear) || alias2).call(alias1,(depth0 != null ? depth0.delivery : depth0),"2025",{"name":"getYear","hash":{},"data":data})) != null ? stack1 : "")
    + ">2025</option>\n                            </select>\n                        </div>\n                    </div>\n                    <div class=\""
    + ((stack1 = (helpers.setLimit || (depth0 && depth0.setLimit) || alias2).call(alias1,(depth0 != null ? depth0.count : depth0),{"name":"setLimit","hash":{},"data":data})) != null ? stack1 : "")
    + "\" style=\"margin: auto\">\n                        <label class=\"limit-label\">\n                            <input class=\"limit-checkbox\" type=\"checkbox\" "
    + ((stack1 = (helpers.limitCount || (depth0 && depth0.limitCount) || alias2).call(alias1,(depth0 != null ? depth0.count : depth0),{"name":"limitCount","hash":{},"data":data})) != null ? stack1 : "")
    + " name=\"limit-checkbox\" data-parsley-excluded />\n                            <span class=\"cr white-cb\"><i class=\"cr-icon glyphicon glyphicon-ok\"></i></span>\n                            "
    + alias4(alias5(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.projectrewardrestrictedamount : stack1), depth0))
    + "\n                        </label>\n                        <input class=\"count\" type=\"text\" value=\""
    + alias4(((helper = (helper = helpers.count || (depth0 != null ? depth0.count : depth0)) != null ? helper : alias2),(typeof helper === alias3 ? helper.call(alias1,{"name":"count","hash":{},"data":data}) : helper)))
    + "\" data-parsley-excluded />\n                    </div>\n                </div>\n            </div>\n        </div>\n\n    </div>\n"
    + ((stack1 = helpers["if"].call(alias1,(depth0 != null ? depth0.locked : depth0),{"name":"if","hash":{},"fn":container.program(2, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "")
    + "</li>\n";
},"2":function(container,depth0,helpers,partials,data) {
    return "    <div class=\"reward__locked\"></div>\n";
},"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1;

  return ((stack1 = helpers.each.call(depth0 != null ? depth0 : {},depth0,{"name":"each","hash":{},"fn":container.program(1, data, 0),"inverse":container.noop,"data":data})) != null ? stack1 : "");
},"useData":true});
templates['story'] = template({"compiler":[7,">= 4.0.0"],"main":function(container,depth0,helpers,partials,data) {
    var stack1, helper, alias1=container.lambda, alias2=container.escapeExpression, alias3=depth0 != null ? depth0 : {}, alias4=helpers.helperMissing, alias5="function";

  return "<div class=\"center block\">\n    <h2 class=\"h2 green-dark fwbold\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.fillprojectdetailspage2textboxtitle : stack1), depth0))
    + "</h2>\n    <p class=\"h4 mt0 green-dark\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.fillprojectdetailspage2textboxsubtitle : stack1), depth0))
    + "</p>\n</div>\n\n<div id=\"form-story\" class=\"table-display full-width mt60 mb30 ptb20 plr10 m-center bg-white border\" data-parsley-validate>\n    <div class=\"row\">\n        <div class=\"col-xs-9\">\n            <div id=\"story-main\" class=\"project-main-form mr10\">\n                <input type=\"hidden\" id=\"video\" value=\""
    + alias2(((helper = (helper = helpers.video || (depth0 != null ? depth0.video : depth0)) != null ? helper : alias4),(typeof helper === alias5 ? helper.call(alias3,{"name":"video","hash":{},"data":data}) : helper)))
    + "\" />\n                <ol class=\"fields no-margin\">\n                    <li class=\"project-video\">\n                        <div class=\"grey-field mb20 clearfix\">\n                            <label for=\"projectVideo\">\n                                "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.fillprojectdetailspage2textboxvideotext : stack1), depth0))
    + "\n                            </label>\n                            <div class=\"field-wrapper\">\n                               <!-- Upload Video-->\n                                <div id=\"uploadVideoWrapper\" class=\"upload border\" style=\"height: 101px;\">\n                                    <input class=\"photo file\" id=\"projectVideo\" name=\"video\" value=\""
    + alias2(((helper = (helper = helpers.video || (depth0 != null ? depth0.video : depth0)) != null ? helper : alias4),(typeof helper === alias5 ? helper.call(alias3,{"name":"video","hash":{},"data":data}) : helper)))
    + "\" />\n                                    <strong class=\"center\">\n                                        "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.fillprojectdetailspage2textboxvideosubtext : stack1), depth0))
    + "\n                                    </strong>\n                                    <p style=\""
    + ((stack1 = (helpers.isTranscoding || (depth0 && depth0.isTranscoding) || alias4).call(alias3,(depth0 != null ? depth0.video : depth0),{"name":"isTranscoding","hash":{},"data":data})) != null ? stack1 : "")
    + "\" id=\"videoProcessing\" class=\"video-processing-message\">Yüklediğiniz video işlenmektedir.</p>\n                                    <p style=\""
    + ((stack1 = (helpers.isVideoReady || (depth0 && depth0.isVideoReady) || alias4).call(alias3,(depth0 != null ? depth0.video : depth0),{"name":"isVideoReady","hash":{},"data":data})) != null ? stack1 : "")
    + "\" id=\"videoIsReady\" class=\"video-ready-message\" th:text=\"${contentStrings.projectstoryvideoready}\">Videonuz gösterime hazırdır.</p>\n                                </div>\n                                <!--Upload Video Progress-->\n                                <div id=\"uploadVideoProgressingWrapper\" class=\"bg-white border ptb10 plr20\" style=\"display: none\">\n                                    <p class=\"pl50 yellow-dark mb0\">Dosyanız yükleniyor, lütfen bekleyiniz. <span\n                                            id=\"videoProgressDone\" class=\"green fwbold\">%0 tamamlandı</span></p>\n\n                                    <div id=\"videoProgress\" class=\"progress yellow-dark center mb0\">\n                                        <div class=\"progress-bar progress-bar-success progress-bar-striped active\"></div>\n                                    </div>\n                                    <button id=\"cancelVideoUpload\" class=\"btn btn-danger fwbold ptb5 m-center mt10\"\n                                       style=\"display: table;\">Yüklemeyi Durdur\n                                    </button>\n                                </div>\n                                <!--Upload Video Completed-->\n                                <div class=\"bg-white border ptb10 plr20\" id=\"videoUploadCompletedWrapper\" style=\"\n                                height: 101px; display: none\">\n                                    <p class=\"text-center green mb10\">Tebrikler! Videonuz başarılı olarak yüklenmiştir.\n                                        Video işlenme süreci tamamlandıktan sonra, videonuz görüntülenecektir.</p>\n                                    <button id=\"videoUploadCompleted\" class=\"btn btn-success fwbold ptb5 m-center mt10\"\n                                            style=\"display: table;\">Tamam</button>\n                                </div>\n                            </div>\n                        </div>\n                    </li>\n                    <li class=\"project-rich-description\">\n                        <div class=\"grey-field mb20 clearfix\">\n                            <label for=\"richDescription\">\n                                <span>"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.fillprojectdetailspage2textboxdesctext : stack1), depth0))
    + "</span>\n                                <span class=\"grey-field__info-box\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.fillprojectdetailspage2textboxdesccoloredtext : stack1), depth0))
    + "</span>\n                            </label>\n                            <div class=\"field-wrapper\">\n                                <textarea id=\"richDescription\" rows=\"8\" name=\"richDescription\" class=\"form-control border\" data-parsley-required>"
    + alias2(((helper = (helper = helpers.richDescription || (depth0 != null ? depth0.richDescription : depth0)) != null ? helper : alias4),(typeof helper === alias5 ? helper.call(alias3,{"name":"richDescription","hash":{},"data":data}) : helper)))
    + "</textarea>\n                                <div class=\"field-help\">\n                                  "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.fillprojectdetailspage2textboxdescsubtext : stack1), depth0))
    + "\n                                </div>\n                            </div>\n                        </div>\n                    </li>\n                    <!--<li class=\"project-challenges\">\n                        <div class=\"grey-field mb20 clearfix\">\n                            <label for=\"challenges\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.fillprojectdetailspage2textboxrisksandchallangestext : stack1), depth0))
    + "</label>\n                            <div class=\"field-wrapper\">\n                                <div class=\"field-help\">\n                                  "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.fillprojectdetailspage2textboxrisksandchallangessubtext : stack1), depth0))
    + "\n                                </div>\n                                <textarea id=\"challenges\" rows=\"4\" name=\"challenges\" class=\"form-control border\" required>"
    + alias2(((helper = (helper = helpers.challenges || (depth0 != null ? depth0.challenges : depth0)) != null ? helper : alias4),(typeof helper === alias5 ? helper.call(alias3,{"name":"challenges","hash":{},"data":data}) : helper)))
    + "</textarea>\n                            </div>\n                        </div>\n                    </li>-->\n                </ol>\n            </div>\n            <br/>\n        </div>\n        <div class=\"col-xs-3\">\n            <div id=\"story-sidebar\" class=\"project-sidebar-form ml10\">\n                <ol class=\"sidebar-help-panels\">\n                    <li id=\"story-sidebar-help\" style=\"display: list-item;\">\n                        <a class=\"green fwbold\" href=\"#\" target=\"_blank\">\n                          "
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.fillprojectdetailspage2textboxstoriestitle : stack1), depth0))
    + "\n                        </a>\n                    </li>\n                    <li><p class=\"mt10 green-dark\">"
    + alias2(alias1(((stack1 = (depth0 != null ? depth0.cs : depth0)) != null ? stack1.fillprojectdetailspage2textboxstoriessubtitle : stack1), depth0))
    + "</p></li>\n                </ol>\n            </div>\n        </div>\n    </div>\n</div>\n";
},"useData":true});
return templates;
  }(Handlebars));
}());