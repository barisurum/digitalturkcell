/**
 * Created by ayberkakici on 02/02/16.
 */
$(function() {
  var config = {
      hash: window.location.hash,
      projectId : function(){
        var id = window.location.pathname.split('/')[2];
        return typeof id === 'undefined' ? 2 : id; //Id 2 for testing purpose
      },
      video: document.getElementsByTagName('video')[0],
      videoPitch: $('#videoPitch'),
      bigPlay: $('#bigPlay'),
      shareBtn: $('#shareBtn'),
      twitterShare: $('#twitterShare'),
      fbShare: $('#fbShare'),
      emailShare: $('#emailShare')
    },

    playVideo = function() {
      config.videoPitch.removeAttr('style').addClass('has_played').css('overflow', 'visible');
      config.bigPlay.hide();
      config.videoPitch[0].plyr.play();
    };


    config.videoPitch.one('click', playVideo);

    plyr.setup();

    fbq('track', 'Purchase', {value: '1.00', currency: 'USD'});
});