if($(window).width() < 700) {

    var simentyCSS = "<style type='text/css'>@media all and (max-width:700px){#ccInfo li#cardCvv,#ccInfo li#cardExpiration{width:100%}#ccInfo li#cardExpiration select{width:47%}.login-form--signup,div#newCardForm div#creditCardForm li.pull-left.relative{float:right!important;top:2px!important}.icon-location{display:none!important}header>div.container>nav[aria-labelledby=main-navigation]>ul.primary-menu>li.primary-menu-link.profile span{margin-top:12px}.img-responsive{width:auto!important;margin:0 auto!important}.plr40.ptb50.white.m0.walkthrough__bWrapper--text{padding-bottom:10px!important;padding-top:10px!important}.walkthrough__bWrapper--text{font-size:16px;display:table;line-height:20px}.plr50.ptb80.white.m0.walkthrough__bWrapper--text{padding-top:20px!important;padding-bottom:20px!important}.viewXS{display:block}.logged-out-link a,.primary-menu-link a{padding:3px 10px 0}li.primary-menu-link.primary-menu-link--about.js-about-us-link{display:inline-block!important}.logged-out-link,.primary-menu-link{display:inline-block;float:none;max-height:30px;width:32%}#cardCvv,#ccInfo .ml40,.pledge-coupon-button,img,table{margin-left:0!important}.pt60,.ptb60{padding-top:20px!important}.pull-right{width:100%;text-align:center}.NS_projects .carousel .item .container{left:0!important;bottom:65px!important;padding:0 10px!important}div.col-xs-6{margin-top:20px!important}.pt60,.ptb60{margin-top:60px!important}.project-card.project-card-tall-big{margin-bottom:25px;display:block;position:relative}.walkthrough__how-it-works{text-align:center;top:-10px}.row div.col-xs-4,div.col-xs-4.walkthrough__how-it-works--wrapper{border-right:none;margin-bottom:0!important;text-align:center}.pb80 .container .row .col-xs-4.walkthrough__how-it-works--wrapper{margin-bottom:50px!important}section.bg-grey-light.ptb70{margin-top:50px!important}.project-card.project-card-tall-big .icon-to-go{width:22px;height:27px}.NS_projects__stats .goal.text{display:inline-block}.NS_projects .carousel .item .carousel-title{text-align:center;font-size:1.6em;font-weight:bolder;line-height:28px}.NS_projects .carousel .item .carousel-text{font-size:16px;line-height:17px}.btn.btn--yellow.btn--large.green-dark--link.plr50.h4{margin-top:0!important}.NS_projects .carousel-btn{bottom:-50px}.pull-left{width:auto}.col-xs-3{width:100%;margin-top:10px}.stat-item .pull-left{width:100%;text-align:center}.grey-light--link.pull-right{width:100px}.icon-location.pull-right.mr5{width:20px}img,table{width:auto!important;margin-right:0!important;padding-left:0!important;padding-right:0!important}.img-responsive.mb10{width:250px!important}.NS_projects__hero-funding .video-player img.poster.landscape,.pledge-page.container .col-xs-5,.show-drop-down-menu{width:100%!important}.show-drop-down-menu{display:block!important;float:right!important;max-height:none!important;margin-bottom:18px!important}.show-drop-down-menu>a{height:auto!important}.nav-user-name{margin-top:30px}header nav:first-child{margin-bottom:10px}.col-xs-6.mega-footer__links-list{width:50%!important;min-width:initial!important;float:left!important;margin-top:0!important}div#videoPitch{text-align:center;display:inline-block}.project-image{text-align:center;float:left;width:100%!important}td img{width:240px!important}.pull-right.border-right{height:120px}.col-xs-6>img{width:132px!important;margin-bottom:20px!important}.show-drop-down-menu .drop-down-menu{top:195px;right:11px}.h4.mr40.pull-right{width:initial}.h4.pull-left{float:none!important}.likeItOrNot{width:100%!important;left:0!important}#likeItOrNot>div.col-xs-9{width:100%!important;max-width:100%!important;min-width:100%!important}.project-card--thinking .project-stats .h4.mr40.pull-right,.project-card--thinking .project-stats .h4.pull-left{float:left!important;width:50%!important;margin:0!important;padding:0!important;text-align:center}.NS_projects__stats .goal.like{display:inline-block;float:none}.project-card--thinking .goal.num{top:-7px;float:none}.creditCardForm li:nth-child(5),.creditCardForm li:nth-child(6){width:60px!important}#applyCoupon{padding:15px 15px 30px!important;margin-bottom:90px!important;width:100%!important}#couponTextField{width:80%!important;font-size:20px!important}.pledge-coupon-button{left:78%!important;padding:9px 8px!important;height:34px!important}.pledge-page .col-xs-3{padding:0}#project-rewards-tab{display:block!important}#startProjectHeader .row div.col-xs-4,div.col-xs-4.walkthrough__how-it-works--wrapper{margin-bottom:170px!important}#startProjectHeader.pt80{padding-top:20px!important}body div.full-width.tools{margin-bottom:-100px!important}#oduller .pt40{padding-top:0!important}#oduller .mb25.mt0.text-center.green-dark{display:none}#storedCardForm .reward{background:#f8f8f8}.nav-user-name{padding:6px}.NS_projects__project-nav li.project-nav-item.active a::after{background-position:-364px 0}.NS_projects__project-nav.full-width.bg-grey-light.mt30.border-bottom.box-shadow{box-shadow:none!important;border:none;background:0 0!important}.timeline-content .date::after,.timeline-content::after{background-color:transparent}#timeline .date{font-size:18px!important}#timeline h5{font-size:20px!important}.timeline-content p{font-size:16px}.NS_projects__congrats{padding:0}.col-xs-6.m-center a.NS_projects__congrats__share{width:50px;display:inline-block;margin:0 20px;float:none !importantportant;line-height:30px}.container .row .col-xs-6 .row.mt40 .col-xs-12 .btn--x-large,.row.mt40 div.col-xs-6{margin-bottom:20px!important}.row.mt40 div.col-xs-6{display:table}.project-card.project-card-tall-big{margin-top:20px!important}.NS_projects__congrats .project-thumbnail::after,.NS_projects__congrats .project-thumbnail::before{display:none!important;background:0 0!important}.NS_projects__hero-funding.bg-white .col-xs-9.tab-content{display:table}div#videoPitch{width:100%!important}.project-card.project-card-tall-big .project-card-footer,ul.navlist{width:100%}#billingInfo .mt90{margin-top:0!important}.project-card.project-card-tall-big .icon-to-go{left:inherit!important;top:7px!important;position:relative!important}h2.mtb0.text-center.green-dark.fwbold{margin-top:20px!important}.NS_projects__congrats .project-badge{right:-8px!important}.NS_projects__congrats .project-thumbnail{background-size:100% 100%;height:auto!important}.navlist li{width:30%}ul.navlist li a{width:100%}.contact-info .col-xs-12.mb20{margin-top:20px!important;margin-bottom:20px!important;margin-left:0!important}.contact-info .contact-content{width:100%;text-align:center}.contact-text-info.long{padding-left:36px}footer.mega-footer-bottom div.container>div.row>div.col-xs-6>img{display:table;float:none!important;height:inherit;margin:20px auto!important;width:inherit}div.player-video-wrapper video.valign{transform:translateY(0);-webkit-transform:translateY(0)}.NS_projects__hero-funding .video-player video{transform:translateY(0);-webkit-transform:translateY(0);opacity:0}.NS_projects__hero-funding .video-player.has_played video{opacity:1!important}.player.fullscreen-active video{top:0;position:absolute}.NS_projects__content section{min-height:0}.sales-agreement{position:absolute;bottom:-50px}.pull-right .user-menu.js-user-menu{height:85px;margin-top:5px}nav.pull-right{height:55px}.simenty-margin-top{margin-top:-15px!important}.NS_projects__header__menu a{padding:7px 4%;margin-bottom:10px}header#header>iframe{display:none}.NS_projects .carousel-control{font-size:2em!important}.logged-out-link a,.primary-menu-link a{line-height:normal;display:inline-block;vertical-align:middle;paddingTop:5px;max-height:100px;height:initial;width:105px}#signUpForm{width:100%!important}.mega-footer__link{padding:5px 5px 5px 0}.start-project input{width:97%!important}div.carousel-inner{max-width:1000px!important;position:relative;width:100vw!important;left:calc(-50vw + 50%)}#questions-form .fields,.form-control.border.remaining.form-val,.grey-field .field-wrapper{width:100%!important}.grey-field .label,.grey-field label{width:100%!important;font-size:17px!important}.field-remaining.pull-right.mr20.t10.fwbold{text-align:end}.grey-field.mb20.clearfix{margin-bottom:0!important}.form-control.border.remaining.form-val{font-size:16px!important}#allProjectContent{margin-top:-20px!important}.item .container .row .col-xs-12{margin-bottom:10px!important}#projects .content .col-xs-3{width:25%!important;margin-top:10px!important;padding-bottom:10px!important}.dashboard-item__item .row .col-xs-3{padding-left:0!important}#startProjectHeader #projects{margin-top:-90px}#projects .h1.green-dark.text-center{margin-bottom:30px!important}.start-project.mtb50.block{margin-top:0!important}.rev-play-button-wrapper{top:0!important}}</style>";

    jQuery("body").append(simentyCSS);


    function moveToContent(selector, location, into, widthValue) {


        if (selector.length > 0) {
            width = $(window).width();
            if (width < widthValue) {
                if (typeof jQuery(selector) !== "undefined" && jQuery(selector).length > 0) {
                    content = $(selector)[0].outerHTML;
                    $(selector).remove();

                    if (into === "top") $(location).prepend(content);
                    else if (into === "bottom") $(location).append(content);
                    else alert('Error');
                }
            }
        }

    }


    $(document).ready(function () {
        if (isVisible('avatar')) {

            $('.pull-right .user-menu.js-user-menu').addClass('simenty-margin-top');

            var spanUsernameSelector = "nav[role='navigation'] span.nav-user-name";
            var spanUsername         = $(spanUsernameSelector).text().split(" ")[0];
            console.log(spanUsername);
            $(spanUsernameSelector).text(spanUsername);

        }
        $('#project-rewards-tab').on('click', function () {
            moveToContent('.NS_projects__content .container .row .col-xs-3', '#oduller', "top", 720);
        });

        //moveToContent('.NS_projects__rewards-list .reward', '#storedCardForm', 'top', 720);
        moveToContent('#applyCoupon', '#billingInfo div', 'top', 720);


        function isVisible(classx) {
            var element;
            if(classx.indexOf('Simenty') > -1) {
                element = $(classx);
            } else {
                element = $('.' + classx);
            }
            if (element.length > 0 && element.css('visibility') !== 'hidden' && element.css('display') !== 'none') {
                return true;
            } else {
                return false;
            }
        }

        if($(window).width() < 700) {

            $("body").prepend($("div.drop-down-menu"));

            $("header div.container").append($("#header nav[aria-labelledby='main-navigation']").clone());

            $("header div.container nav[aria-labelledby='main-navigation'] ul li:last a").remove();
            $("header div.container nav[aria-labelledby='main-navigation'] ul li:last").removeClass('primary-menu-link--about');
            $("header div.container nav[aria-labelledby='main-navigation'] ul li:last").removeClass('js-about-us-link');
            $("header div.container nav[aria-labelledby='main-navigation'] ul li:last").addClass('profile');
            $("header div.container nav[aria-labelledby='main-navigation'] ul li:last").append($("#header nav[aria-labelledby='user-navigation'] ul li a").clone());

            $("header div.container div.hamburger-wrapper").click(function(){
                console.log('click');
                $("div.drop-down-menu").toggleClass('visible');
            });

            if($("header div.container nav[aria-labelledby='main-navigation'] ul li:last a").attr("href") === '/kayit'){
                $("header div.container nav[aria-labelledby='main-navigation'] ul li:last a:first").hide();
                $("header div.container h1.site-logo").css("width", "100%");
                $("header div.container nav[aria-labelledby='main-navigation'] ul li").each(function(i){
                    $(this).css({'width': '25%','float': 'left !important'});
                });
            }

            /*
            var setMobileImages = function() {
              var items = $('.NS_projects .carousel .item');
              for (var i = 0; i < items.length; i++) {
                var url = $(items[i]).context.childNodes[5].value;
                if (typeof url === 'undefined') {
                  url = $(items[i]).context.childNodes[3].value;
                }
                if(url !== '') {
                  $(items[i]).css('background-image', 'url('+url+')');
                }
              }
            }();*/

            $('.pull-right .user-menu.js-user-menu').addClass('simenty-margin-top');

            var visible = true;
            if(visible && $(window).width() < 700){
                if(!$('header').hasClass('visible')){
                    $('header').addClass('visible');
                    console.log('Header visible');
                    if(isVisible('header .avatar')) {
                        $('header div.container h1.site-logo').css('margin-left', '28px');
                        if($(window).width() > 392) {
                            $('.logged-out-link a, .primary-menu-link a').addClass('setLineHeight');
                        } else if($(window).width() > 355){
                            $('.primary-menu-link.js-discover-link a, .primary-menu-link.profile a').addClass('setLineHeight');
                        }

                    } else {
                        if($('.logged-out-link a, .primary-menu-link a').hasClass('setLineHeight')) {
                            $('.logged-out-link a, .primary-menu-link a').removeClass('setLineHeight');
                        }
                        if($('.primary-menu-link.js-discover-link a, .primary-menu-link.profile a').hasClass('setLineHeight')) {
                            $('.primary-menu-link.js-discover-link a, .primary-menu-link.profile a').removeClass('setLineHeight');
                        }
                        console.log('margined');
                    }
                }
            }else if(!visible){
                if($('header').hasClass('visible')){
                    $('header').removeClass('visible');
                    console.log('Header invisible');
                }
                if($("div.drop-down-menu").hasClass('visible')){
                    $("div.drop-down-menu").removeClass('visible');
                    console.log('menu invisible');
                }
                $('.logged-out-link a, .primary-menu-link a').removeClass('setLineHeight');
                $(".hamburger-line").removeClass('active');
            }

        }

        document.querySelector(".hamburger-wrapper" ).addEventListener( "click", function() {
            $('.hamburger-line').toggleClass('active');
        });
    });
}
