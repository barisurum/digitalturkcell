window.app.ProjectModule = (function ($, app) {
  'use strict';
  var baseUrl = '';
  var config = {
        hash: window.location.hash,
        body: $('body'),
        projectId : function(){
          var id = window.location.pathname.substring(6);
          return isNaN(id) ? 1 : id;
        },
        tabs: $('#project-edit-tabs'),
        projectTabs: {
          basics: $('a[aria-controls="basics"]'),
          story: $('a[aria-controls="story"]'),
          rewards: $('a[aria-controls="rewards"]'),
          about: $('a[aria-controls="about-you"]'),
          merchant: $('a[aria-controls="merchant-info"]')
        },
        uploadBtn: $('#fileupload'),
        completeBtn: $('#projectComplete'),
        discardBtn: $('#projectDiscard'),
        allProjectContent: $('#allProjectContent'),
        discardOrSave: $('#discardOrSave'),
        rewardsMain: $('#rewardsMain'),
        rewardsTemplate: $('#rewardTemplate'),
        addRewardBtn: $('#addReward'),
        savingLayer: $('#savingLayer'),
        projectComplete: $('.project-complete'),
        requestApproval:$('#request-approval'),
        videoUploadOnProgress: function() {
          return 'Şu anda video yükleme işlemi devam etmektedir.\
          Bu sayfadan ayrılırsanız yükleme işlemi durdurulacaktır. \
          Devam etmek istediğiniz emin misiniz?';
        },
        tabNavigation: $('.tab-navigation'),
        errorModal: $('#errorModal'),
        btnPreview: $('#btnPreview')
      },

      manageTabs = function (e) {
        var hash = $(e.target).attr('href');
        if (hash.substr(0, 1) === '#') {
          var position = $(window).scrollTop();
          location.replace('#' + hash.substr(1));
          $(window).scrollTop(position);
        }
      },

      animatedScroll = function(elem) {
        $('html, body').animate({
          scrollTop: elem.offset().top
        }, 500);
      },

      tabNavigation = function() {
        var $this = $(this);
        if($this[0].id === 'previous-tab') {
          config.tabs.find('.active').prev().find('a').trigger('click');
        }
        else if($this[0].id === 'next-tab') {
          config.tabs.find('.active').next().find('a').trigger('click');
        }
        else {
          return false;
        }

        animatedScroll(config.body); //Scroll top when tabs are shown

      },

      onChange = function() {
        config.allProjectContent.bind('input select textarea propertychange', function() {
          discardOrSave('open');
          config.projectComplete.attr({ //Prevent Complete button to be clicked when dirty
            disabled: 'disabled',
            title: 'Önce yaptığınız değişiklikleri kaydetmeniz gerekmektedir'
          });
        });
      },

      discardOrSave = function(openOrClose) {
        if(openOrClose==='open') {
          config.discardOrSave.addClass('visible');
        }
        if(openOrClose==='close') {
          config.discardOrSave.removeClass('visible');
        }
      },
      uploadImageMethods = {
        startProjectImage: function() {
          $('#fileupload').dropzone({
            url: baseUrl + '/misc/upload-image',
            paramName: 'image',
            maxFiles: 1,
            previewsContainer: '.project-thumbnail',
            maxThumbnailFilesize: 2,
            thumbnailWidth: 207,
            thumbnailHeight: 207,
            addRemoveLinks:true,
            dictRemoveFile: 'Kaldır',
            dictCancelUpload: 'Yüklemeyi durdur',
            dictCancelUploadConfirmation: 'Yükleme durdurulsun mu?',
            clickable: true,
            headers: {
              'Cache-Control': null,
              'X-Requested-With': null
            },
            acceptedFiles: 'image/*',
            init: function() {
              var defaultThumbnail = $('.project-thumbnail-img');
              this.on('addedfile', function(file) {
                defaultThumbnail.hide();
              })
                .on('removedfile', function(file) {
                  defaultThumbnail.show();
                  $('#projectImage').val(defaultThumbnail.attr('src'));
                  $('#uploadSuccess').hide();
                  $('#uploader').show();
                })
                .on('success', function(file, response) {
                  $('#uploadSuccess').html('<p class="green fwbold">Görseliniz başarılı ile yüklenmiştir.</p><p>Yeni bir görsel yüklemek için var olanı kaldırmanız gerekmektedir.</p>');
                  $('#projectImage').val(response.url);
                  $('#fileupload').val(response.url);
                  discardOrSave('open');
                })
              .on('maxfilesexceeded', function(file) {
                console.log('max');
              })
              .on('maxfilesreached', function() {
                $('#uploader').hide();
                $('#uploadSuccess').html('<p>Görseliniz yükleniyor.</p><p>Lütfen bekleyiniz</p>').show();
              });
            }
          });
        },
        startLikeIcon: function() {
            $('#likeIcon').dropzone({
                url: baseUrl + '/misc/upload-image',
                paramName: 'image',
                maxFiles: 1,
                previewsContainer: '.project-thumbnail',
                maxThumbnailFilesize: 2,
                thumbnailWidth: 207,
                thumbnailHeight: 207,
                addRemoveLinks:true,
                dictRemoveFile: 'Kaldır',
                dictCancelUpload: 'Yüklemeyi durdur',
                dictCancelUploadConfirmation: 'Yükleme durdurulsun mu?',
                clickable: true,
                headers: {
                    'Cache-Control': null,
                    'X-Requested-With': null
                  },
                acceptedFiles: 'image/*',
                init: function() {
                    this.on('addedfile', function(file) {
                      })
                          .on('removedfile', function(file) {
                            $('#likeIcon').val(defaultThumbnail.attr('src'));
                            $('#uploadSuccessLike').hide();
                            $('#uploader').show();
                          })
                          .on('success', function(file, response) {
                            $('#uploadSuccessLike').html('<p class="green fwbold">Görseliniz başarılı ile yüklenmiştir.</p><p>Yeni bir görsel yüklemek için var olanı kaldırmanız gerekmektedir.</p>');
                            $('#likeIcon').val(response.url);
                            discardOrSave('open');
                          })
                          .on('maxfilesexceeded', function(file) {
                            console.log('max');
                          })
                  }
              });
          },
        startdisLikeIcon: function() {
            $('#dislikeIcon').dropzone({
                url: baseUrl + '/misc/upload-image',
                paramName: 'image',
                maxFiles: 1,
                previewsContainer: '.project-thumbnail',
                maxThumbnailFilesize: 2,
                thumbnailWidth: 207,
                thumbnailHeight: 207,
                addRemoveLinks:true,
                dictRemoveFile: 'Kaldır',
                dictCancelUpload: 'Yüklemeyi durdur',
                dictCancelUploadConfirmation: 'Yükleme durdurulsun mu?',
                clickable: true,
                headers: {
                    'Cache-Control': null,
                    'X-Requested-With': null
                  },
                acceptedFiles: 'image/*',
                init: function() {
                    this.on('addedfile', function(file) {
                      })
                          .on('removedfile', function(file) {
                            $('#dislikeIcon').val(defaultThumbnail.attr('src'));
                            $('#uploadSuccessdisLike').hide();
                          })
                          .on('success', function(file, response) {
                            $('#uploadSuccessdisLike').html('<p class="green fwbold">Görseliniz başarılı ile yüklenmiştir.</p><p>Yeni bir görsel yüklemek için var olanı kaldırmanız gerekmektedir.</p>');
                            $('#dislikeIcon').val(response.url);
                            discardOrSave('open');
                          })
                          .on('maxfilesexceeded', function(file) {
                            console.log('max');
                          })
                  }
              });
          },
        startProfileImage: function() {
          $('#photoUpload').dropzone({
            url: baseUrl + '/misc/upload-image',
            paramName: 'image',
            maxFiles: 1,
            previewsContainer: '.image-clip',
            maxThumbnailFilesize: 2,
            thumbnailWidth: 207,
            thumbnailHeight: 207,
            addRemoveLinks:true,
            dictRemoveFile: 'Kaldır',
            dictCancelUpload: 'Yüklemeyi durdur',
            dictCancelUploadConfirmation: 'Yükleme durdurulsun mu?',
            clickable: true,
            headers: {
              'Cache-Control': null,
              'X-Requested-With': null
            },
            acceptedFiles: 'image/*',
            init: function() {
              var defaultThumbnail = $('.image-clip-img');
              this.on('addedfile', function(file) {
                defaultThumbnail.hide();
              })
                .on('removedfile', function(file) {
                  defaultThumbnail.show();
                  $('#userPhoto').val(defaultThumbnail.attr('src'));
                  $('#profileUploadSuccess').hide();
                  $('#profileUploader').show();
                })
                .on('success', function(file, response) {
                  $('#profileUploadSuccess').html('<p class="green fwbold">Görseliniz başarılı ile yüklenmiştir.</p><p>Yeni bir görsel yüklemek için var olanı kaldırmanız gerekmektedir.</p>');
                  $('#userPhoto').val(response.url);
                  $('#photoUpload').val(response.url);
                  discardOrSave('open');
                })
                .on('maxfilesexceeded', function(file) {
                  console.log('max');
                })
                .on('maxfilesreached', function() {
                  $('#profileUploader').hide();
                  $('#profileUploadSuccess').html('<p>Görseliniz yükleniyor.</p><p>Lütfen bekleyiniz</p>').show();
                });
            }
          });
        },
        startCongratsImage: function() {
          $('#congratsImage').dropzone({
            url: baseUrl + '/misc/upload-image',
            paramName: 'image',
            maxFiles: 1,
            previewsContainer: '#rev-congrats-preview',
            maxThumbnailFilesize: 2,
            thumbnailWidth: 207,
            thumbnailHeight: 207,
            addRemoveLinks:true,
            dictRemoveFile: 'Kaldır',
            dictCancelUpload: '',
            dictCancelUploadConfirmation: 'Yükleme durdurulsun mu?',
            clickable: true,
            headers: {
              'Cache-Control': null,
              'X-Requested-With': null
            },
            acceptedFiles: 'image/*',
            init: function() {
              var defaultThumbnail = $('#rev-congrats-thumbnail-img');
              this.on('addedfile', function(file) {
                defaultThumbnail.hide();
              })
                .on('removedfile', function(file) {
                  defaultThumbnail.show();
                  $('#uploadSuccessCongrats').hide();
                  $('#uploader-congrats').show();
                })
                .on('success', function(file, response) {
                  $('#uploadSuccessCongrats').html('<p class="green fwbold">Görseliniz başarı ile yüklenmiştir.</p><p>Yeni bir görsel yüklemek için var olanı kaldırmanız gerekmektedir.</p>');
                  $('#congratsImage').val(response.url);
                  discardOrSave('open');
                })
                .on('maxfilesexceeded', function(file) {
                  console.log('max');
                })
                .on('maxfilesreached', function() {
                  $('#uploader-congrats').hide();
                  $('#uploadSuccessCongrats').html('<p>Görseliniz yükleniyor.</p><p>Lütfen bekleyiniz</p>').show();
                });
            }
          });
        },
        startCongratsVideo: function() {
          $('#congratsVideo').dropzone({
            url: baseUrl + '/misc/upload-video',
            paramName: 'video',
            maxFiles: 1,
            clickable: true,
            headers: {
              'Cache-Control': null,
              'X-Requested-With': null
            },
            acceptedFiles: 'video/*',
            init: function() {
              var uploadVideoWrapper = $('#uploadVideoWrapperCongrats'),
                videoProcessing = $('#videoProcessingCongrats'),
                videoIsReady = $('#videoIsReadyCongrats'),
                uploadVideoProgressingWrapper = $('#uploadVideoProgressingWrapperCongrats'),
                videoProgressDone = $('#videoProgressDoneCongrats'),
                videoProgress = $('#videoProgressCongrats'),
                cancelVideoUpload = $('#cancelVideoUploadCongrats'),
                videoUploadCompletedWrapper = $('#videoUploadCompletedWrapperCongrats'),
                videoUploadCompleted = $('#videoUploadCompletedCongrats'),
                timer,
                checkTranscoding = function() {
                  getData('/projects/'+config.projectId()).done(function(data){
                    var video = data.video;
                    if(video === 'transcoding') {
                      console.log('transcoding');
                      videoIsReady.hide();
                      videoProcessing.show();
                    }
                    else if(video !== 'transcoding' && video !== '' && typeof video !== 'undefined') {
                      console.log('ready');
                      videoProcessing.hide();
                      videoIsReady.show();
                      window.clearInterval(timer);
                    }
                    else {
                      return false;
                    }
                  });
                };

              var self = this;
              cancelVideoUpload.click(function() {  //Cancel Video Upload Button
                var r = confirm('Video yükleme işlemini iptal etmek istediğinzden emin misiniz?');
                if (r == true) {
                  self.removeAllFiles(true);
                } else {
                  return false;
                }
              });

              videoUploadCompleted.click(function() { //Video Upload Successfully Completed Button
                videoUploadCompletedWrapper.hide();
                videoProgressDone.html('%0 tamamlandı');
                videoProgress.find('.progress-bar').css('width', '0%');
                uploadVideoWrapper.show();
                timer = window.setInterval(checkTranscoding, 10000);

              });

              this.on('addedfile', function(file) {
                uploadVideoWrapper.hide();
                videoProgressDone.html('%0 tamamlandı');
                videoProgress.find('.progress-bar').css('width', '0%');
                uploadVideoProgressingWrapper.show();
              })
                .on('sending', function(file, xhr, formData) { //Append projectId parameter while posting
                  formData.append('congratsVideo', true);
                  formData.append('projectId', parseInt(config.projectId()));
                })
                .on('processing', function(processingfile) {
                  window.onbeforeunload = config.videoUploadOnProgress;
                  videoProcessing.hide();
                  videoIsReady.hide();
                })
                .on('uploadprogress', function(file, progress) {
                  videoProgressDone.html('%' + progress.toFixed(0) + ' tamamlandı');
                  videoProgress.find('.progress-bar').css('width', progress+'%');
                })
                .on('canceled', function() {
                  window.onbeforeunload = null;
                  uploadVideoProgressingWrapper.hide();
                  uploadVideoWrapper.show();
                })
                .on('success', function(file, response) {
                  window.onbeforeunload = null;
                  uploadVideoProgressingWrapper.hide();
                  videoUploadCompletedWrapper.show();
                  discardOrSave('open');
                });
            }
          });
        },

        startBipVideo: function() {
          $('#bipVideo').dropzone({
            url: baseUrl + '/misc/upload-video',
            paramName: 'video',
            maxFiles: 1,
            clickable: true,
            headers: {
              'Cache-Control': null,
              'X-Requested-With': null
            },
            acceptedFiles: 'video/*',
            init: function() {
              var uploadVideoWrapper = $('#uploadVideoWrapperBip'),
                videoProcessing = $('#videoProcessingBip'),
                videoIsReady = $('#videoIsReadyBip'),
                uploadVideoProgressingWrapper = $('#uploadVideoProgressingWrapperBip'),
                videoProgressDone = $('#videoProgressDoneBip'),
                videoProgress = $('#videoProgressBip'),
                cancelVideoUpload = $('#cancelVideoUploadBip'),
                videoUploadCompletedWrapper = $('#videoUploadCompletedWrapperBip'),
                videoUploadCompleted = $('#videoUploadCompletedBip'),
                timer,
                checkTranscoding = function() {
                  getData('/projects/'+config.projectId()).done(function(data){
                    var video = data.video;
                    if(video === 'transcoding') {
                      console.log('transcoding');
                      videoIsReady.hide();
                      videoProcessing.show();
                    }
                    else if(video !== 'transcoding' && video !== '' && typeof video !== 'undefined') {
                      console.log('ready');
                      videoProcessing.hide();
                      videoIsReady.show();
                      window.clearInterval(timer);
                    }
                    else {
                      return false;
                    }
                  });
                };

              var self = this;
              cancelVideoUpload.click(function() {  //Cancel Video Upload Button
                var r = confirm('Video yükleme işlemini iptal etmek istediğinzden emin misiniz?');
                if (r == true) {
                  self.removeAllFiles(true);
                } else {
                  return false;
                }
              });

              videoUploadCompleted.click(function() { //Video Upload Successfully Completed Button
                videoUploadCompletedWrapper.hide();
                videoProgressDone.html('%0 tamamlandı');
                videoProgress.find('.progress-bar').css('width', '0%');
                uploadVideoWrapper.show();
                timer = window.setInterval(checkTranscoding, 10000);

              });

              this.on('addedfile', function(file) {
                uploadVideoWrapper.hide();
                videoProgressDone.html('%0 tamamlandı');
                videoProgress.find('.progress-bar').css('width', '0%');
                uploadVideoProgressingWrapper.show();
              })
                .on('sending', function(file, xhr, formData) { //Append projectId parameter while posting
                  formData.append('bipVideo', true);
                  formData.append('projectId', parseInt(config.projectId()));
                })
                .on('processing', function(processingfile) {
                  window.onbeforeunload = config.videoUploadOnProgress;
                  videoProcessing.hide();
                  videoIsReady.hide();
                })
                .on('uploadprogress', function(file, progress) {
                  videoProgressDone.html('%' + progress.toFixed(0) + ' tamamlandı');
                  videoProgress.find('.progress-bar').css('width', progress+'%');
                })
                .on('canceled', function() {
                  window.onbeforeunload = null;
                  uploadVideoProgressingWrapper.hide();
                  uploadVideoWrapper.show();
                })
                .on('success', function(file, response) {
                  window.onbeforeunload = null;
                  uploadVideoProgressingWrapper.hide();
                  videoUploadCompletedWrapper.show();
                  discardOrSave('open');
                });
            }
          });
        },
      },

      startProfileVideo = function() {
        $('#projectVideo').dropzone({
          url: baseUrl + '/misc/upload-video',
          paramName: 'video',
          maxFiles: 1,
          clickable: true,
          headers: {
            'Cache-Control': null,
            'X-Requested-With': null
          },
          acceptedFiles: 'video/*',
          init: function() {
            var uploadVideoWrapper = $('#uploadVideoWrapper'),
              videoProcessing = $('#videoProcessing'),
              videoIsReady = $('#videoIsReady'),
              uploadVideoProgressingWrapper = $('#uploadVideoProgressingWrapper'),
              videoProgressDone = $('#videoProgressDone'),
              videoProgress = $('#videoProgress'),
              cancelVideoUpload = $('#cancelVideoUpload'),
              videoUploadCompletedWrapper = $('#videoUploadCompletedWrapper'),
              videoUploadCompleted = $('#videoUploadCompleted'),
              timer,
              checkTranscoding = function() {
                getData('/projects/'+config.projectId()).done(function(data){
                  var video = data.video;
                  if(video === 'transcoding') {
                    console.log('transcoding');
                    videoIsReady.hide();
                    videoProcessing.show();
                  }
                  else if(video !== 'transcoding' && video !== '' && typeof video !== 'undefined') {
                    console.log('ready');
                    videoProcessing.hide();
                    videoIsReady.show();
                    window.clearInterval(timer);
                  }
                  else {
                    return false;
                  }
                });
              };

            var self = this;
            cancelVideoUpload.click(function() {  //Cancel Video Upload Button
              var r = confirm('Video yükleme işlemini iptal etmek istediğinzden emin misiniz?');
              if (r == true) {
                self.removeAllFiles(true);
              } else {
                return false;
              }
            });

            videoUploadCompleted.click(function() { //Video Upload Successfully Completed Button
              videoUploadCompletedWrapper.hide();
              videoProgressDone.html('%0 tamamlandı');
              videoProgress.find('.progress-bar').css('width', '0%');
              uploadVideoWrapper.show();
              timer = window.setInterval(checkTranscoding, 10000);

            });

            this.on('addedfile', function(file) {
              uploadVideoWrapper.hide();
              videoProgressDone.html('%0 tamamlandı');
              videoProgress.find('.progress-bar').css('width', '0%');
              uploadVideoProgressingWrapper.show();
            })
              .on('sending', function(file, xhr, formData) { //Append projectId parameter while posting
                formData.append('projectId', parseInt(config.projectId()));
              })
              .on('processing', function(processingfile) {
                window.onbeforeunload = config.videoUploadOnProgress;
                videoProcessing.hide();
                videoIsReady.hide();
              })
              .on('uploadprogress', function(file, progress) {
                videoProgressDone.html('%' + progress.toFixed(0) + ' tamamlandı');
                videoProgress.find('.progress-bar').css('width', progress+'%');
              })
              .on('canceled', function() {
                window.onbeforeunload = null;
                uploadVideoProgressingWrapper.hide();
                uploadVideoWrapper.show();
              })
              .on('success', function(file, response) {
                window.onbeforeunload = null;
                uploadVideoProgressingWrapper.hide();
                videoUploadCompletedWrapper.show();
                discardOrSave('open');
              });

          }
        });
      },

      remainingChars = function(selector) {
        var elem = $(selector);
        elem.each(function() {
              var $this = $(this),
                max = $this.attr('maxlength'),
                length = $this.val().length,
                remaining = max-length;
              $this.parents('li').find('.field-remaining').text('Kalan '+remaining+'/'+max);
              $this.bind('input propertychange', function() {
                  var newRemaining = max-$this.val().length;
                  $this.parents('li').find('.field-remaining').text('Kalan '+newRemaining+'/'+max);
                });
            });

      },

      formatNumeric = function(selector) {
        var im = new Inputmask({ 'mask': '9', 'repeat': '*', 'greedy': false, 'rightAlign': false });
        im.mask(selector);
      },

      formatCurrency = function(selector) {
        var im = new Inputmask('decimal', {
          radixPoint: ',',
          rightAlign: false,
          autoGroup: true,
          groupSeparator: '.',
          groupSize: 3,
          autoUnmask: true,
          allowPlus: false,
          allowMinus: false
        });
        im.mask(selector);
      },

      buttonAnimation = null,

      animateButton = function(button, text, padding, addedClass){
        var i = 0,
          btn = button.attr('disabled', 'disabled').removeClass('green-dark--link').addClass(addedClass).html(text).css({
            'text-align': 'left',
            'padding-left': padding+'px'
          });
        buttonAnimation = setInterval(function () {
          i = ++i % 4;
          btn.html(text + new Array(i + 1).join('.'));
        }, 500);

        return button;
      },

      sendData = function (url, data) {
        return $.ajax({
          url: baseUrl + url,
          type: 'POST',
          contentType: 'application/json',
          dataType: 'json',
          data: JSON.stringify(data)
        });
      },

      getData = function (url) {
        return $.ajax({
          url: baseUrl + url,
          type: 'GET',
          contentType: 'application/json',
          dataType: 'json'
        });
      },

      getProject = function() {
        config.savingLayer.show();
        return [
          getData('/projects/'+config.projectId()+'/thin').done(function(data){
            //LANG
            data.cs = contentStrings;
            render(data, 'basics', 'basics');
            render(data, 'story', 'story');
            projectStatus(data.status);
            config.btnPreview.attr('href', '/projects/'+config.projectId()+'/detail'); //Preview Button (Ön İzleme)
          })
        ];
      },

      updateProject = function(project, rewards) {
        config.savingLayer.show();
        return [
          sendData('/projects/update', project).done(function(data){
            data.cs = contentStrings;
            if(!('updateErrorMessage' in data)) { //Render if there is no error after
              render(data, 'basics', 'basics');
              render(data, 'story', 'story');
              projectStatus(data.status);
            }
            else {
              /*discardOrSave('open');*/
              config.projectComplete.attr({ //Prevent Complete button to be clicked when dirty
                disabled: 'disabled',
                title: 'Önce yaptığınız değişiklikleri kaydetmeniz gerekmektedir'
              });
              config.errorModal.find('p').html(data.updateErrorMessage);
              config.errorModal.modal('show');
            }
          })
        ];
      },

      projectStatus = function(status) {

        switch(parseInt(status)) {
        case 0:
          config.projectComplete.html(contentStrings.projectstatussendapprove + '<div class="subtext">En kısa zamanda</div>')
            .removeAttr('disabled');
          config.requestApproval.attr('action', function() {
              return '/projects/'+config.projectId()+'/request-approval';
            });
          break;
        case 1:
          config.projectComplete.html('Onay Bekliyor')
              .attr('disabled', 'disabled');
          break;
        case 2:
          config.projectComplete.html('Çılgın Oylama Süreci')
              .attr('disabled', 'disabled');
          break;
        case 3:
          config.projectComplete.html(contentStrings.profilebackedprojectslivedesc)
              .attr('disabled', 'disabled');
          break;
        case 4:
        case 5:
          config.projectComplete.html('Proje Kapandı')
              .attr('disabled', 'disabled');
          break;
        case 6:
          config.projectComplete.html(contentStrings.projectstatusdiscard)
              .attr('disabled', 'disabled');
          break;
        default:
          config.projectComplete.html('Bilinmiyor')
              .attr('disabled', 'disabled');
        }

        if(parseInt(status) >= 3) {
          /*disableEditRewards();
          disableDeleteRewards();*/

        }
        else {
          /*enableEditRewards();
          enableDeleteRewards();*/
        }
      },

      // autoComplete = function() {
      //   var cityName = $('.cityName');
      //
      //   cityName.each(function() {
      //     var thisCity = $(this),
      //       cityId = $(this).parent().find('.cityId');
      //
      //     thisCity.autocomplete({
      //       minLength: 1,
      //       source: function( request, response ) {
      //         $.ajax({
      //           url: baseUrl + '/misc/autocomplete-city',
      //           data: {
      //             query: request.term
      //           },
      //           success: function( data ) {
      //             response( data );
      //           }
      //         });
      //       },
      //       select: function( event, ui ) {
      //         thisCity.val( ui.item.name );
      //         cityId.val( ui.item.id );
      //         return false;
      //       },
      //       open: function(){
      //         $('.ui-autocomplete').css('width', '449px');
      //       }
      //     })
      //       .bind('input propertychange', function() {
      //         cityId.val('');
      //       })
      //       .autocomplete( 'instance' )._renderItem = function( ul, item ) {
      //       return $( '<li>' )
      //         .append( '<a>' + item.name )
      //         .appendTo( ul );
      //     };
      //   });
      //
      // },

      validateProject = function () {
        /*Validations Start*/
        var parsleyOptions = {
          errorClass: 'error',
          successClass: 'valid',
          classHandler: function(el) {
              return el.$element.parents('.grey-field');
            }
        };
        var validBasics = $('#basics-form').parsley(parsleyOptions),
          validStory = $('#form-story').parsley(parsleyOptions),
          validRewards = $('#rewardsMain').parsley(parsleyOptions);

        var addRemoveErrorClass = function(elem, addOrRemove) {
          if(addOrRemove==='add') {
            elem.parent().addClass('error');
          }
          else if(addOrRemove==='remove') {
            elem.parent().removeClass('error');
          }
          else {
            return false;
          }
        };

        //Color Steps Tabs if any errors
        validBasics.validate() ? addRemoveErrorClass(config.projectTabs.basics, 'remove') : addRemoveErrorClass(config.projectTabs.basics, 'add');
        //validStory.validate() ? addRemoveErrorClass(config.projectTabs.story, 'remove') : addRemoveErrorClass(config.projectTabs.story, 'add');

        return validBasics.validate();

      },

      // Done Callback is invoked when all requests return 200 OK
      doneCallback = function() {
        uploadImageMethods.startProjectImage();
        uploadImageMethods.startProfileImage();
        uploadImageMethods.startCongratsImage();
        uploadImageMethods.startCongratsVideo();
        uploadImageMethods.startBipVideo();
        uploadImageMethods.startLikeIcon();
        uploadImageMethods.startdisLikeIcon();
        // uploadImageMethods.startSuccessfulProjectImage();
        startProfileVideo();

        //Remove validation error indicators
        $('.steps').find('li').each(function(){$(this).removeClass('error')});

        // autoComplete();

        config.requestApproval.on('submit', function(form) {
          form.preventDefault();
          //If form is validated post form
          if( validateProject() ) {
            animateButton(config.projectComplete, 'Gönderliliyor', '0', 'left pl30');
            this.submit();
          }
          else {
            animatedScroll(config.body); //Scroll Top If Validation Fails
          }
          /*Validations Ends*/

        });

        config.savingLayer.fadeOut(600);
        discardOrSave('close');
        //Focus Input on click em.rec
        $('em.rec').click(function() {
          $(this).prev('input').focus();
        });

        config.projectComplete.removeAttr('title'); //Remove complete button's title

        //Allow only numeric inputs
        formatNumeric($('#phone, #pstn, #tckNo, #taxNo'));

        //Remaining Char limit
        remainingChars('.remaining');

      },

      // Done Callback End

      failCallback = function() {
        config.savingLayer.fadeOut(600);
        discardOrSave('close');
        console.log('fail');
      },

      render = function(data, template, target) {
        $('#'+target).html($.parseHTML(app.Templates[template](data)));
      },

      rewardsHelpers = function() {
        $('.reward').filter(function () {
          return $(this).css('display') !== 'none';
        }).each(function () {
          var thisReward = $(this);
          //Focus inputs
          thisReward.find('label').click(function () {
            $(this).next().focus();
          });

          //Rewards Help Content Show/Hide Slide Animation
          thisReward.find('.primary.help').unbind('click')
            .click(function () {
              $(this).next('.field-wrapper').find('.field-help').slideToggle();
            });

          //Rewards Set Count Input Show/Hide + Reset Value
          thisReward.find('.limit-checkbox').change(function () {
            var limit = $(this).closest('.limit');
            if ($(this).is(':checked')) {
              limit.addClass('limit-set').find('.count').val('').focus();
            }
            else {
              limit.removeClass('limit-set').find('.count').val('');
            }
          });

          //Delete Reward
          thisReward.find('.deleteReward').click(function (e) {
            e.preventDefault();
            thisReward.remove();
            var visibleRewards = $('.reward');
            if (visibleRewards.length > 0) {
              //Update reward #numbers
              visibleRewards.each(function (index) {
                $(this).find('.reward-num').text(index + 1);
              });
            }
            else {
              //Scroll to last body if all rewards are erased
              animatedScroll(config.body);
            }
            discardOrSave('open'); //Deleting reward triggers discardOrSave()
          });

          formatCurrency($('.currency'));
          formatNumeric($('.count'));

        });
      },

      addReward = function() {
        var rewardsMain = config.rewardsMain,
          rewardHtml = $.parseHTML(app.Templates['reward-single']({cs: contentStrings})),
          rewards = rewardsMain.find('.reward'),
          rewardsCount = rewards.length;
        rewardsMain.find('ol').append(rewardHtml);
        var addedReward = rewardsMain.find('.reward:last-child');
        addedReward.find('.reward-num').text(rewardsCount + 1);
        animatedScroll(addedReward);
        discardOrSave('open'); //Adding reward triggers discardOrSave()
      },

      saveProject = function() {
        /*Model Objects Start*/

        //Get rewards
        var visibleRewards = $('.reward'),
        //UI Elements for Project
          $status = parseInt($('#status').val()),
          $image = $('#projectImage').val(),
          $succcessImage = $('#successProjectImage').val(),
          $title = $('#projectTitle').val(),
          $question1 = $('#question1').val(),
            $question2 = $('#question2').val(),
            $question3 = $('#question3').val(),
            $question4 = $('#question4').val(),
            $question5 = $('#question5').val(),
          $projectSector = $('#projectSector').val(),
          $projectCategory = $('#projectCategory').val(),
          $video = $('#video').val(),
          $shortDescription = $('#shortDescription').val(),
          $richDescription = config.richDescription,//HTML content of CKEditor
          $searchKeywords = $('#searchKeywords').val(),
          $duration = 60,
          $goal = 10000,
          $challenges = $('#challenges').val(),
          $gaTrackingCode = $('#gaTrackingCode').val(),
          $congratsText = $('#congratsText').val(),
          $congratsImage = $('#congratsImage').val(),
          $agreementAccepted = $('#agreed-checkbox').prop('checked'),
          $likeName = $('#likeName').val(),
          $likePoint = $('#likePoint').val(),
          $likeIcon = $('#likeIcon').val(),
          $dislikeName = $('#dislikeName').val(),
          $dislikePoint = $('#dislikePoint').val(),
          $dislikeIcon = $('#dislikeIcon').val(),
          $commentHiding =  ($('#commentHiding').prop('checked') ? true : false),

        //Set project object
          project = new app.models.ProjectModel(config.projectId(), $projectSector, $projectCategory, $question1, $question2, $question3, $question4, $question5, $image, $title, $video, $shortDescription, $richDescription, $searchKeywords, $duration, $goal, $challenges, $gaTrackingCode, $congratsText, $congratsImage, $likeName, $likeIcon, $likePoint, $dislikeName, $dislikeIcon, $dislikePoint, $commentHiding),

          rewards = [];

        /*Model Objects End*/

        /*Send Update Request*/
        if ($status>0) {
          //If form is validated save
          if( validateProject() ) {
            $.when.apply($, updateProject(project)).done(function() {
              doneCallback();
            })
              .fail(function() {
                failCallback();
              });
          }
          else {
            animatedScroll(config.body); //Scroll Top If Validation Fails
          }
          /*Validations Ends*/
        }

        else {
          $.when.apply($, updateProject(project)).done(function() {
            doneCallback();
          })
            .fail(function() {
              failCallback();
            });
        }
      },

      //INIT START
      init = function () {
        if (config.hash !== '') {
          $('a[href="' + config.hash + '"]').tab('show');
        }

        $.ajaxSetup({
          beforeSend: function(jqXHR) {
            jqXHR.setRequestHeader('cf-request-type', 'ajax');
          },
          statusCode: {
            403: function() {
              location.href = '/login'
            }
          }
        });

        //GET PROJECT DATA REQUEST
        $.when.apply($, getProject()).done(function() {
          doneCallback();
        })
        .fail(function() {
          failCallback();
        });

        //Tab Management
        config.tabs.on('shown.bs.tab', manageTabs);
        config.tabNavigation.find('button').on('click', tabNavigation);

        addReward(); //Add an empty reward
        rewardsHelpers();
        onChange();

        // SAVE PROJECT ON SAVE (KAYDET BUTTON)
        config.completeBtn.click(function(e) {
          e.preventDefault();
          saveProject();
        });

        //RELOAD PROJECT ON DISCARD (VAZGEÇ BUTTON)
        config.discardBtn.click(function(e) {
          e.preventDefault();
          $.when.apply($, getProject()).done(function () {
            doneCallback();
          })
            .fail(function () {
              failCallback();
            });
        });

        //ADD REWARD ON ADD
        config.addRewardBtn.click(function (e) {
          e.preventDefault();
          addReward();
          rewardsHelpers();
        });

      };
    //INIT ENDS

  return {
      start: init
    }
}(jQuery, window.app));


