(function() {
  'use strict';
  /*window.app = window.app || {};*/
  window.app.ProjectDetailModule = (function ($) {
    console.log(projectId);
    var baseUrl = '';
    var config = {
      body: $('body'),
      hash: window.location.hash,
      tabs: $('#project-detail-tabs'),
      stats: $('#stats'),
      detailRewards: $('#detail-rewards'),
      savingLayer: $('#savingLayer'),
      savingLayerBotom: $('#savingLayerBotom'),
      projectStartDate: $('#projectStartDate'),
      timeline: $('#timeline').find('.timeline-content'),
      commentsNewFeatures: $('#newFeatures').find('.comments-content'),
      commentsDefects: $('#defects').find('.comments-content'),
      backProjectBtn: $('#backProjectBtn'),
      writeComment: $('.writeComment'),
      writeCommentNewFeatures: $('.writeCommentNewFeatures'),
      writeCommentDefects: $('.writeCommentDefects'),
      infoModal: $('#infoModal'),
      commentModal: $('#commentModal'),
      message: $('#message'),
      sendMessage: $('#sendMessage'),
      contact: $('#contact'),
      contactModal: $('#contactModal'),
      contactMessage: $('#contactMessage'),
      sendContactMessage: $('#sendContactMessage'),
      counterpartId: $('#counterpartId'),
      addToFavourites: $('#addToFavourites'),
      favouriteModal: $('#favouriteModal'),
      videoPitch: $('#videoPitch'),
      bigPlay: $('#bigPlay'),
      shareBtn: $('#shareBtn'),
      twitterShare: $('#twitterShare'),
      fbShare: $('#fbShare'),
      emailShare: $('#emailShare'),
      likeItOrNotComment: $('#likeItOrNotComment'),
      rating: $('#rating'),
      ratingSuccess: $('#ratingSuccess'),
      likeItOrNot: $('#likeItOrNot'),
      likeItOrNotButton: $('#likeItOrNotButton')
    },

    manageTabs = function (e) {
      var hash = $(e.target).attr('href');
      if (hash.substr(0, 1) === '#') {
        var position = $(window).scrollTop();
        location.replace('#' + hash.substr(1));
        $(window).scrollTop(position);
      }
    },

    rewardHelpers = function(e) {
      var $el = $(e.target);
      var $elem = $el.closest('.hover-group');
      console.log($elem);
      config.body.data('click', false);
      if($elem.hasClass('reward--available')) {
        var selectedForm = $elem.find('form');
        console.log(selectedForm);
        selectedForm.submit();
      }
      e.stopPropagation();
    },

    rewardFocusOut = function(e) {
      var rewardSelected = $('.reward--selected');
      if(config.body.data('click')) {
        rewardSelected.removeClass('reward--selected').addClass('reward--available');
        rewardSelected.find('form').removeClass('reward__checkout--is-visible').attr({'aria-expanded': false, 'aria-hidden': true});
        config.body.data('click', false);
      }
      else {
        config.body.data('click', true);
      }
      /*e.stopPropagation();*/
    },

    sendData = function (url, data) {
      return $.ajax({
        url: baseUrl + url,
        type: 'POST',
        data: data
      });
    },

    getData = function (url) {
      return $.ajax({
        url: baseUrl + url,
        type: 'GET',
        contentType: 'application/json',
        dataType: 'json'
      });
    },

    getProject = function() {
      //REV Show page
      config.savingLayer.fadeOut(600);
      return [
        getData('/projects/'+projectId).done(function(data){
          //DBG
            console.log(data);
            errorMessageHead = data.dislikeName + ' veya ' + data.likeName;
            likeHead = data.likeName;
            dislikeHead = data.dislikeName;
          data.cs = contentStrings;
          data.au = activeUser;

          if (typeof(activeUser) !== 'undefined') {
            if (isMob) {
                render(data, 'project-stats-mob', config.stats );
            } else {
                render(data, 'project-stats', config.stats );
            }
          }
          render(data, 'project-detail-tabs', config.tabs);
          config.projectStartDate.html(formatDatewithHours(data.created));
        }),

        getData('/projects/' + projectId + '/activities').done(function(data) {
          data.cs = contentStrings;
          render(data, 'project-detail-timeline', config.timeline);
          return data.length > 0 ? config.timeline.removeClass('empty') : config.timeline.addClass('empty');
        }),

        getData('/projects/' + projectId + '/new-features').done(function(data) {
          if (data.length > 0) {
            var l = data.length;
            for (var i = 0; i < data.length; ++i) {
              data[i].cs = contentStrings;
              data[i].activeUser = activeUser;
            }
            window.setTimeout(function() {
              $('#newFeaturesCount').find('data').html(l);
            }, 3000);
            console.log(data);
            render(data, 'project-detail-comments', config.commentsNewFeatures);
            config.writeCommentNewFeatures.click(writeCommentNewFeatures);

            $(window).animate({
              scrollTop: $("#project-detail-tabs").offset().top
            }, 500);
          } else {
            $('#newFeatures').html('<p>Henüz bir ' + likeHead + ' girişi yapılmamıştır.</p>');
          }
        }),

        getData('/projects/' + projectId + '/defects').done(function(data) {
          if (data.length > 0) {
            var l = data.length;
            for (var i = 0; i < data.length; ++i) {
              data[i].cs = contentStrings;
              data[i].activeUser = activeUser;
            }
            window.setTimeout(function() {
              $('#defectsCount').find('data').html(l);
            }, 3000);
            console.log(data);
            render(data, 'project-detail-comments', config.commentsDefects);
            config.writeCommentDefects.click(writeCommentDefects);

            $(window).animate({
              scrollTop: $("#project-detail-tabs").offset().top
            }, 500);
          } else {
            $('#defects').html('<p>Henüz bir ' + dislikeHead + ' girişi yapılmamıştır.</p>');
          }
        })
        ]
    },

    writeCommentNewFeatures = function() {
      var $this = $(this);

      if ( $this.data('requestRunning') ) {
        return;
      }
      $this.data('requestRunning', true);

      getData('/projects/' + projectId + '/am-i-backer').done(function(data){
        $this.data('requestRunning', false);

        if(data.success) {
          config.sendMessage.click(postComment);
          config.commentModal.modal('show');
        }
        else if(!data.success) {
          config.infoModal.find('p').html(data.message);
          config.infoModal.modal('show');
        }
        else {
          console.log('writeComment fail');
          return false;
        }
      });
    },

    writeCommentDefects = function() {
      var $this = $(this);

      if ( $this.data('requestRunning') ) {
        return;
      }
      $this.data('requestRunning', true);

      getData('/projects/' + projectId + '/am-i-backer').done(function(data){
        $this.data('requestRunning', false);

        if(data.success) {
          config.sendMessage.click(postComment);
          config.commentModal.modal('show');
        }
        else if(!data.success) {
          config.infoModal.find('p').html(data.message);
          config.infoModal.modal('show');
        }
        else {
          console.log('writeComment fail');
          return false;
        }
      });
    },

    addToFavourites = function(e) {
      var $this = $(this);
      $this.off('click');
      e.target.setAttribute('disabled', 'disabled');
      $.ajax({
        url: baseUrl+'/projects/'+projectId+'/add-to-favorites',
        type: 'POST',
        contentType: 'application/x-www-form-urlencoded; charset=UTF-8'
      }).done(function(data){
        $this.removeAttr('disabled');
        if(data.success) {
         fbq('track', 'AddToWishlist');
         config.favouriteModal.find('p').html(data.message);
         config.favouriteModal.modal('show');
        }
        else if(!data.success) {
          console.log('add-to-favorites success false')
        }
        else {
          console.log('add-to-favorites fail');
          return false;
        }
        $this.click(addToFavourites);
        e.target.removeAttribute('disabled');
      });
    },

    contactMessage = function() {
      config.contact.click(function() {
        config.counterpartId.val($(this).data('id'))
      });
      config.sendContactMessage.click(function(e) {
        var $this = $(this);
        if ( $this.data('requestRunning') ) {
          return;
        }
        $this.data('requestRunning', true);

        sendData('/messages/create', {counterpartId: parseInt(config.counterpartId.val()), message: config.contactMessage.val()}).done(function(data){
          $this.data('requestRunning', false);
          config.contactModal.modal('hide');
          config.contactMessage.val('');

          //Send Google Analytics Hit Contact
          ga('send', 'event', 'contactProjectOwner', config.projectId);
          Curio.eventCreate({eventKey: 'contactProjectOwner', eventValue: projectId});
        });

      });
    }(),

    postComment = function(e) {
      var $this = $(this);
      if ( $this.data('requestRunning') ) {
        return;
      }
      $this.data('requestRunning', true);
      sendData('/projects/'+projectId+'/comments/create', {comment: config.message.val()}).done(function(data){
        config.commentModal.modal('hide').on('hidden.bs.modal', function() {
          $this.data('requestRunning', false);
          $('a[href="#yorumlar"]').trigger('click');
          config.location = '#yorumlar';
        });

        config.message.val('');
        getData('/projects/'+projectId+'/comments').done(function(data){
          data.cs = contentStrings;
          render(data, 'project-detail-comments', config.comments);
          config.writeComment.click(writeComment);
        });
      });
    },

    postQuickRate = function(e) {
      console.log('girdi');
      var btn = config.likeItOrNotButton;
      var form = btn.closest('form');

      if ( btn.data('requestRunning') ) {
        return;
      }

      btn.data('requestRunning', true);
      config.savingLayerBotom.show();
      btn.attr('disabled', 'disabled');
      form.submit();

    },

    formatDatewithHours = function(fullDate) {
      var date = fullDate.substr(0,10),
        months = ['Ocak', 'Şubat', 'Mart', 'Nisan', 'Mayıs', 'Haziran', 'Temmuz', 'Ağustos', 'Eylül', 'Ekim', 'Kasım', 'Aralık'],
        dateArr = date.split('.');
      return dateArr[0]+ ' ' + months[dateArr[1]-1]  + ' ' + dateArr[2];
    },

    render = function(data, template, target) {
      target.html($.parseHTML(app.Templates[template](data)))
    },

    doneCallback = function() {
      console.log('done');

      if(config.hash === '#writeComment') {
        config.writeComment.trigger('click');
      }

      if (config.hash !== '' && config.hash !== '#writeComment') {
        $('a[href="' + config.hash + '"]').tab('show');
      }
      config.tabs.on('shown.bs.tab', manageTabs);

      //Greater than equal validator for parsley
      window.ParsleyValidator.addValidator('gte',
        function (value, requirement) {
          return parseFloat(value) >= parseFloat($(requirement).val());
        }, 32);

      window.ParsleyConfig = {
        excluded: 'input[type=button], input[type=submit], input[type=reset]',
        inputs: 'input, textarea, select, input[type=hidden], :hidden'
      };

      /*autoComplete();*/

      $('form').each( function() { //Form validations
        $(this).parsley(
          { errorClass: 'error',
            successClass: 'valid',
            classHandler: function(el) {
              /*console.log(el.$element);*/
              return el.$element.parents('.form-group');
            }
          });
      });

      $('.reward').click(rewardHelpers);
      config.body.not('.reward--selected').click(rewardFocusOut);

      config.backProjectBtn.click(function(e) {
        e.preventDefault();
        $('html, body').animate({ scrollTop: $('#detail-rewards').offset().top - 70 }, 'slow');
      });

      //Add to Favourites(Hatırlat)
      config.addToFavourites.click(addToFavourites);

      //Send Google Analytics Hit when project is added to Favourites (Hatırlat)
      config.addToFavourites.one('click', function() {
        ga('send', 'event', 'addProjectToFavorites', projectId);
        Curio.eventCreate({eventKey: 'addProjectToFavorites', eventValue: projectId});
      });

      //Send Google Analytics Hit Contact is inside contactMessage method above
      //Send Google Analytics Hit Video Play is inside playVideo method below

      //Send Google Analytics Hit when attempted to Pledge (Devam)
      $('.reward--available').find('button').each(function() {
        var button = $(this);
        button.click(function() {
          var amount = button.prev().find('input[name="amount"]').val();
          ga('send', 'event', 'attemptToPledge', projectId, amount);
          Curio.eventCreate({eventKey: 'attemptToPledge', eventValue: projectId + ',' + amount});
        });
      });

      //Send Google Analytics Hit when Timeline is showed
      $('a[href="#timeline"]').click(function(){
        ga('send', 'event', 'openProjectTimeline', projectId);
        Curio.eventCreate({eventKey: 'openProjectTimeline', eventValue: projectId});
      });

      //Send Google Analytics Hit when Yorumlar is showed
      $('a[href="#yorumlar"]').click(function(){
        ga('send', 'event', 'openProjectComments', projectId);
        Curio.eventCreate({eventKey: 'openProjectComments', eventValue: projectId});
      });

      //Send Google Analytics Hit when Twitter Share
      config.twitterShare.click(function(){
        ga('send', 'event', 'shareTwitter', projectId);
        Curio.eventCreate({eventKey: 'shareTwitter', eventValue: projectId});
      });

      //Send Google Analytics Hit when Facebook Share
      config.fbShare.click(function(){
        ga('send', 'event', 'shareFacebook', projectId);
        Curio.eventCreate({eventKey: 'shareFacebook', eventValue: projectId});
      });

      //Send Google Analytics Hit when Email Share
      config.emailShare.click(function(){
        ga('send', 'event', 'shareEmail', projectId);
        Curio.eventCreate({eventKey: 'shareEmail', eventValue: projectId});
      });


      config.likeItOrNot.attr('action', '/quick-rate/' + projectId);
      console.log('HERE 1');
      //config.likeItOrNotButton.on('click', postQuickRate);
      config.likeItOrNot.on('submit', postQuickRate);

      //Show page
      config.savingLayer.fadeOut(600);

      $('.corp-button-delete-comment').on('click', function() {
        var aydi = $(this).attr('data-id');
        $('#deleteCommentModal').modal('show');

        $('#btnDeleteComment').off('click');
        $('#btnDeleteComment').on('click', function() {
          $.ajax({
            url: "/projects/comments/" + aydi + "/remove",
            type: "get",
            dataType: "json",
            success: function(result){
              if (result.success) {
                window.location.reload(true);
              } else {
                $('#infoModal').find('.text-center').html(result.message);
                $('#infoModal').modal('show');
              }
            }
          });
        })
      });
    },

    playVideo = function() {
      config.videoPitch.removeAttr('style').addClass('has_played').css('overflow', 'visible');
      config.bigPlay.hide();
      var div = document.getElementById('videoPitch');
      div.plyr.play();

      //Send Google Analytics Hit on watchProjectVideo
      ga('send', 'event', 'watchProjectVideo', projectId);
      Curio.eventCreate({eventKey: 'watchProjectVideo', eventValue: projectId});
    },

    init = function() {

      $.ajaxSetup({
        beforeSend: function(jqXHR) {
          jqXHR.setRequestHeader('cf-request-type', 'ajax');
        },
        statusCode: {
          403: function() {
            location.href = '/login'
          }
        }
      });

      $.when.apply($, getProject()).done(function() {
          doneCallback();
      })
      .fail(function() {
        console.log('fail')
      });

      // config.rewardCheckoutForms.click(function(event){
      //   event.preventDefault();
      //   fbq('track', 'AddToCart');
      //   $(this).submit();
      // });

      config.shareBtn.click(function() {
        $('#sharingIsCaring').slideToggle(500);
      });



      config.videoPitch.one('click', playVideo);


      plyr.setup();

    };

    return {
      start: init
    }

  }(jQuery));

    var errorMessageHead = '';
    var likeHead = '';
    var dislikeHead = '';

    $('.button-rate use').addClass('darken');
  $('.button-rate span').css({'color': '#555'});

  $('.button-rate').on('click', function() {
    $('.button-rate use').addClass('darken');
    $('.button-rate span').css({'color': '#555'});
    $('.button-rate input[name="rate"]').attr('checked', false);
    $(this).find('input[name="rate"]').attr('checked', 'checked');
    if ($(this).hasClass('button-rate-like')) {
      $('.button-rate-like').find('use').removeClass('darken');
      $('.button-rate-like span').css('color', '#FFF');
    } else {
      $('.button-rate-dislike').find('use').removeClass('darken');
      $('.button-rate-dislike span').css('color', '#FFF');
    }
    var val = $(this).attr('data-value');
    var $li = $(this).closest('li');
    $('li.project-nav-item').removeClass('active');
    console.log('VAL: ' + val);
    if (val === '1') {
      $('#newFeatures').tab('show');
      $('li[data-value="1"]').addClass('active');
    } else {
      $('#defects').tab('show');
      $('li[data-value="2"]').addClass('active');
    }
  });

  $('#likeItOrNotFile').dropzone({
    url: '/misc/upload-file',
    paramName: 'file',
    clickable: true,
    createImageThumbnails: false,
    headers: {
      'Cache-Control': null,
      'X-Requested-With': null
    },
    init: function() {
      this.on('addedfile', function(file) {
            console.log(file);
            $('.corp-quickrate-file-loader').css('display', 'inline-block');
          }).on('success', function(file, response) {
            console.log(file);
            $('#likeItOrNotFileHidden').val(response.url);
            $('#likeItOrNotFileName').html(file.name);
            $('.corp-quickrate-file-loader').css('display', 'none');
          }).on('error', function(file, response) {
            $('#likeItOrNotFileName').html('<span style="color: red">Hata Oluştu!</span>');
            $('.corp-quickrate-file-loader').css('display', 'none');
          });
    }
  });

  $('#likeItOrNotButton').on('click', function() {
    var payload = {};
    payload.rate = parseInt($('input.input-rate[checked="checked"]').val());
    console.log(payload);
    if (isNaN(payload.rate)) {
      $('#quickRateModal #error-message').html(errorMessageHead + ' seçmelisin.');
      $('#quickRateModal').modal('show');
      return false;
    };
    payload.comment = $('#likeItOrNotComment').val();
    /*if (payload.comment === '') {
      $('#quickRateModal #error-message').html('Açıklama girmelisin sonra gönderebilirsin.');
      $('#quickRateModal').modal('show');
      return false;
    };*/
    payload.file = $('input[name="file"]').val();

    var url = $('#likeItOrNot').attr('action');
    $.ajax({
      "url": url,
      "type": "post",
      "data": payload,
      "dataType": "json",
      'success': function (res) {
        if (res.success) {
            $('#infoModal #infoModalText').html(res.message);
            $('#infoModal').modal('show');
            $('#infoModal').on('hidden.bs.modal', function () {
                console.log('OLDU');
                window.location.reload();
            })
        } else {
          $('#quickRateModal #error-message').html(res.message);
          $('#quickRateModal').modal('show');
        }
      },
      'error': function () {
        $('#quickRateModal #error-message').html('Teknik bir hata oluştu.');
        $('#quickRateModal').modal('show');
      }
    });
  });


  $(document).on('click', '.corp-button-micro', function(e) {
    console.log('LAYK DISLAYK');
    var el = e.target;
    var postfix = $('#rev-project-header').attr('data-postfix');
    if (!$(el).hasClass('corp-button-delete-comment')) {
      if ($(el).hasClass('button-micro-file')) {
        var href = $(el).attr('data-href');
        if (href.indexOf('.png') !== -1 || href.indexOf('.PNG') !== -1 || href.indexOf('.jpg') !== -1 || href.indexOf('.jpeg') !== -1 || href.indexOf('.JPG') !== -1 || href.indexOf('.JPEG') !== -1 ) {
          $('#pictureModalPicture').attr('src', href);
          $('#pictureModal').modal('show');
        } else {
          window.open(href);
        }
      } else {
        /*
        var selected = $(el).attr('data-selected') === '1';
        if (!isLoggedIn) {
          window.location.assign('/projeler/' + postfix + '/secure')
        } else {

          var val = $(el).attr('data-value');
          var aydi = $(el).attr('data-id');

          if (selected) {
            $.ajax({
              "url": '/projects/comments/' + aydi + '/un-rate',
              "type": "post",
              "data": {rate: val},
              "dataType": "json",
              'success': function (res) {
                if (res.success) {
                  window.location.reload(true);
                } else {
                  $('#quickRateModal #error-message').html(res.message);
                  $('#quickRateModal').modal('show');
                }
              },
              'error': function () {
                $('#quickRateModal #error-message').html('Teknik bir hata oluştu.');
                $('#quickRateModal').modal('show');
              }
            });
          } else {
            $.ajax({
              "url": '/projects/comments/' + aydi + '/rate',
              "type": "post",
              "data": {rate: val},
              "dataType": "json",
              'success': function (res) {
                if (res.success) {
                  window.location.reload(true);
                } else {
                  $('#quickRateModal #error-message').html(res.message);
                  $('#quickRateModal').modal('show');
                }
              },
              'error': function () {
                $('#quickRateModal #error-message').html('Teknik bir hata oluştu.');
                $('#quickRateModal').modal('show');
              }
            });
          }
        }
        */
      }
    }
  });

}());
