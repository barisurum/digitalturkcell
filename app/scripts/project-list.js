(function() {
  'use strict';
  window.app = window.app || {};
  window.app.ProjectListModule = (function ($, app) {
    var baseUrl = '';
    var config = {
      hash: window.location.hash,
      status: window.location.hash.substr(1),
      navlist: $('.navlist'),
      projectsPopular: $('#projectsPopular'),
      projectsAboutToFinish: $('#projectsAboutToFinish'),
      projectsAll: $('#projectsAll'),
      loadMore: $('#loadMore'),
      loading: $('#loading'),
      projectPlaceholders: $('.project-placeholder'),
      search: $('#query'),
      searchWrapper: $('.nav-livesearch'),
      noMore: $('#noMore')
      },

      manageTabs = function (e) {
        var hash = $(e.target).attr('href');
        if (hash.substr(0, 1) === '#') {
          /* window.history.pushState(null, null, hash);*/
          var position = $(window).scrollTop();
          location.replace('#' + hash.substr(1));
          $(window).scrollTop(position);
        }
      },

      getData = function (url, data) {
        return $.ajax({
          url: baseUrl + url,
          data: data,
          type: 'GET',
          contentType: 'application/json',
          dataType: 'json'
        });
      },

      getProjects = function(status) {
        config.noMore.hide();
        config.loadMore.off('click');

        var addStatus = function() {
          if(status===2) {
            return '?status=2';
          }
          else if (status===3) {
            return '?status=3';
          }
          else if (status===4) {
            return '?status=4';
          }
          else if (status===5) {
            return '?status=5';
          }
          else {
            return '';
          }
        };

        return [
          getData('/projects/popular'+addStatus()).done(function(data){
            for (var i = 0; i < data.length; ++i) {
              data[i].cs = contentStrings
            }
            console.log(data);
            render(data, 'projects', config.projectsPopular);

            if (activeUser == null) {
              $('.project-card').css('height', '370px');
            }
          }),
          getData('/projects/about-to-finish'+addStatus()).done(function(data){
            for (var i = 0; i < data.length; ++i) {
              data[i].cs = contentStrings
            }
            console.log(data);
            render(data, 'projects', config.projectsAboutToFinish);

            if (activeUser == null) {
              $('.project-card').css('height', '370px');
            }
          }),
          getData('/projects/all'+addStatus(), {'page': 0}).done(function(data){
            for (var i = 0; i < data.length; ++i) {
              data[i].cs = contentStrings
            }
            console.log(data);
            config.projectPlaceholders.hide();
            config.loading.hide();
            render(data, 'projects', config.projectsAll, true);
            config.loadMore.click(function(){
              loadMoreProjects(1, addStatus());
              config.loadMore.hide();
            });
            if(data.length >= 6) { //Hide Daha Fazla Yükle button whenever projects are less than 6
              config.loadMore.show();
            }

            if (activeUser == null) {
              $('.project-card').css('height', '370px');
            }
          })
        ];
      },

      loadMoreProjects = function(pageNumber, status) {
        var isAjaxCalled = false;

        $(window).scroll(function(){

          if(!isAjaxCalled && hasScrolledBottom()) {
            isAjaxCalled = true;
            config.loading.show();
            getData('/projects/all'+status, {'page': pageNumber}).done(function(data){
              config.loading.hide();
              for (var i = 0; i < data.length; ++i) {
                data[i].cs = contentStrings
              }
              console.log(data);
              if(data.length=== 6) {
                render(data, 'projects', config.projectsAll, true);
                pageNumber++;
                loadMoreProjects(pageNumber, status);
              }
              else if (data.length > 0 && data.length < 6) { //If last page with data
                render(data, 'projects', config.projectsAll, true);
                config.noMore.show();
              }
              else if (data.length === 0) { //If last page without data
                config.noMore.show();
              }
              else {
                return false;
              }
            });
          }

          else {
            return false;
          }


        });

      },

      hasScrolledBottom = function() {

          var docViewTop = $(window).scrollTop();
          var windowHeight = $(window).height();

          var elemTop = config.projectsAll.offset().top;
          var elemHeight = config.projectsAll.height();

          return docViewTop + windowHeight >= elemTop + elemHeight;

      },

      autoComplete = function() {

          config.search.autocomplete({
            appendTo: config.searchWrapper,
            minLength: 3,
            source: function( request, response ) {
              $.ajax({
                url: baseUrl + '/projects/search',
                data: {
                  query: request.term
                },
                success: function( data ) {

                  if(data.length === 0){
                    data.push({
                      id: 0,
                      name: 'Sonuç bulunamadı'
                    });
                  }
                  response(data);
                  fbq('track', 'Search');
                }
              });
            },
            open: function(){
              $('.ui-autocomplete').css({'width': '420px', 'top': '1px', 'left': '-11px'}).addClass('search-list');
            }
          })
            .autocomplete( 'instance' )._renderItem = function( ul, item ) {
            if(item.id!==0) {
              return $( '<li>' )
                .append( '<a href="/projects/'+item.id+'/detail" target="_blank">' + item.name )
                .appendTo( ul );
            }
            else {
              return $( '<li>' )
                .append( '<span class="text-center block">' + item.name )
                .appendTo( ul );
            }
          };

      }(),

      doneCallback = function() {
        console.log('all done');
      },

      failCallback = function() {
        console.log('fail!');
      },

      render = function(data, template, target, append) {
        console.log(data);
        append ? target.append($.parseHTML(app.Templates[template](data))) : target.html($.parseHTML(app.Templates[template](data)));
      },

      init = function() {
        $.ajaxSetup({
          beforeSend: function(jqXHR) {
            jqXHR.setRequestHeader('cf-request-type', 'ajax');
          },
          statusCode: {
            403: function() {
              location.href = '/login'
            }
          }
        });

        function listProjects(status) {
          if(status==='düşünme') {
            $.when.apply($, getProjects(2)).done(function() {
              doneCallback();
            })
              .fail(function() {
                failCallback();
              });
          }

          else if (status==='fonlama') {
            $.when.apply($, getProjects(3)).done(function() {
              doneCallback();
            })
              .fail(function() {
                failCallback();
              });
          }

          else {
            $.when.apply($, getProjects()).done(function() {
              doneCallback();
            })
              .fail(function() {
                failCallback();
              });
          }
        }

        listProjects(config.status);

        config.navlist.find('a').click(manageTabs);

        if (config.hash !== '') {
          var activeTab = $('a[href="' + config.hash + '"]');
          activeTab.trigger('click');
        }

        window.onhashchange = function() {
          listProjects(window.location.hash.substr(1));
        };

      };

    return {
      start: init
    }

  }(jQuery, window.app));
}());
